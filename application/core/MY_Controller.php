<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
	}	
	
	/* load the view files admin */
	public function show_view_admin($view, $data = '') 
	{    	
		//$data= array();		
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar',$data);
		$this->load->view($view, $data);
		$this->load->view('admin/footer');
	}
	
	public function email($to, $subject, $msg) {
        $this->load->library('email');

        $config['protocol'] = 'sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('info@votivetech.in', 'Kourier');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->send();         
    

    }
    
    public function generate_id()
	{
		$id= round(microtime(true) * 1000); // rand(round(microtime(true) * 1000) , 2);

		return $id =($id < 0)? $id+1: $id;
	}	

	public function show_view_restaurant($view, $data = '') 
	{    	
		//$data= array();		
		$this->load->view('restaurant/header');
		$this->load->view('restaurant/sidebar',$data);
		$this->load->view($view, $data);
		$this->load->view('restaurant/footer');
	}
	
	public function ImageUpload($filename, $name, $imagePath, $fieldName)
	{
		$temp = explode(".",$filename);
		$extension = end($temp);
		$filenew =  time().'_'.str_replace($filename,$name,$filename)."." .$extension;  		
		$config['file_name'] = $filenew;
		$config['upload_path'] = $imagePath;
		$config['allowed_types'] = 'GIF | gif | JPE | jpe | JPEG | jpeg | JPG | jpg | PNG | png';
		$this->upload->initialize($config);
		$this->upload->set_allowed_types('*');
		$this->upload->set_filename($config['upload_path'],$filenew);
		
		if(!$this->upload->do_upload($fieldName))
		{
			$data = array('msg' => $this->upload->display_errors());
			echo $data;
			die;
		}
		else 
		{ 
			$data = $this->upload->data();	
			$imageName = $data['file_name'];			
			return $imageName;
		}		
	}

		
	/*	Check Session Admin */
/*  Send Message*/

public function sendMessage($mobileNumber, $message)
    {
//Your authentication key
$authKey = "SKc8119a5e482cf0f850bdde10a2f08b92";

//Multiple mobiles numbers separated by comma
// $mobileNumber = "9753220392";

//Sender ID,While using route4 sender id should be 6 characters long.
$senderId = "ACf3c15917b08e13ca3ae4c5bb91839065";
$authtoken ="75c471631a7848576a1ee53e769e683f";
$TWILIO_ACCOUNT_SID = "AC94f15f204bb73bb30f3c680f113f4d59";
$TWILIO_AUTH_TOKEN = "75c471631a7848576a1ee53e769e683f";
//Your message to send, Add URL encoding here.
// $message = urlencode("Test message");

//Define route
$route = "4";
//Prepare you post parameters
$postData = array(
'authkey' => $authKey,
'mobiles' => $mobileNumber,
'message' => $message,
'sender' => $senderId,
'route' => $route,
'country' => '91'
);

//API URL
$url="https://api.twilio.com/2010-04-01/Accounts \
     -u <ACf3c15917b08e13ca3ae4c5bb91839065>:<57bd920870e7a35862931fcf3333cd86>";

// init the resource
$ch = curl_init();

curl_setopt_array($ch, array(
CURLOPT_URL => $url,
CURLOPT_RETURNTRANSFER => true,
CURLOPT_POST => true,
CURLOPT_POSTFIELDS => $postData
//,CURLOPT_FOLLOWLOCATION => true
));


//Ignore SSL certificate verification
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
$output = curl_exec($ch);


//Print error if any
if(curl_errno($ch))
{
echo 'error:' . curl_error($ch);
}

curl_close($ch);

echo $output;
    }

}
?>