@extends('admin.layout.layout')
@section('title', 'Edit Question')

@section('current_page_css')
@endsection


@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       View Time Slot
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">View Time Slot</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="{{url('admin/timeSlotList/day')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->
          <form action="{{url('/admin/update_question')}}" method="POST"
           enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
             <div class="box-body">
              <div class="row">
                
                 @foreach ($viewtabl_data as $row)
<?php    
          if($row->status == 1)
          {
            $status = 'Active';
          } 
          else
          {
            $status = 'Deactive';
          }

          ?>
                <div class="col-md-9 for-divrsn">
                  <div class="row" style="text-align: left;">
                    <div class="col-md-12">                      
                       <div class="form-group">
                        <div class="row">
                          <div class="col-md-6"><strong>Day:</strong></div>
                          <div class="col-md-6">{{$row->day}}</div>
                        </div>
                      </div>
                       <div class="form-group">
                        <div class="row">
                          <div class="col-md-6"><strong>Start Time:</strong></div>
                          <div class="col-md-6">{{$row->start_time}}</div>
                        </div>
                      </div>
                     <div class="form-group">
                        <div class="row">
                          <div class="col-md-6"><strong>End Time:</strong></div>
                          <div class="col-md-6">{{$row->end_time}}</div>
                        </div>
                      </div>
                       
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6"><strong>Status:</strong></div>
                          <div class="col-md-6">{{$status}}</div>
                        </div> 
                      </div>
                     
                     
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box-body -->
          </div>
           
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('current_page_js')
<script type="text/javascript">
 $(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>
@endsection