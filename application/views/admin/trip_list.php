<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Reiseliste
            <small>Reiseliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Reiseliste</h3>
                  <div class="pull-right box-tools">
                  <a href="<?php echo base_url();?>admin/Trip/addtrip" class="btn btn-info btn-sm">Neu hinzufügen</a> 
                  </div>
                </div><!-- /.box-header -->
            <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="the_table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>Name</th>
						            <th>Firma Name</th>
                        <th>Tour Name</th>
                        <th>Fahrername</th>
                        <th>Status zuweisen</th>
						            <th></th>
                        <th>Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i =1 ;
                        foreach($trip_details as $value)
                        {
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $value->name; ?></td>
							              <td><?php echo $value->restaurant_name; ?> <b>(<?php echo $value->first_name; ?>)</b></td>
                            <!-- <?php
                                 if(!empty($value->parcel_id))
                                 {
                                  $parcel_array = explode(",", $value->parcel_id);
                                  $customer_name = array();
                                  foreach ($parcel_array as $value_par) 
                                  {
                                     
                                     $customerdetails =     $this->db->get_where('customer',array('parcel_id'=>$value_par))->row();
                                     $customer_name[] =         $customerdetails->first_name.' '.$customerdetails->last_name;

                                  }
                                $customer_name_main  =        implode(" , ",$customer_name);
                                if(!empty($customer_name_main))
                                {
                                   $customer_name = $customer_name_main;
                                }
                                else
                                {

                                   $customer_name ="";
                                }
                              }
                              else
                              {
                                $customer_name = "";
                              }
                            ?>
                            <td><?php echo $customer_name; ?></td> -->
                            <td><?php echo $value->brand; ?></td>
                         
                            <td>
                              <?php 
                              $driver_details   =    $this->Trip_model->getDriverDataById($value->driver_id);
                              if(!empty($driver_details))
                              {
                                 echo $driver_details->first_name.' '.$driver_details->surname;
                                  
                              }
                              else
                              {
                                  echo "";
                              }
                              
                              ?>
                          </td>
                          <td>
                              <?php 
                              if($value->trip_assign_status == 0)
                              {echo 'Not Assigned'; }
                              elseif($value->trip_assign_status == 1)
                              {
                                echo 'Assigned By Admin';  
                              }
                              elseif($value->trip_assign_status == 2)
                              {
                                  echo 'Self assigned';
                              }
                              else 
                              {
                                  echo "";
                              }
                              ?>
                          </td>
						<td><a href="<?php echo base_url();?>admin/trip/trackingtrip/<?php echo $value->trip_id; ?>" class="btn btn-info btn-sm">Live-Tracking</a></td>

                          <td>
                              <a href="<?php echo base_url();?>admin/trip/tripfullView/<?php echo $value->trip_id; ?>" title="View"><i class="fa fa-eye fa-2x "></i></a>&nbsp;&nbsp;<a href="<?php echo base_url();?>admin/trip/addTrip/<?php echo $value->trip_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>&nbsp;&nbsp;
                          <a class="confirm" onclick="return delete_trip('<?php echo $value->trip_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                        </tr>
                      
                      <?php
                      $i++;
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
<script type="text/javascript">
    function delete_trip(driverid)
    {
          
          bootbox.confirm("Are you sure you want to delete Trip Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>admin/trip/delete_trip/"+driverid;
            }
        });
    } 
</script>
<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>
