<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             Restaurant ansehen 
            <small>Restaurant ansehen 
</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i>  Zuhause</a></li>

          </ol>
        </section>
         <br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
              <div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Restaurant ansehen </strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>admin/restaurant" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
            <br>
            </div>
             <div class="panel-body">
               <form action="#" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">

                   <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Vorname <span class="text-danger"> *</span></label>
                                <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo $restaurent_details->first_name; ?>" readonly/>
                                <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Nachname</label><span class="text-danger"> *</span></label>
                                <input name="surname" class="form-control" type="text" id="surname" value="<?php echo $restaurent_details->surname; ?>" readonly/>
                                <?php echo form_error('surname','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Firma Name<span class="text-danger"> *</span></label>
                                <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo $restaurent_details->restaurant_name; ?>" readonly/>
                                <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?>
                           </div> 
                            
                         </div>
                       </div>
                       <div class="row">
                         <div class="form-group">
                             <div class="col-md-4 col-sm-4">
                              <label>Telefonnummer<span class="text-danger"> *</span></label>
                                <input name="mobile_number" class="form-control" type="text" id="mobile_number" value="<?php echo $restaurent_details->mobile_number; ?>" readonly/>
                                <?php echo form_error('mobile_number','<span class="text-danger">','</span>'); ?>
                           </div>
                          
                            <div class="col-md-4 col-sm-4">
                              <label>E-Mail<span class="text-danger"></span></label>
                                <div class="fancy-file-upload fancy-file-primary">
                                  <input type="email" class="form-control" name="email" value="<?php echo $restaurent_details->email; ?>" readonly>
                                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>

                                 </div>                                
                            </div>  
                            <div class="col-md-4 col-sm-4">
                             <label>Stadt<span class="text-danger"> *</span></label>
                                <input name="city" class="form-control" type="text" id="city" value="<?php echo $restaurent_details->city; ?>" readonly/>
                                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>
                           </div>
                           </div>
                           
                        </div>
                       
                       <div class="row">
                         <div class="form-group">
                             <div class="col-md-4 col-sm-4">
                              <label>Postleitzahl<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="post_code" value="<?php echo $restaurent_details->zipcode; ?>"  readonly>
                                <?php echo form_error('post_code','<span class="text-danger">','</span>'); ?>
                           </div>
                             <div class="col-md-4 col-sm-4">
                              <label>Adresse<span class="text-danger">*</span></label>
                               <textarea name="address" class="form-control" value="" readonly><?php echo $restaurent_details->address; ?></textarea>
                                <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
                           </div>
                            <div class="col-md-4 col-sm-4">
                                <label><span class="text-danger">*</span></label>
                             <img src="<?php echo $restaurent_details->restaurant_image ?>" height="100px" width="100px">
                            </div>
                           <div class="col-md-4 col-sm-4">
                             <label>Status<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control" readonly>
                                    <option <?php if($restaurent_details->status == '1'){echo 'selected';}?> value="1">Aktiv</option>
                                    <option <?php if($restaurent_details->status == '0'){echo 'selected';}?> value="0">Inaktiv</option>
                                </select>
                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>
                       </div>
                       <div class="row">
                           
                               <div class="col-md-4">
                      <h4><b>Bankverbindung</b></h4>
                      </div>
                           </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                              <label>Bankname <span class="text-danger">*</span></label>
                               <textarea name="address" class="form-control" value="" readonly><?php echo $restaurent_details->bank_details; ?></textarea>
                                <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
                           </div>
			   <div class="col-md-4">
			       <label>Name auf der Karte<span class="text-danger"> *</span></label>
                 <input name="name_holder" class="form-control" type="text" id="name_holder" value="<?php echo $restaurent_details->name_holder ?>" placeholder="Name on the card" readonly/>
                <?php echo form_error('name_holder','<span class="text-danger">','</span>'); ?>
              </div>
			  
			   <div class="col-md-4">
			       <label>Kartennummer<span class="text-danger"> *</span></label>
                 <input name="card_no" class="form-control" type="text" id="card_no" value="<?php echo $restaurent_details->card_no ?>" placeholder="Card Number" readonly/>
                <?php echo form_error('card_no','<span class="text-danger">','</span>'); ?>
              </div>
			 </div>
            <div class="row">

			   <div class="col-md-4">
			     <label>Verfallsdatum <span class="text-danger"> *</span></label>
                 <input name="expire_date" class="form-control" type="text" id="expire_date" value="<?php echo $restaurent_details->expire_date ?>" placeholder="Expire Date -02/2021" readonly/>
                <?php echo form_error('expire_date','<span class="text-danger">','</span>'); ?>
              </div>
              <div class="col-md-4">
			     <label>Sicherheitscode <span class="text-danger"> *</span></label>
                 <input name="security_code" class="form-control" type="text" id="security_code" value="<?php echo $restaurent_details->security_code ?>" placeholder="" readonly/>
                <?php echo form_error('expire_date','<span class="text-danger">','</span>'); ?>
              </div>
        
             </div> 

                       <br>
                  <div class="row">
                   <div class="form-group">
                    <div class="col-md-1">
                     <a href="<?php echo base_url();?>admin/restaurant" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>
                     </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      
  
  
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</body>
<!-- /MIDDLE -->

<script type="text/javascript">
    $(function () {
       
        $('#emp_dob_i').datetimepicker({
             format: 'Y-M-D'
        });    
    });

    function getStateList(country_id)
    {
        var str = 'country_id='+country_id;
        var PAGE = '<?php echo base_url(); ?>client/getStateList';
        
        jQuery.ajax({
            type :"POST",
            url  :PAGE,
            data : str,
            success:function(data)
            {           
                if(data != "")
                {
                    $('#client_state_id').html(data);
                }
                else
                {
                    $('#client_state_id').html('<option value=""></option>');
                }
            } 
        });
    }
</script>