<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bestellliste 
            <small>Bestellliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Bestellliste</h3>
                  <div class="pull-right box-tools">
                  
                  </div>
                </div><!-- /.box-header -->
            <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Vorname des Kunden</th>
                        <th>Nachname des Kunden</th>
                        <th>Kundennummer</th>
                        <th>Kundenadresse</th>
                        <th style="width: 175px;">Abholort</th>
                        <th>Ablageort</th>
                        <th>Notiz</th>
                        <th>Paket Status</th>
                        <th>Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach($customer_details as $value)
                        {
                            
                          $customer_details      =  $this->Parcel_model->getCustomerById($value->parcel_id);
                          $parcel_details      =  $this->Customer_model->getParcelById($value->parcel_id);
                        ?>
                         <tr>
                          <td>
                          <?php 
                          if(!empty($customer_details->first_name))
                          {                          
                              
                              echo $customer_details->first_name; 
                          }
                          else
                          {
                              echo "";
                          }
                          ?> 
                          </td>
                          <td>
                          <?php 
                          if(!empty($customer_details->last_name))
                          {                          
                              
                              echo $customer_details->last_name; 
                          }
                          else
                          {
                              echo "";
                          }
                          ?> 
                          </td>
                          <td><?php 
                          if(!empty($customer_details->customer_number))
                          { 
                              echo $customer_details->customer_number;
                              
                          }
                          else
                          {
                              echo "";
                          }
                          ?>
                          </td>
                          <td>
                              <?php 
                              if(!empty($customer_details->customer_address))
                              {
                                  echo $customer_details->customer_address;
                              }
                              else
                              {
                                  echo "";
                              }
                              ?>
                          </td>
                          <td><?php echo $parcel_details->pic_up_location; ?></td>
                          <td><?php echo $parcel_details->drop_location; ?></td>
                          <td><?php echo $parcel_details->parcel_information; ?></td>
                          <td>
                          <?php 
                          if($parcel_details->status =='1')
                          {
                              ?>
                              <button class="btn btn-success">Aktiv</button>
                              <?php
                          }
                          elseif($parcel_details->status =='2')
                          {
                              ?>
                              <button class="btn btn-info">Liefern</button>
                              <?php
                          }
                          else
                          {
                             ?> 
                            <button class="btn btn-danger">Inaktiv</button>
                           <?php
                          }
                          ?></td>
                          <td>
                              <a href="<?php echo base_url();?>admin/parcel/parcelFullView/<?php echo $parcel_details->parcel_id; ?>" title="Edit"><i class="fa fa-eye fa-2x "></i></a>&nbsp;<a href="<?php echo base_url();?>admin/parcel/addparcel/<?php echo $parcel_details->parcel_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>&nbsp;
                          
                          <a class="confirm" onclick="return delete_parcel('<?php echo $parcel_details->parcel_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                        </tr>
                      
                      <?php
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
  