<!DOCTYPE html>
<html>
    <style>
        .table {
  border: 1px;
  overflow-x: auto;
  display: block;
}
    </style>
  <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            GPS Kundenliste
            <small>GPS Kundenliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">GPS Kundenliste</h3>
                  <div class="pull-right box-tools">
                  <a href="<?php echo base_url();?>admin/restaurant/addRestaurant" class="btn btn-info btn-sm">Neue hinzufügen</a> 
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
              
                  <table id="the_table" class="table table-bordered table-striped container-fluid">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>Vorname</th>
                        <th>Name</th>
                        <th>Firma Name</th>
                        <th>Telefonnummer</th>
                        <th>E-Mail</th>
                        <th>Stadt</th>
                        <th>Firmen Logo</th>
						            <th>Total versendete SMS</th>
                        <th>Admin Status</th>
                        <th>Status</th>
                        <th>Reise hinzufügen</th>
                        <th>Aktion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($restaturant_details as $value)
                    {                  
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->first_name; ?></td>
                          <td><?php echo $value->surname; ?></td>
                          <td><?php echo  $value->restaurant_name; ?> </td>
                          <td><?php echo $value->mobile_number; ?></td>
                          <td><?php echo $value->email; ?></td>
                          
                          <td><?php echo $value->city; ?></td>
                          <td><img src="<?php echo $value->restaurant_image; ?>" height="100px" width="100px"></td>
                          <?php $smscoundata= $this->db->get_where('smsmonthdata',array('restr_id'=> $value->restaurant_id ))->result_array(); ?>
						  <td>
                  <?php if(!empty($smscoundata)){$mo = explode('-',date("m-d-Y")); 
				  if($mo[0]==01){ echo $smscoundata[0]['january'];}
				  if($mo[0]==02){ echo $smscoundata[0]['february'];}
				  if($mo[0]==03){ echo $smscoundata[0]['march'];}
				  if($mo[0]==04){ echo $smscoundata[0]['april'];}
				  if($mo[0]==05){ echo $smscoundata[0]['may'];}
				  if($mo[0]==06){ echo 'June - '; echo $smscoundata[0]['june'];}
				  if($mo[0]==07){ echo $smscoundata[0]['july'];}
				  /* if($mo[0]==08){ echo $smscoundata[0]['august'];}
				  if($mo[0]==09){ echo $smscoundata[0]['september'];}
				  if($mo[0]==10){ echo $smscoundata[0]['october'];}
				  if($mo[0]==11){ echo $smscoundata[0]['november'];}
				  if($mo[0]==12){ echo $smscoundata[0]['december'];} */ } else
				  { 
				  $mo = explode('-',date("m-d-Y"));
				  if($mo[0]==01){ echo 0;}
				  if($mo[0]==02){ echo 0;}
				  if($mo[0]==03){ echo 0;}
				  if($mo[0]==04){ echo 0;}
				  if($mo[0]==05){ echo 0;}
				  if($mo[0]==06){ echo 'June - '; echo 0;}
				  if($mo[0]==07){ echo 0;}
				  } ?><p><a href="<?php echo base_url(); ?>admin/restaurant/smsmonthlist/<?php echo $value->restaurant_id;  ?>">Mehr sehen</a></p></td>
						  
                          <td>
						    <label class="switch">
                                        <input type="checkbox" onclick="return status_changesd('<?php echo $value->restaurant_id; ?>','<?php echo $value->admin_approved_status ?>');" <?php if($value->admin_approved_status == 1){ echo 'checked'; } ?>>
                                        <span class="slider round"></span>
                           </label>					  
                          
                          </td>
                          <td>
                            <?php 
                              if($value->status == '1')
                              { 
                                ?>
                                <button class="btn btn-success">Aktiv</button>
                               <?php 
                              }
                              else
                              {
                                ?>
                                <button class="btn btn-info btn btn-danger">In Aktiv</button>
                               <?php    
                              }
                              ?>
                          </td>
                           <td><a href="https://votivetech.in/courier/admin/Trip/addtrip?<?php echo 'resturent_id='.$value->restaurant_id; ?>" class="btn btn-info btn-sm">Reise hinzufügen</a>
                           </td>
                          <td>
                              <a href="<?php echo base_url();?>admin/restaurant/fullViewRestaurant/<?php echo $value->restaurant_id; ?>" title="View"><i class="fa fa-eye fa-2x "></i></a>&nbsp;&nbsp;
                                <a href="<?php echo base_url();?>admin/restaurant/addRestaurant/<?php echo $value->restaurant_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>&nbsp;&nbsp;
                             <a class="confirm" onclick="return delete_restaurant('<?php echo $value->restaurant_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                            
                        <!--<td> <a href="<?php echo base_url(); ?>admin/restaurant/buy/<?php echo $value->restaurant_id; ?>" class="btn"><img src="<?php echo base_url();?>assets/ezgif-6-c36878577925.jpg" height="100px" width="100px"></a></td>-->
                        </tr>
                      <?php
                      $i++;
                         }
            
              ?>                       
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<script type="text/javascript">
function status_changesd(id,status){
//alert(status);	
 $.ajax({
  url: '<?php echo base_url(); ?>admin/restaurant/status_institute',
  type: "POST",
  data:{id:id,status:status}, 
  //dataType: "json",
  success: function(data){
   //alert(data);
     setTimeout(function(){
      location.reload(); 
    }, 1000);
  } 
}); 
}


    function delete_restaurant(driverid)
    {
          
          bootbox.confirm("Are you sure you want to delete Restaurant Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>admin/restaurant/delete_restaurant/"+driverid;
            }
        });
    } 
	
 	
	
</script>

<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>
