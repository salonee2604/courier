<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Referenzen hinzufügen
            <small>Referenzen hinzufügen</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Referenzen hinzufügen</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>admin/Pagedata/referenzenList" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>

            <div class="panel-body">
               <form action="<?php echo base_url();?>admin/Pagedata/repostrefer" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
                  
                    
                     <div class="row">
                        <div class="form-group">
                           <div class="col-md-6 col-sm-6">
                              <label>Titel<span class="text-danger"> *</span></label>
                                <input name="rp_title" class="form-control" type="text" id="rp_title" value="<?php echo set_value('rp_title'); ?>" required/>
                                <?php echo form_error('rp_title','<span class="text-danger">','</span>'); ?>
                           </div> 
                         
                            <div class="col-md-6 col-sm-6">
                              <label>Beschreibung<span class="text-danger"> *</span></label>
							   <textarea rows="4" name="rp_description" class="form-control" required> </textarea>
                                
                           </div>
                          
                         </div>
                       </div>
                       
                       <div class="row">					   
                    
						
                         <div class="col-md-4 col-sm-12">
                              <label>Bild<span class="text-danger"> *</span></label>
                                <input type="file" name="image" class="form-control" id="image" value="<?php echo set_value('image'); ?>" required/>
                                <?php echo form_error('image','<span class="text-danger">','</span>'); ?>
                           </div>
                          
                           </div>
                       </div>
                       <br>
                  <div class="row">
                    <div class="form-group"> 

                     <div class="col-md-1">
                        <!--<input type="submit" name="submit" value="Add" class="btn btn btn-success margin-top-30">-->
                        <button type="submit" name="submit" class="btn btn btn-success margin-top-30">Hinzufügen</button>
                     </div>
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>admin/Pagedata/referenzenList" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>
                     </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

