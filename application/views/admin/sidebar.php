 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php  echo $_SESSION['web_admin'][0]->first_name;
                     ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
         
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="<?php echo base_url(); ?>admin/restaurant">
                <i class="text_admin">GKL</i>
               
               <span>GPS Kundenliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>admin/restaurant/membership">
                <i class="text_admin">MSD</i>
                <span>Mitgliedschaftsdetails</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>admin/contactlist/">
                <i class="text_admin">KOL</i>
                <span>Kontaktliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>admin/Pagedata/referenzenList">
                <i class="text_admin">RFZ</i>
                <span>Referenzen</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
             <li class="active reeview">
              <a href="<?php echo base_url(); ?>admin/Pagedata/restaurantlogoList">
                <i class="text_admin">KLB</i>
                <span>Kunden Logo Banner</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
               <li class="active treeview">
              <a href="<?php echo base_url(); ?>admin/driver">
                <i class="text_admin">FL</i>
                <span>Fahrerliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
             <li class="active treeview">
              <a href="<?php echo base_url(); ?>admin/parcel">
                <i class="text_admin">PL</i>
                <span>Paketliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
             <li class="active treeview">
              <a href="<?php echo base_url(); ?>admin/customer">
                <i class="text_admin">KL</i>
                <span>Kundenliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>admin/trip">
                <i class="text_admin">RL</i>
                <span>Reiseliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>admin/message/liveLocation">
                <i class="text_admin">LAS</i>
                <span>Live Ampel System</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
			      
           
		    
           </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script>
  $( document ).ready(function() 
  {
      $(".sidebar-toggle").on('click', function(event)
      {
        $(".text_admin").toggle();
        // $(".icon_hide").hide();
        // $(".fa").show();
      });
      $(".icon_hide").show();
      $(".text_admin").hide();
  });
</script>