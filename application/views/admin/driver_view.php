<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             Treiber anzeigen
            <small>Treiber anzeigen</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Heim</a></li>

          </ol>
        </section>
         <br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
              <div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Treiber anzeigen</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>admin/driver" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
            <br>
            </div>
             <div class="panel-body">
               <form action="<?php echo base_url();?>admin/driver/adddriver/<?php echo $edit_driver->driver_id; ?> " method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
                  
                    <!-- <div class="row">
                       <div class="col-md-4">
                           <img src="<?php echo base_url(); ?>image/<?php echo $img_url; ?>" height="100px" width="100px">
                       </div>
                   </div>  -->
                     <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Vorname<span class="text-danger"> *</span></label>
                                <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo $edit_driver->first_name; ?>" readonly/>
                                <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Nachname</label><span class="text-danger"> *</span></label>
                                <input name="surname" class="form-control" type="text" id="surname" value="<?php echo $edit_driver->surname; ?>" readonly/>
                                <?php echo form_error('surname','<span class="text-danger">','</span>'); ?>
                           </div>
                            <div class="col-md-4 col-sm-4">
                              <label>Telefonnummer<span class="text-danger"> *</span></label>
                                <input name="mobile_number" class="form-control" type="text" id="mobile_number" value="<?php echo $edit_driver->mobile_number; ?>" readonly/>
                                <?php echo form_error('mobile_number','<span class="text-danger">','</span>'); ?>
                           </div>
                          
                         </div>
                       </div>
                       <div class="row">
                         <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>E-Mail<span class="text-danger"></span></label>
                                <div class="fancy-file-upload fancy-file-primary">
                                  <input type="email" class="form-control" name="email" value="<?php echo $edit_driver->email; ?>" readonly>
                                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>

                                 </div>                                
                            </div> 
                         </div>
                       </div>                      
                       <div class="row">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4">
                              <label>Adresse*<span class="text-danger">*</span></label>
                               <textarea name="address" class="form-control" value="" readonly><?php echo $edit_driver->address; ?></textarea>
                                <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Postleitzahl<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="post_code" value="<?php echo $edit_driver->post_code; ?>"  readonly>
                                <?php echo form_error('post_code','<span class="text-danger">','</span>'); ?>
                           </div>
                           
                        
                            <div class="col-md-4 col-sm-4">
                             <label>Stadt<span class="text-danger"> *</span></label>
                                <input name="city" class="form-control" type="text" id="city" value="<?php echo $edit_driver->city; ?>" readonly/>
                                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>
                           </div>
                           </div>
                       </div>
                       <div class="row">
                         <div class="form-group"> 
                         <div class="col-md-4 col-sm-4">
                            <img src="<?php echo $edit_driver->driver_image; ?>" height="100px" width="120px">
                        
                           </div>
                           <div class="col-md-4 col-sm-4">
                             <label>Status<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control" readonly>
                                    <option <?php if($edit_driver->status == '1'){echo 'selected';}?> value="1">Active</option>
                                    <option <?php if($edit_driver->status == '0'){echo 'selected';}?> value="0">Inactive</option>
                                </select>
                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>
                       </div>
                       <br>
                  <div class="row">
                     
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>admin/driver" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      
  
  
</div>
</div>
</div>
</div>
</div>
</div>
</section>
       </div>
       </div>
       </body>
<!-- /MIDDLE -->

<script type="text/javascript">
    $(function () {
       
        $('#emp_dob_i').datetimepicker({
             format: 'Y-M-D'
        });    
    });

    function getStateList(country_id)
    {
        var str = 'country_id='+country_id;
        var PAGE = '<?php echo base_url(); ?>client/getStateList';
        
        jQuery.ajax({
            type :"POST",
            url  :PAGE,
            data : str,
            success:function(data)
            {           
                if(data != "")
                {
                    $('#client_state_id').html(data);
                }
                else
                {
                    $('#client_state_id').html('<option value=""></option>');
                }
            } 
        });
    }
</script>