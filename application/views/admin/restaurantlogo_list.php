<!DOCTYPE html>
<html>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            firmen Logo Liste
            <small>firmen Logo Liste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">firmen Logo Liste</h3>
                  <div class="pull-right box-tools">
                  <a href="<?php echo base_url();?>admin/Pagedata/addrestaurantlogo" class="btn btn-info btn-sm">Neu hinzufügen</a> 
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
              
                  <table id="the_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					              <th>No</th>
                        <th>Bild</th>                                  
                        <th>Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
             <?php
                $i=1; 
				   foreach($refelist as $value)
                    {                  
                      ?>
                        <tr>
						  <td><?php echo $i; ?></td>
                          <td><img src="<?php echo $value->rs_logo; ?>" height="100px" width="100px"> </td>                                         
                          <td>
                             <a class="confirm" onclick="return delete_refedata('<?php echo $value->rs_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                        </tr>
                      <?php
                      $i++;   }
            
              ?>                       
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
">
    
</script>
<script type="text/javascript">
    function delete_refedata(driverid)
    {
          
          bootbox.confirm("Are you sure you want to delete Restaurant Logo",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>admin/Pagedata/delete_restlogo/"+driverid;
            }
        });
    } 
</script>

<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
td.sorting_1 {
    width: 7% !important;
}
.table td img {
    height: 53px;
}
</style>
