<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Live Location
            <small>Live Location</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Live Location</h3>
                </div>
               <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                <input id="phpObj" type="hidden" data-items='<?php echo $details; ?>'>
                <div id="dialogMap">
            <div id="dvMap" style="height: 280px; width: 1000px;">
            </div>
           </div>
              </div>
                         
         </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
<script type="text/javascript">
$( document ).ready(function() {
      
        var lat_lng  = jQuery.parseJSON($('#phpObj').attr("data-items"));
        console.log(lat_lng);
         $("#dialogMap").dialog({
               modal: true,
               title: "Google Map",
               width: 500,
               height: 'auto',
               buttons: {
                  Close: function () {
                     $(this).dialog('close');
                  }
               },
               open: function () {
                  var mapOptions = {
                     center: new google.maps.LatLng(lat_lng[0][0], lat_lng[0][1]),
                     zoom: 15,
                     mapTypeId: google.maps.MapTypeId.ROADMAP
                  }
                  map = new google.maps.Map($("#dvMap")[0], mapOptions);
                  
                  var flightPlanCoordinates = [];
                  
                  for(i=0;i<lat_lng.length;i++)
                  {
                     var latlngset    = new google.maps.LatLng(lat_lng[i][0], lat_lng[i][1]);
                      placeMarkers(latlngset,'',lat_lng[i][2],i+1);
                     
                            flightPlanCoordinates.push(latlngset);
                  }
                  var flightPath = new google.maps.Polyline({
                      path: flightPlanCoordinates,
                      geodesic: true,
                      strokeColor: '#3C8DBC',
                      strokeOpacity: 1.0,
                      strokeWeight: 2
                     });
               
                     flightPath.setMap(map);
               }
            });
      });
</script>
<!-- /* Marker Function */ -->
<script type="text/javascript">
      function placeMarkers(myLocation,titleString,contentString,iconNo)
      {
      var marker = new google.maps.Marker({
         position: myLocation,
         //title: titleString
         animation: google.maps.Animation.DROP,
         icon:'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+iconNo+'|3C8DBC|FFF',
      });
      var infowindow = new google.maps.InfoWindow({
         content: contentString,
         
        });
        marker.addListener('click', function() {
         infowindow.open(map, marker);
        });
      marker.setMap(map);
      }
</script>
<style>
.content-wrapper {
     min-height: 901px !important;
}
    .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons.ui-draggable.ui-resizable {
    left: 252px !important;
    width: 100% !important;

}
.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix {
    float: left;
    margin-top: 40px;
    background-color: #00c0ef;
    border-color: #00acd6;
}
div#dvMap {
    width: 80% !important;
    height: 420px !important;
        top: 20px;
}
.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix.ui-draggable-handle {
    display: none;
}
.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix button {
    background-color: #3c8dbc;
    color: #fff;
    border: 1px solid #3c8dbc;
}
.box {
 
    height:525px;
}
</style>

