<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Restaurant-Logo hinzufügen
            <small>Restaurant-Logo hinzufügeno</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>  Zuhause</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Restaurant-Logo hinzufügen</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>admin/Pagedata/restaurantlogoList" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>

            <div class="panel-body">
               <form action="<?php echo base_url();?>admin/Pagedata/restlogopost" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
                  
              
                       <div class="row">     
						
                         <div class="col-md-4 col-sm-12">
                              <label>Bild <span class="text-danger"> *</span></label>
                                <input type="file" name="image" class="form-control" id="image" value="<?php echo set_value('image'); ?>" />
                                <?php echo form_error('image','<span class="text-danger">','</span>'); ?>
                           </div>
                          
                           </div>
                       </div>
                       <br>
                  <div class="row">
                    <div class="form-group"> 

                     <div class="col-md-1">
                        <button type="submit" name="submit" value="Add" class="btn btn btn-success margin-top-30">Hinzufügen</button>
                     </div>
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>admin/Pagedata/restaurantlogoList" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>
                     </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

<script type="text/javascript">
    $(function () {
       
        $('#emp_dob_i').datetimepicker({
             format: 'Y-M-D'
        });    
    });

    function getStateList(country_id)
    {
        var str = 'country_id='+country_id;
        var PAGE = '<?php echo base_url(); ?>client/getStateList';
        
        jQuery.ajax({
            type :"POST",
            url  :PAGE,
            data : str,
            success:function(data)
            {           
                if(data != "")
                {
                    $('#client_state_id').html(data);
                }
                else
                {
                    $('#client_state_id').html('<option value=""></option>');
                }
            } 
        });
    }
</script>