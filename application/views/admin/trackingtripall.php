<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
         Live Ampel System
            <small>Live Ampel System</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Zuhause</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">              

<style>
#map_canvas {
    height: 500px;
    width: 100%;
    margin: 0px;
    padding: 0px
}
img.mapuser {
    margin-left: 10px;
    border-radius: 50%;
}
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgGHa9xRofBw7WY4zcUj9hXyQWCk4-Qtc"></script>
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
      
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Live Ampel System</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url(); ?>admin/dashboard" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>     
			
			<div id="map" style="width:100%; height:450px; border: 2px solid #3872ac;"></div>
         </div>
		 
		 <div class="box-body">
              
                  <table id="the_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Strasse</th>
                        <th>Stadt</th>
                        <th>Entfernen</th>                      
                        </tr>
                    </thead>
                    <tbody>
                   <?php
                    $i=1;
                    foreach($trackingtrip as $value)
                    {                  
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->first_name;   echo $value->surname;?></td>
                          <td><?php echo $value->live_address;; ?></td>
                          <td><?php echo $value->live_city; ?></td>
                          <td><?php echo $value->live_zip_code; ?></td>
                        </tr>
                      <?php
                      $i++;
                         }
            
              ?>                       
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

<script type="text/javascript">
var locations = [
            <?php foreach($trackingtrip as $tel){
             if($tel->parcel_status==2){
         $img = "".base_url()."front_assets/img/green.png"; 
       }
       if($tel->parcel_status==4){
         $img = "".base_url()."front_assets/img/orange.png";
       }
       if($tel->parcel_status==5){
         $img = "".base_url()."front_assets/img/red.png";
       }
       if($tel->parcel_status==3){
         $img = "".base_url()."front_assets/img/orange.png";
       }
       if($tel->parcel_status==""){
         $img = "".base_url()."front_assets/img/red.png";
       }
			 /* if($tel->driver_image!=""){
				 $imgu = "<img src='".base_url()."front_assets/img/red.png'>";
			 }
			 else {
				 $imgu = "<img src='".base_url()."front_assets/img/red.png'>"; 
			 } */
			 
			 if($tel->driver_image!=""){
				 $imgu = $tel->driver_image;
			 }
			 else {
				 $imgu = "".base_url()."image/driver/dummy.png"; 
			 }
			?>
			["<?php echo $tel->first_name; ?>  <?php echo $tel->surname; ?>", <?php echo $tel->location_lat; ?>, <?php echo $tel->location_long; ?>, "<?php echo $img; ?>", '<?php echo $imgu; ?>',],
			<?php } ?>
            /* ['Purana Qila', 28.618174, 77.242686, 2],
            ['Red Fort', 28.663973, 77.241656, 3],
            ['India Gate', 28.620585, 77.228609, 4],
            ['Jantar Mantar', 28.636219, 77.213846, 5],
            ['Akshardham', 28.622658, 77.277704, 6] */
        ];
		
	  		
		 var image1 = "<?php echo base_url(); ?>front_assets/img/green.png";
		
		 var image2 = "<?php echo base_url(); ?>front_assets/img/orange.png";
		
		 var image3 = "<?php echo base_url(); ?>front_assets/img/red.png";  
	 
		  var latlng = new google.maps.LatLng(0, 0);
        function InitMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 6,
                center: new google.maps.LatLng(46.8182, 8.2275),
				//center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();

           var status;
            var marker, i;
            for (i = 0; i < locations.length; i++) {
				var contentString = '<div id="content"><h1>' +locations[i][0] +
                        '</h1><img src='+locations[i][4] +'></div>';

                marker = new google.maps.Marker({
				/* status = locations[i][3];	
				if(status==2){
					var img = image1;	
					}
					if(status==4){
					var img =  image2;	
					}
					if(status==5){
					var img =  image3;	
					}  */
				
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					 map: map,
					 icon: locations[i][3], 
                   
                });
				marker.content = '<div id="content"><img src='+locations[i][4] +' class="mapuser" height="80px" width="80px"><h1 style="text-align: center;font-size: 12px;text-transform: capitalize;font-weight: bold;">' +locations[i][0] +
                        '</h1></div>';
						
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        //infowindow.setContent(locations[i][0]);
						infowindow.setContent(marker.content);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

</script>