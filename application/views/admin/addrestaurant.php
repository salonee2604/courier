<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Firma hinzufügen
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Heim</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Firma hinzufügen</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>admin/restaurant" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>

            <div class="panel-body">
               <form action="<?php echo base_url();?>admin/restaurant/addRestaurant" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
                   <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Vorname<span class="text-danger"> *</span></label>
                                <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" placeholder="Vorname"/>
                                <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Name</label><span class="text-danger"> *</span></label>
                                <input name="surname" class="form-control" type="text" id="surname" value="<?php echo set_value('surname'); ?>" placeholder="Name"/>
                                <?php echo form_error('surname','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Firma Name<span class="text-danger"> *</span></label>
                                <input name="restaurant_name" class="form-control" type="text" id="restaurant_name" value="<?php echo set_value('restaurant_name'); ?>" placeholder="Firma Name"/>
                                <?php echo form_error('restaurant_name','<span class="text-danger">','</span>'); ?>
                           </div>
                          </div>
                       </div>
                       <div class="row">
                         <div class="form-group">
                             <div class="col-md-4 col-sm-4">
                              <label>Telefonnummer<span class="text-danger"> *</span></label>
                                <input name="mobile_number" class="form-control" type="text" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>" minlength="10"  placeholder="Telefonnummer"/>
                                <?php echo form_error('mobile_number','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Passwort<span class="text-danger"> *</span></label>
                                <input type="password" name="password" class="form-control" id="password" value="<?php echo set_value('password'); ?>" placeholder="Passwort"/>
                                <?php echo form_error('password','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Passwort bestätigen<span class="text-danger"> *</span></label>
                                <input type="password" name="c_password" class="form-control" id="c_password" value="<?php echo set_value('client_conf_password'); ?>" placeholder="Passwort bestätigen"/>
                                <?php echo form_error('c_password','<span class="text-danger">','</span>'); ?>
                           </div>
                         </div>
                       </div>                      
                       <div class="row">
                         <div class="form-group"> 
                            <div class="col-md-4 col-sm-4">
                              <label>E-Mail<span class="text-danger"></span></label>
                                <div class="fancy-file-upload fancy-file-primary">
                                  <input type="email" placeholder="E-Mail" class="form-control" name="email" onchange="jQuery(this).next('input').val(this.value);">
                                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>

                                 </div>                                
                            </div> 
                            <div class="col-md-4 col-sm-4">
                             <label>Stadt<span class="text-danger"> *</span></label>
                                <input name="city" class="form-control" type="text" id="city" value="<?php echo set_value('city'); ?>" placeholder="Stadt"/>
                                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>
                           </div>
                           </div>
                          <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Postleitzahl<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="post_code" value="<?php echo set_value('post_code'); ?>"  placeholder="Postleitzahl">
                                <?php echo form_error('post_code','<span class="text-danger">','</span>'); ?>
                           </div>
                          </div>
                       </div>
                       <div class="row">
                        <div class="form-group"> 
                        <div class="col-md-4 col-sm-4">
                              <label>Adresse<span class="text-danger">*</span></label>
                              <input type="text" class="form-control pac-target-input" name="address" value="" id="autocomplete1" value="" required="required" onfocus="geolocate()" placeholder="Adresse" autocomplete="off">
                                  <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                             <label>Status<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Aktiv</option>
                                    <option value="0">Inaktiv</option>
                                </select>
                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                          
                         <div class="col-md-4 col-sm-4">
                              <label>Bild<span class="text-danger"> *</span></label>
                                <input type="file" name="image" class="form-control" id="image" value="<?php echo set_value('image'); ?>" />
                                <?php echo form_error('image','<span class="text-danger">','</span>'); ?>
                           </div>
                           </div>
                       </div>
                       <input type="hidden" name="latitude" id="pickup_latitude" value="">
                       <input type="hidden" name="longitude" id="pickup_longitude" value="">
                 <div class="row">
                     <div class="col-md-4">
                      <h4><b>Bankverbindung</b></h4>
                      </div>
                 </div>       
			   <div class="row">
			   <div class="col-md-4">
			       <label>Name auf der Karte<span class="text-danger"> *</span></label>
                 <input name="name_holder" class="form-control" type="text" id="name_holder" value="<?php echo set_value('name_holder'); ?>" placeholder="Name auf der Karte"/>
                <?php echo form_error('name_holder','<span class="text-danger">','</span>'); ?>
              </div>
			  
			   <div class="col-md-4">
			       <label>Kartennummer<span class="text-danger"> *</span></label>
                 <input name="card_no" class="form-control" type="text" id="card_no" value="<?php echo set_value('card_no'); ?>" 
                 placeholder="Kartennummer"/>
                <?php echo form_error('card_no','<span class="text-danger">','</span>'); ?>
              </div>
			

			   <div class="col-md-4">
			     <label>Verfallsdatum<span class="text-danger"> *</span></label>
                 <input name="expire_date" class="form-control" type="text" id="expire_date" value="<?php echo set_value('expire_date'); ?>" placeholder="Verfallsdatum -02/2021"/>
                <?php echo form_error('expire_date','<span class="text-danger">','</span>'); ?>
              </div>
                        </div> 
                        <div class="row">
                         <div class="form-group">
                          <div class="col-md-4">
                 <label>Sicherheitscode<span class="text-danger"> *</span></label>
                 <input name="security_code" class="form-control" type="text" id="security_code" value="<?php echo set_value('security_code'); ?>" placeholder="Sicherheitscode"/>
                <?php echo form_error('security_code','<span class="text-danger">','</span>'); ?>
              </div>
 
                         </div>
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Bank Name<span class="text-danger">*</span></label>
                               <textarea name="bank_details" class="form-control" value="" placeholder="Bank Name"></textarea>
                                <?php echo form_error('bank_details','<span class="text-danger">','</span>'); ?>
                           </div>  
                        </div>
                       </div>
                       <br>
                  <div class="row">
                    <div class="form-group"> 

                     <div class="col-md-1">
                        <input type="submit" name="addresturentsubmit" value="Hinzufügen" class="btn btn btn-success margin-top-30">
                     </div>
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>admin/restaurant" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>
                     </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/PlacePicker.js"></script>
  <script type="text/javascript">
    

      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;



      var componentForm = {



        //street_number: 'short_name',



        //route: 'long_name',



        //locality: 'long_name',



        //administrative_area_level_1: 'short_name',



        //country: 'long_name',



        //postal_code: 'short_name'



      };



      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),



          {types: ['geocode']});
        autocomplete1 = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),



          {types: ['geocode']});  



        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);
        autocomplete1.addListener('place_changed', fillInAddress);


    }
      


    function fillInAddress() 
    {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();



        for (var component in componentForm) {



          document.getElementById(component).value = '';



          document.getElementById(component).disabled = false;



        }



        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



          var addressType = place.address_components[i].types[0];



          if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



      //alert(val);



    }



  }



}



      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      function geolocate() 
      {


  
        if (navigator.geolocation) {



          navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
             
              

            };
            
            var pickuplat = geolocation.lat;
            var pickuplong = geolocation.lng;
            $('#pickup_latitude').val(pickuplat); 
            $('#pickup_longitude').val(pickuplong); 

            var circle = new google.maps.Circle({



              center: geolocation,



              radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



          });



        }



      }
       

  </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
     
    


<script type="text/javascript">
    $(document).ready(function(){
      $("#pickup").PlacePicker({
        btnClass:"btn btn-xs btn-default",
        key:"AIzaSyBAGm4WfH69Ny5DmWFAA88s26UwiNqHMn8",
        center: {lat: 17.6868, lng: 83.2185},
        success:function(data,address){
          //data contains address elements and
          //address conatins you searched text
          //Your logic here
          $("#pickup").val(data.formatted_address);
        }
      });
    });
</script>

<script>
/*
$("#pickup_country").PlacePicker({

  key:"AIzaSyB6jpjQRZn8vu59ElER36Q2LaxptdAghaA"

});

$("#pickup_country").PlacePicker({

  title: "Popup Title Here"

});

$("#pickup_country").PlacePicker({

  btnClass: "btn btn-secondary btn-sm"

});

$("#pickup_country").PlacePicker({

    center: {lat: -34.397, lng: 150.644}



});

$("#pickup_country").PlacePicker({

   zoom: 10

});

$("#pickup_country").PlacePicker({

  success:function(data,address){

    //data contains address elements and

    //address conatins you searched text

    //Your logic here

    $("#pickup_country").val(data.formatted_address);

  }

});

*/

</script>