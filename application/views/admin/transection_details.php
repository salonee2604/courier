<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Transektionsdetails<small>MemberShip List</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Transektionsdetails</h3>
                  <div class="pull-right box-tools">
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                <a href="<?php echo base_url(); ?>admin/restaurant/membership" class="btn btn-info btn-sm pull-right box-tools">Zurück</a>
                <br>
                <br>
                  <table id="the_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>Firma Name</th>
                        <th>Txn-ID</th>
                        <th>Zahlung brutto</th>
                        <th>Währung</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Status</th>
                   </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach ($membership_details as $resturent_details)
                        {
                          ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          
                          <td><?php echo $resturent_details->restaurant_name; ?> </td>
                          <td><?php echo $resturent_details->txn_id; ?></td>
                          <td><?php echo $resturent_details->payment_gross; ?></td>
                          <td><?php echo $resturent_details->currency_code; ?></td>
                          <td>
                          <?php echo $resturent_details->payer_name; ?>
                          </td>
                          <td><?php echo $resturent_details->payer_email; ?></td>
                          <td><?php echo $resturent_details->status; ?></td>
                        </tr>
                        <?php
                        $i++;
                        }
                    ?>

            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>

