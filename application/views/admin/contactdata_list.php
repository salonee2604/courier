<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Kontaktliste
            <small>Kontaktliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Kontaktliste</h3>
                  
                </div><!-- /.box-header -->
            <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="the_table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>S.NO</th> 
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Betreff</th>
						            <th>Nachricht</th>
                        <th>Erstellungsdatum</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($contactdata as $value)
                        {
                            
                          
                        ?>
                         <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->c_name; ?></td>
                          <td><?php echo $value->c_email; ?></td>
                          <td><?php echo $value->c_subject; ?></td>
						              <td><?php echo $value->c_message; ?></td>
                          <td><?php echo $value->create_dt; ?></td>
                   
                        </tr>
                      
                      <?php
                      $i++;
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
  