<!DOCTYPE html>

<html>

  

  <body class="skin-blue sidebar-mini">

    <div class="wrapper">

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->

        <section class="content-header">

          <h1>

              Profilaktualisierung

            <small>Profilaktualisierung</small>

          </h1>

          <ol class="breadcrumb">

            <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Heim</a></li>



          </ol>

        </section>

         <br>

        <!-- Main content -->

        <section class="content">

          <div class="row">

            <div class="col-xs-12">

              <div class="box">

              <div id="content" class="padding-20">

   <div class="row">

      <div class="col-md-12">

         <div class="panel panel-default">

            <div class="panel-heading panel-heading-transparent">

               <strong>Profilaktualisierung</strong>

                 <div class="pull-right box-tools">

                    <a href="<?php echo base_url();?>admin/dashboard" class="btn btn-info btn-sm">Zurück</a>                           

                </div>

                <br>

            <br>

            </div>

              <div id="msg_div">

                 <?php echo $this->session->flashdata('message');?>

                </div>

             <div class="panel-body">

               <form action="<?php echo base_url(); ?>admin/restaurant/profile/<?php echo $restaurent_details->restaurant_id; ?>" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">



                   <div class="row">

                        <div class="form-group">

                           <div class="col-md-3 col-sm-3">

                              <label>Vorname<span class="text-danger"> *</span></label>

                                <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo $restaurent_details->first_name; ?>" />

                                <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?>

                           </div> 

                            <div class="col-md-3 col-sm-3">

                              <label>Email<span class="text-danger"></span></label>

                                <div class="fancy-file-upload fancy-file-primary">

                                  <input type="email" class="form-control" name="email" value="<?php echo $restaurent_details->email; ?>" readonly>

                                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>



                                 </div>                                

                            </div>

                            <div class="col-md-3 col-sm-3">

                             <label>Passwort<span class="text-danger"> *</span></label>

                                <input name="password" class="form-control" type="password" id="password" value=""/>

                                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>

                           </div>

                           <div class="col-md-3 col-sm-3">

                                <label><span class="text-danger">*</span></label>

                             <img src="<?php echo $restaurent_details->restaurant_image ?>" height="100px" width="100px">

                            </div>

                            </div>

                            </div>



                         </div>

                       </div>

                       



                       <br>

                  <div class="row">

                   <div class="form-group">

                     <div class="col-md-1">

                         <input type="submit" name="submit" value="Bearbeiten" class="btn btn-success margin-top-30">

                     </div>

                    <div class="col-md-1">

                     <a href="<?php echo base_url();?>admin/restaurant" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>

                     </div>

                     </div>

                  </div>

               </form>

            </div>

         </div>

      

      

  

  

</div>

</div>

</div>

</div>

</div>

</div>

</section>

</div>

</div>

</body>

<!-- /MIDDLE -->



<script type="text/javascript">

    $(function () {

       

        $('#emp_dob_i').datetimepicker({

             format: 'Y-M-D'

        });    

    });



    function getStateList(country_id)

    {

        var str = 'country_id='+country_id;

        var PAGE = '<?php echo base_url(); ?>client/getStateList';

        

        jQuery.ajax({

            type :"POST",

            url  :PAGE,

            data : str,

            success:function(data)

            {           

                if(data != "")

                {

                    $('#client_state_id').html(data);

                }

                else

                {

                    $('#client_state_id').html('<option value=""></option>');

                }

            } 

        });

    }

</script>