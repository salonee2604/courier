<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Mitgliederliste<small>Mitgliederliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Mitgliederliste</h3>
                  <div class="pull-right box-tools">
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
              
                  <table id="the_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>Firma Name</th>
                        <th>Telefonnummer</th>
                        <th>E-Mail</th>
                        <th>Admin Status</th>
                        <th>Erstellungsdatum</th>
                        <th>Kostenlose Testversion</th>
                        <th>Aktion</th>
                        <th>Zahlungsdetails</th>
                   </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach ($membership_details as $resturent_details)
                        {
                          ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          
                          <td><?php echo $resturent_details->restaurant_name; ?> </td>
                          <td><?php echo $resturent_details->mobile_number; ?></td>
                          <td><?php echo $resturent_details->email; ?></td>
                          <td>
                           <?php 
                              if($resturent_details->admin_approved_status == '1')
                              { 
                                ?>
                                <button class="btn btn-success">Freigegeben</button>
                               <?php 
                              }
                              else
                              {
                                ?>
                                <button class="btn btn-info btn btn-danger">Nicht genehmigt</button>
                               <?php    
                              }
                              ?>
                          </td>
                          
                          <td><?php echo $resturent_details->created_date; ?></td>
                          <td>
                          <?php 
                            $date =  $resturent_details->created_date;
                            echo date('Y-m-d', strtotime($date. ' + 14 days'));
                          ?>
                          </td>
                           <td> 
                             <a href="<?php echo base_url();?>admin/restaurant/edit_membership/<?php echo $resturent_details->restaurant_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>
                          </td>
                          <td> 
                              <a href="<?php echo base_url(); ?>admin/restaurant/transactionDetails/<?php echo $resturent_details->restaurant_id; ?>" class="btn btn-primary">Transaktionsdetails</a>
                          </td>
                        
                        </tr>
                        <?php
                        $i++;
                        }
                    ?>

            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>

