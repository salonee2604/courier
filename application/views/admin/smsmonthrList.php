<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sms Month List
            <small>Sms Month List</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Sms Month List</h3>
                  <div class="pull-right box-tools">
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
              
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Januar</th>
                        <th>Februar</th>
                        <th>März</th>
                        <th>April</th>
                        <th>Mai</th>
                        <th>Juni</th>
                        <th>Juli</th>
                        <th>August</th>
                        <th>September</th>
						<th>Oktober</th>
						<th>November</th>
						<th>Dezember</th>
                        </tr>
                    </thead>
                    <tbody>
                   <?php
                    $i=1;					
                    foreach($smscoundata as $value)
                    {                  
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value['january']; ?></td>
                          <td><?php echo $value['february']; ?></td>
                          <td><?php echo $value['march']; ?></td>
                          <td><?php echo $value['april']; ?></td>
						  <td><?php echo $value['may']; ?></td>
                         <td><?php  echo $value['june']; ?></td>
						 <td><?php  echo $value['july']; ?></td>
						 <td><?php  echo $value['august']; ?></td>
						 <td><?php  echo $value['september']; ?></td>
						 <td><?php  echo $value['october']; ?></td>
						  <td><?php echo $value['november']; ?></td>
						   <td><?php echo $value['december']; ?></td>
                        </tr>
                      <?php
                      $i++;
                         }
            
              ?>                       
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
">
    
</script>


<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>
