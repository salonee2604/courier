<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Reise hinzufügen 
            <small>Reise hinzufügen </small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Heim</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Reise hinzufügen </strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>restaurant/trip" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>

            <div class="panel-body">
               <form action="<?php echo base_url();?>restaurant/trip/addTrip" method="post" enctype="multipart/form-data">
                  <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Name<span class="text-danger"> *</span></label>
                                <input name="name" class="form-control" type="text" id="name" value="<?php echo set_value('name'); ?>" />
                                <?php echo form_error('name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Tour Name</label></label>
                                <input name="brand" class="form-control" type="text" id="brand" value="<?php echo set_value('brand'); ?>" />
                                <?php echo form_error('brand','<span class="text-danger">','</span>'); ?>
                           </div>

                           <div class="col-md-4 col-sm-4">
                              <label>Fahrer auswählen<span class="text-danger"> *</span></label>
                                <select class="form-control"  name="driver_id" id="driver_id">
                                    <option value=""></option>
                                    <?php 
                                        foreach ($driver_list as $value)
                                        {
                                            ?>
                                            <option value="<?php echo $value->driver_id; ?>"><?php echo $value->first_name.' '.$value->surname ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                           </div>
                           
                        
                           
                          
                         </div>
                       </div>
                       <div class="row">
                         <div class="form-group"> 
                         <div class="col-md-4 col-sm-4">
                             <label>Status<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Aktiv</option>
                                    <option value="0">Inaktiv</option>
                                </select>
                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>                           
                           <div class="form-group"> 
                          <div class="col-md-4 col-sm-4">
                             <label>Paket auswählen<span class="text-danger">*</span></label> 
                                <select class="selectpicker form-control"  name="parcel_id[]" id="parcel_id" multiple>
                                    <option>Mehrere Pakete auswählen</option>
                                    <?php 
                                        foreach ($parcel_details as $value)
                                        {
                                        $parcel_id          = $value->parcel_id;
                                        
                                        $customer_details  = $this->Trip_model->getCustomerByParcelId($parcel_id);
                                           if(!empty($customer_details->first_name))
                                           {


                                            ?>
                                            <option value="<?php echo $value->parcel_id; ?>"><?php echo $customer_details->first_name.$customer_details->last_name ?></option>
                                      <?php
                                        }
                                        }
                                    ?>
                                </select>
                               <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>
        
        
                       </div>                      
                       <br>
                  <div class="row">
                     <div class="col-md-1">
                        <input type="submit" name="addtripsubmit" value="hinzufügen" class="btn btn btn-success margin-top-30">
                     </div>
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>restaurant/trip" ><button type="button" class="btn btn-danger margin-top-30 ">Stornieren</button></a>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<!-- /MIDDLE -->

