<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Kundenliste
            <small>Kundenliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Zuhause</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Kundenliste</h3>
                  
                </div><!-- /.box-header -->
            <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="the_table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Name des Kunden</th>
                        <th>Nachname des Kunden</th>
                        <th>Kundennummer</th>
                        <th>Kundenadresse</th>
                        <th>Erstelldatum</th>
                        <th>Status</th>
                        <th>Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach($customer_details as $value)
                        {
                            
                          
                        ?>
                         <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->first_name; ?></td>
                          <td><?php echo $value->last_name; ?></td>
                          <td><?php echo $value->customer_number; ?></td>
                          <td><?php echo $value->customer_address; ?></td>
                          <td><?php echo $value->created_date; ?></td>
                          <td>
                              <?php
                              if($value->status == 1)
                              {
                                 ?>
                                 <button class="btn btn-success">Aktiv</button>
                                 <?php
                              }
                              else
                              {
                                 ?>
                                    <button class="btn btn-danger">InAktiv</button>
                                 <?php                              
                                  
                              }
                              ?>
                          </td>
                        <td><a href="<?php echo base_url(); ?>restaurant/customer/orderList/<?php echo $value->customer_number; ?>"><button class="btn btn-success">Aufträge</button></a>
                        </td>
                        </tr>
                      
                      <?php
                      $i++;
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap 4 dependency -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- bootbox code -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js
"></script>
    
<script type="text/javascript">
    function delete_parcel(parcel_id)
    {
          bootbox.confirm("Are you sure you want to delete Parcel Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>admin/parcel/delete_Parcel/"+parcel_id;
            }
        });
    } 
</script>
<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>