<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
         Reiseansicht   
            <small>Reiseansicht</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Zuhause</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Zuhause</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>restaurant/trip" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>
    
            <div class="panel-body">
               <form action="<?php echo base_url();?>admin/trip/addTrip/<?php echo $edit_trip->trip_id; ?>" method="post" enctype="multipart/form-data">
                  <!--  <div class="row">
                       <div class="col-md-4">
                           <img src="<?php echo base_url(); ?>image/<?php echo $img_url; ?>" height="100px" width="100px">
                       </div>
                   </div> -->
                  <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Name<span class="text-danger"> *</span></label>
                                <input name="name" class="form-control" type="text" id="name" value="<?php echo $edit_trip->name; ?>" readonly/>
                                <?php echo form_error('name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Tour Name</label><span class="text-danger"> *</span></label>
                                <input name="brand" class="form-control" type="text" id="brand" value="<?php echo $edit_trip->brand ?>" readonly/>
                                <?php echo form_error('brand','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Fahrer auswählen<span class="text-danger"> *</span></label>
                                <select class="form-control"  name="driver_id" id="driver_id" readonly>
                                    <option value="">Select One</option>
                                    <?php 
                                        foreach ($driver_list as $value)
                                        {
                                            ?>
                                            <option <?php if($value->driver_id == $edit_trip->driver_id){echo 'selected';} ?>  value="<?php echo $value->driver_id; ?>"><?php echo $value->first_name.' '.$value->surname ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                           </div>
                           
                         </div>
                           
                         </div>
                       </div>

                       
                       <div class="row">
                         
                         <div class="form-group"> 
                          <div class="col-md-4 col-sm-4">
                             <label>Status<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control" readonly>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>
                            <div class="form-group"> 
                          <div class="col-md-4 col-sm-4">
                             <label>Paket auswählen<span class="text-danger">*</span></label>
                             <?php 
                             $trip_parcel_id       = $edit_trip->parcel_id;
                             $trip_parcel_id_array = explode(",", $trip_parcel_id);
                            ?>
                                <select class="selectpicker form-control"  name="parcel_id[]" id="parcel_id" multiple readonly>
                                    <option value="">Select One</option>
                                    <?php 
                                       
                                        foreach ($parcel_details as $value)
                                        {
                                            
                                            $parcel_id          = $value->parcel_id;
                                        $customer_details  = $this->Trip_model->getCustomerByParcelId($parcel_id);
                                          if(!empty($customer_details->last_name))
                                         {
                                            ?>
                                            <option <?php if (in_array($value->parcel_id, $trip_parcel_id_array)){echo "selected";}?> value="<?php echo $value->parcel_id; ?>"><?php echo $customer_details->first_name.' '.$customer_details->last_name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                               <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>
        
        
                       </div> 
                       <div class="row">
                        <?php
                                       if(!empty($edit_trip->parcel_id))
                                       {
                                        $parcel_array = explode(",", $edit_trip->parcel_id);
                                        $customer_name = array();
                                        ?>
                                    <table id="the_table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                    <th style="text-align: center; width:20px;">S.No</th>
                                    <th style="text-align: center; width:20px;">Nachname des Kunden</th>
                                    <th style="text-align: center; width:20px;">Kundenadresse</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i =1;
                                       foreach ($parcel_array as $value_par) 
                                       {
                                          
                                          $customerdetails =     $this->db->get_where('customer',array('parcel_id'=>$value_par))->row();
                                          ?>
                                    <tr>
                                    <td scope="row" ><?php echo $i; ?></td>
                                    <td><?php echo $customerdetails->first_name.' '.$customerdetails->last_name; ?></td>
                                    <td> 
                                      <?php echo $customerdetails->customer_address; ?>
                                    </td>
                                    </tr>
                                    <?php
                                    $i++;     
                                       }
                                      
                                       ?>
                                    </tbody>
                                    </table> 
                                    <?php
                                       }

                                           ?>
                                    <br>
                       </div>                     

                       <br>
                  <div class="row">
                    <div class="col-md-1">
                     <a href="<?php echo base_url();?>restaurant/trip" ><button type="button" class="btn btn-danger margin-top-30 ">Stornieren</button></a>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

<script type="text/javascript">
    $(function () {
       
        $('#emp_dob_i').datetimepicker({
             format: 'Y-M-D'
        });    
    });

    function getStateList(country_id)
    {
        var str = 'country_id='+country_id;
        var PAGE = '<?php echo base_url(); ?>client/getStateList';
        
        jQuery.ajax({
            type :"POST",
            url  :PAGE,
            data : str,
            success:function(data)
            {           
                if(data != "")
                {
                    $('#client_state_id').html(data);
                }
                else
                {
                    $('#client_state_id').html('<option value=""></option>');
                }
            } 
        });
    }
</script>