 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <a href="<?php echo base_url(); ?>restaurant/dashboard">
              <img src="<?php echo $_SESSION['web_admin'][0]->restaurant_image; ?>" class="img-circle" alt="User Image" height="50px;" width="50px;"/>
              </a>
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['web_admin'][0]->restaurant_name; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
         
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
            <li class="active treeview">
              <a href="<?php echo base_url(); ?>restaurant/driver">
                <!-- <i class="fa fa-taxi"></i> -->
                <i class="text_res">FL</i>
                <span>Fahrerliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>

            <li class="active treeview">
              <a href="<?php echo base_url(); ?>restaurant/parcel">
                <!-- <i class="fa fa-gift"></i> -->
                <i class="text_res">PL</i>
                <span>Paketliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>restaurant/message/connectNearestDriver">
                <!-- <i class="fa fa-map-marker"></i> -->
                <i class="text_res">OM</i>
                <span>Organisation‘s Karte</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>restaurant/trip">
                <!-- <i class="fa fa-plane"></i> -->
                <i class="text_res">RL</i>
                <span>Reiseliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
             <li class="active reeview">
              <a href="<?php echo base_url(); ?>restaurant/message/liveLocationall">
                <!-- <i class="fa fa-map-marker"></i> -->
                <i class="text_res">LAS</i>
                <span>Live Ampel System</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
            <li class="active reeview">
              <a href="<?php echo base_url(); ?>restaurant/customer">
                <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                <i class="text_res">KL</i>
                <span>Kundenliste</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
           
           
			
           
             <li class="active reeview">
              <a href="<?php echo base_url(); ?>restaurant/message/connected_driver_list">
                <!-- <i class="fa fa-map-marker"></i> -->
                 <i class="text_res">VF</i>
                <span>Verbundene Fahrer</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
             <li>
            <a href="<?php echo base_url(); ?>restaurant/Membership">

                <!-- <i class="fa fa-map-marker"></i> -->
                 <i class="text_res">E</i>
                <span>Einstellungen</span>
                <span class="label label-primary pull-right"></span>
              </a>
                
            </li>
            

           </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

      <script>
  $( document ).ready(function() 
  {
      $(".sidebar-toggle").on('click', function(event)
      {
        $(".text_res").toggle();
        // $(".icon_hide").hide();
        // $(".fa").show();
      });
      $(".icon_hide").show();
      $(".text_res").hide();
  });
</script>