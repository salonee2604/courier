<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Treiber hinzufügen
            <small>Treiber hinzufügen</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Heim</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Treiber hinzufügen</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>restaurant/driver" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>

            <div class="panel-body">
               <form action="<?php echo base_url();?>restaurant/driver/adddriver" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
                  
                    
                     <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Vorname<span class="text-danger"> *</span></label>
                                <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" />
                                <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Nachname</label><span class="text-danger"> *</span></label>
                                <input name="surname" class="form-control" type="text" id="surname" value="<?php echo set_value('surname'); ?>" />
                                <?php echo form_error('surname','<span class="text-danger">','</span>'); ?>
                           </div>
                            <div class="col-md-4 col-sm-4">
                              <label>Telefonnummer<span class="text-danger"> *</span></label>
                                <input name="mobile_number" class="form-control" type="text" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>"  maxlength="10"/>
                                <?php echo form_error('mobile_number','<span class="text-danger">','</span>'); ?>
                           </div>
                          
                         </div>
                       </div>
                       <div class="row">
                         <div class="form-group">
                              <div class="col-md-4 col-sm-4">
                              <label>E-Mail<span class="text-danger">*</span></label>
                                <div class="fancy-file-upload fancy-file-primary">
                                  <input type="email" id="email" class="form-control" name="email" onchange="jQuery(this).next('input').val(this.value);">
                                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>

                                 </div>                                
                            </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Passwort<span class="text-danger"> *</span></label>
                                <input type="password" name="password" class="form-control" id="password" value="<?php echo set_value('password'); ?>" />
                                <?php echo form_error('password','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Passwort bestätigen<span class="text-danger"> *</span></label>
                                <input type="password" name="c_password" class="form-control" id="c_password" value="<?php echo set_value('client_conf_password'); ?>" />
                                <?php echo form_error('c_password','<span class="text-danger">','</span>'); ?>
                           </div>
                           
                         </div>
                       </div>                      
                       <div class="row">
                         <div class="form-group">
                             <div class="col-md-4 col-sm-4">
                              <label>Adresse</label>
                               <textarea name="address" class="form-control" value=""></textarea>
                                <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Postleitzahl<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="post_code" value="<?php echo set_value('post_code'); ?>"  >
                                <?php echo form_error('post_code','<span class="text-danger">','</span>'); ?>
                           </div>
                            <div class="col-md-4 col-sm-4">
                             <label>Stadt<span class="text-danger">*</span></label>
                                <input name="city" class="form-control" type="text" id="city" value="<?php echo set_value('city'); ?>" />
                                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>
                           </div>
                           </div>
                          </div>
                       <div class="row">
                        <div class="form-group"> 
                         <div class="col-md-4 col-sm-4">
                              <label>Bild</label>
                                <input type="file" name="image" class="form-control" id="image" value="<?php echo set_value('image'); ?>" />
                                <?php echo form_error('image','<span class="text-danger">','</span>'); ?>
                           </div>
                          <div class="col-md-4 col-sm-4">
                             <label>Status<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Aktiv</option>
                                    <option value="0">Inaktiv</option>
                                </select>
                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
                           </div> 
                           </div>
                       </div>
                       <br>
                  <div class="row">
                    <div class="form-group"> 

                     <div class="col-md-1">
                        <input type="submit" name="adddriversubmit" value="Hinzufügen" class="btn btn btn-success margin-top-30">
                     </div>
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>restaurant/driver" ><button type="button" class="btn btn-danger margin-top-30 ">Abbrechen</button></a>
                     </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

<script type="text/javascript">
    $(function () {
       
        $('#emp_dob_i').datetimepicker({
             format: 'Y-M-D'
        });    
    });

    function getStateList(country_id)
    {
        var str = 'country_id='+country_id;
        var PAGE = '<?php echo base_url(); ?>client/getStateList';
        
        jQuery.ajax({
            type :"POST",
            url  :PAGE,
            data : str,
            success:function(data)
            {           
                if(data != "")
                {
                    $('#client_state_id').html(data);
                }
                else
                {
                    $('#client_state_id').html('<option value=""></option>');
                }
            } 
        });
    }
</script>