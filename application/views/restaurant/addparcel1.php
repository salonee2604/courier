
<!DOCTYPE html>
<html>
    <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add Parcel
            <small>Add Parcel</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                

 
<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
        
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Add Parcel</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>Parcel" class="btn btn-info btn-sm">Back</a>                           
                </div>
                <br>
                </br>
            </div>

            <div class="panel-body">
               <form action="<?php echo base_url();?>Parcel/addparcel" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
                  
                    
                     <div class="row">
                        <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Reciver Name<span class="text-danger"> *</span></label>
                                <input name="reciver_name" class="form-control" type="text" id="reciver_name" value="<?php echo set_value('reciver_name	'); ?>" />
                                <?php echo form_error('reciver_name','<span class="text-danger">','</span>'); ?>
                           </div> 
                           <div class="col-md-4 col-sm-4">
                              <label>Reciver Mobile Number</label><span class="text-danger"> *</span></label>
                                <input name="reciever_number" class="form-control" type="text" id="surname" value="<?php echo set_value('surname'); ?>" />
                                <?php echo form_error('reciever_number','<span class="text-danger">','</span>'); ?>
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <label>Pickup Location<span class="text-danger"> *</span></label>
                                <input type="text" name="pickup_location" class="form-control" id="drop_location" value="<?php echo set_value('drop_location'); ?>" />
                                <?php echo form_error('drop_location','<span class="text-danger">','</span>'); ?>
                                <div class="pac-card" id="pac-card">
                       </div>
                       </div>
                       </div>
                       </div>
                       <div class="row">
                         <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Drop Location<span class="text-danger"> *</span></label>
                                <input type="text" name="drop_location" class="form-control" id="drop_location" value="<?php echo set_value('drop_location'); ?>" />
                                <?php echo form_error('drop_location','<span class="text-danger">','</span>'); ?>
                                </div>
                                </div>
                         <div class="form-group">
                           <div class="col-md-4 col-sm-4">
                              <label>Parcel Information<span class="text-danger"> *</span></label>
                                <textarea type="text" name="parcel_information" class="form-control" id="parcel_information" value="<?php echo set_value('parcel_information'); ?>" /></textarea>
                                <?php echo form_error('parcel_information','<span class="text-danger">','</span>'); ?>
                                </div>
                                </div>
                    
                       
                        
                        </div>
                        
                       <br>
                  <div class="row">
                     <div class="col-md-1">
                        <input type="submit" name="submit" value="Add" class="btn btn btn-success margin-top-30">
                     </div>
                     <div class="col-md-1">
                     <a href="<?php echo base_url();?>driver" ><button type="button" class="btn btn-danger margin-top-30 ">Cancel</button></a>
                     </div>
                  </div>
               </form>
               

            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6jpjQRZn8vu59ElER36Q2LaxptdAghaA&callback=initMap&libraries=places&v=weekly"
      async
    ></script>
 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      function initMap() {
        const map = new google.maps.Map(document.getElementById("map"), {
          center: { lat: 40.749933, lng: -73.98633 },
          zoom: 13,
        });
        const card = document.getElementById("pac-card");
        const input = document.getElementById("pac-input");
        const biasInputElement = document.getElementById("use-location-bias");
        const strictBoundsInputElement = document.getElementById(
          "use-strict-bounds"
        );
        const options = {
          componentRestrictions: { country: "us" },
          fields: ["formatted_address", "geometry", "name"],
          origin: map.getCenter(),
          strictBounds: false,
          types: ["establishment"],
        };
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
        const autocomplete = new google.maps.places.Autocomplete(
          input,
          options
        );
        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo("bounds", map);
        const infowindow = new google.maps.InfoWindow();
        const infowindowContent = document.getElementById("infowindow-content");
        infowindow.setContent(infowindowContent);
        const marker = new google.maps.Marker({
          map,
          anchorPoint: new google.maps.Point(0, -29),
        });
        autocomplete.addListener("place_changed", () => {
          infowindow.close();
          marker.setVisible(false);
          const place = autocomplete.getPlace();

          if (!place.geometry || !place.geometry.location) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert(
              "No details available for input: '" + place.name + "'"
            );
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          infowindowContent.children["place-name"].textContent = place.name;
          infowindowContent.children["place-address"].textContent =
            place.formatted_address;
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          const radioButton = document.getElementById(id);
          radioButton.addEventListener("click", () => {
            autocomplete.setTypes(types);
            input.value = "";
          });
        }
        setupClickListener("changetype-all", []);
        setupClickListener("changetype-address", ["address"]);
        setupClickListener("changetype-establishment", ["establishment"]);
        setupClickListener("changetype-geocode", ["geocode"]);
        biasInputElement.addEventListener("change", () => {
          if (biasInputElement.checked) {
            autocomplete.bindTo("bounds", map);
          } else {
            // User wants to turn off location bias, so three things need to happen:
            // 1. Unbind from map
            // 2. Reset the bounds to whole world
            // 3. Uncheck the strict bounds checkbox UI (which also disables strict bounds)
            autocomplete.unbind("bounds");
            autocomplete.setBounds({
              east: 180,
              west: -180,
              north: 90,
              south: -90,
            });
            strictBoundsInputElement.checked = biasInputElement.checked;
          }
          input.value = "";
        });
        strictBoundsInputElement.addEventListener("change", () => {
          autocomplete.setOptions({
            strictBounds: strictBoundsInputElement.checked,
          });

          if (strictBoundsInputElement.checked) {
            biasInputElement.checked = strictBoundsInputElement.checked;
            autocomplete.bindTo("bounds", map);
          }
          input.value = "";
        });
      }
    </script>