<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
         Reise Live Verfolgen  
            <small>Reise Live Verfolgen</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Heim</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">              

<style>
#map {
    height: 500px;
    width: 100%;
    margin: 0px;
    padding: 0px;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgGHa9xRofBw7WY4zcUj9hXyQWCk4-Qtc"></script>

<!-- /page title -->
<div id="content" class="padding-20">
   <div class="row">
      <div class="col-md-12">
      
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
               <strong>Reise Live Verfolgen</strong>
                 <div class="pull-right box-tools">
                    <a href="<?php echo base_url();?>restaurant/trip" class="btn btn-info btn-sm">Zurück</a>                           
                </div>
                <br>
                </br>
            </div>
    
            <div class="panel-body">
			<div id="map"></div>	
            </div>
         </div>
      
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /MIDDLE -->

<script type="text/javascript">
 function InitializeMap() {
            var ltlng = [];

            <?php foreach($trackingtrip as $tel){				
			?>
			
			 ltlng.push(new google.maps.LatLng(<?php echo $tel->location_lat; ?>, <?php echo $tel->location_long; ?>));
			<?php } ?>
			
			/* ltlng.push(new google.maps.LatLng(17.22, 78.28));
            ltlng.push(new google.maps.LatLng(13.5, 79.2));
            ltlng.push(new google.maps.LatLng(15.24, 77.16)); */
  
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions = {
                zoom: 11,
				<?php if(!empty($trackingtrip)) {?>
                //center: latlng,
                center: ltlng[0],
				<?php } else { ?>
				center: latlng,
				<?php } ?>
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"), myOptions);

            for (var i = 0; i < ltlng.length; i++) {
                var marker = new google.maps.Marker
                    (
                    {
                        // position: new google.maps.LatLng(-34.397, 150.644),
                        position: ltlng[i],
                        map: map,
                        title: 'Click me'
                    }
                    );
            }
            //***********ROUTING****************//

            //Intialize the Path Array
            var path = new google.maps.MVCArray();

            //Intialize the Direction Service
            var service = new google.maps.DirectionsService();

            //Set the Path Stroke Color
            var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
            //Loop and Draw Path Route between the Points on MAP
            for (var i = 0; i < ltlng.length; i++)
            {
                if ((i + 1) < ltlng.length) {
                    var src = ltlng[i];
                    var des = ltlng[i + 1];
                    path.push(src);
                    poly.setPath(path);
                    service.route({
                        origin: src,
                        destination: des,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    }, function (result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                path.push(result.routes[0].overview_path[i]);
                            }
                        }
                    });
                }
            }

        }

        window.onload = InitializeMap;
</script>