      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Restaurant-Dashboard
            <small>Schalttafel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Heim</a></li>
            <li class="active">Armaturenbrett</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $driver_count; ?></h3>
                  <p>Fahreranzahl</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo base_url(); ?>restaurant/driver" class="small-box-footer">Mehr Infos <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $parcel_count; ?><sup style="font-size: 20px"></sup></h3>
                  <p>Paketanzahl</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?php echo base_url(); ?>restaurant/parcel" class="small-box-footer">Mehr Infos <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $trip_count; ?></h3>
                  <p>Fahrtenanzahl</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>restaurant/trip" class="small-box-footer">Mehr Infos  <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>
                  <?php
                  if(!empty($get_top_driver->count)) 
                  {

                     echo $get_top_driver->count;
                  }
                  else
                  {
                    echo "";
                  }
                  ?></h3>
                  <p>Top-Lieferfahrer</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>restaurant/driver" class="small-box-footer">Mehr Infos  <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
          </div>
          <div class="row">
           
             <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <?php

                  $connectivity_number         = $restaurant_details->connectivity_number;
                  if(!empty($connectivity_number))
                  {
                    
                      $passcode = $connectivity_number;

                  }
                  else
                  {
                      $passcode = "";
                  }
                  ?>
                  
                  <h3><?php echo $passcode;?></h3>
                     <p></p>
                  <p><button type="button" style="color:black" 
                  onclick="genereteRandomNumber(<?php echo  $_SESSION[web_admin][0]->restaurant_id; ?>);">Passwort generieren</button></p>
                </div>
               
               
              </div>
            </div><!-- ./col -->
			
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <?php $mo = explode('-',date("m-d-Y"));  ?>
                  <h3><?php  
                  
                  if(!empty($smscoundata))
                    { if($mo[0]==01){ echo $smscoundata[0]['january'];}
				  if($mo[0]==02){ echo $smscoundata[0]['february'];}
				  if($mo[0]==03){ echo $smscoundata[0]['march'];}
				  if($mo[0]==04){ echo $smscoundata[0]['april'];}
				  if($mo[0]==05){ echo $smscoundata[0]['may'];}
				  if($mo[0]==06){ echo 'June - '; echo $smscoundata[0]['june'];}
				  if($mo[0]==07){ echo 'July - '; echo $smscoundata[0]['july'];}
				  if($mo[0] >= 07){ echo 'August - '; echo $smscoundata[0]['august'];}
				 /* if($mo[0]==09){ echo $smscoundata[0]['september'];}
				  if($mo[0]==10){ echo $smscoundata[0]['october'];}
				  if($mo[0]==11){ echo $smscoundata[0]['november'];}
				  if($mo[0]==12){ echo $smscoundata[0]['december'];} */ } else {
					  echo '';
				  }?></h3>	
                  <p>Gesamte SMS senden</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo base_url(); ?>restaurant/parcel/smsmonthlist" class="small-box-footer tss">Mehr Infos <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
			
          </div>
          
          <!-- /.row -->
          <!-- Main row -->
          

        </section><!-- /.content -->
      </div>
      
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
function genereteRandomNumber(resturent_id){
 $.ajax({
  url: '<?php echo base_url(); ?>restaurant/login/genereteRandomNumber',
  type: "POST",
  data:{resturent_id:resturent_id}, 
  //dataType: "json",
  success: function(data){
  
     location.reload();

  } 
}); 
}
</script>
      <!-- /.content-wrapper -->
<style>
    .small-box {
    height: 135px;
}

.bg-green button {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: 11%;
    margin-top: 5%;
    float: left;
    padding: 7px 30px;
}
/*.bg-red p {
    margin-top: 21%;
}
.tss {
    margin-top: 13%;
}*/
</style>
