<!DOCTYPE html>

<html>

    <body class="skin-blue sidebar-mini">

    <div class="wrapper">

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->

        <section class="content-header">

          <h1>

            Paket bearbeiten

            <small>Paket bearbeiten</small>

          </h1>

          <br>

          </br>

          <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Heim</a></li>

           

          </ol>

        </section>



        <!-- Main content -->

        <section class="content">

          <div class="row">

            <div class="col-xs-12">

              <div class="box">

                



 

<!-- /page title -->

<div id="content" class="padding-20">

   <div class="row">

      <div class="col-md-12">

        

         <div class="panel panel-default">

            <div class="panel-heading panel-heading-transparent">

               <strong>Paket bearbeiten</strong>

                 <div class="pull-right box-tools">

                    <a href="<?php echo base_url(); ?>restaurant/Parcel" class="btn btn-info btn-sm">Zurück</a>                           

                </div>

                <br>

                </br>

            </div>



            <div class="panel-body">
             <form action="<?php echo base_url(); ?>restaurant/parcel/addparcel/<?php echo $edit_parcel->parcel_id; ?>" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">

                    <div class="row">

                        <div class="form-group">

                           <div class="col-md-4 col-sm-4">
                            <?php 
                            $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;
                            $customer_details      =  $this->Parcel_model->getCustomerById($edit_parcel->parcel_id,$restaurant_id);
                              if(!empty($customer_details))
                              {
                                  $customer_first_name = $customer_details->first_name;
                                  $customer_last_name = $customer_details->last_name;
                                  $customer_number = $customer_details->customer_number;
                                  $customer_address = $customer_details->customer_address;
                                  $customer_city  = $customer_details->customer_city;
                                  $customer_zip_code = $customer_details->customer_zip_code;
                              }
                              else
                              {
                                  $customer_first_name = ''; 
                                  $customer_last_name  = '';
                                  $customer_number  = '';
                                  $customer_address = '';
                                  $customer_city    = '';
                                  $customer_zip_code    = '';

                                  
                              }
                              
                            ?>
                              <label>Name</label><span class="text-danger"> *</span>

                                <input name="first_name" class="form-control" type="text" id="reciver_name" value="<?php echo $customer_first_name; ?>" />

                                <?php echo form_error('first_name', '<span class="text-danger">', '</span>'); ?>

                           </div>
                           <div class="col-md-4 col-sm-4">

                              <label>Nachname</label><span class="text-danger"> *</span></label>

                                <input name="last_name" class="form-control" type="text" id="surname" 
                                value="<?php echo $customer_last_name; ?>" />

                                <?php echo form_error('customer_number', '<span class="text-danger">', '</span>'); ?>

                           </div>

                           <div class="col-md-4 col-sm-4">

                              <label>Nummer</label><span class="text-danger"> *</span></label>

                                <input name="customer_number" class="form-control" type="text" id="surname" 
                                value="<?php echo $customer_number; ?>" />

                                <?php echo form_error('customer_number', '<span class="text-danger">', '</span>'); ?>

                           </div>
                        </div>
                     </div>
					
					 
					 
                     <div class="row">
                        <?php
                      if($edit_parcel->pickup_location_status == 1)
                      {
                        
                        ?>
                         <div class="col-md-4 col-sm-4">

                              <label>Abholort</label>
                                <input type="text" class="form-control pac-target-input" name="pickup_location" id="autocomplete" value="<?php echo $edit_parcel->pic_up_location; ?>"  onfocus="geolocate()" placeholder="Pickup Address" autocomplete="off">
                                   <?php echo form_error('pic_up_location', '<span class="text-danger">', '</span>'); ?>

                                <div class="pac-card" id="pac-card">

                       </div>

                       </div>
                       <?php
                      }
                      ?>

                         <div class="form-group">
                       


                           <div class="col-md-4 col-sm-4">

                              <label>Ablagort<span class="text-danger"> *</span></label>

                                <input type="text" class="form-control pac-target-input" name="drop_location" id="autocomplete1" value="<?php echo  $edit_parcel->drop_location	;?>"  onfocus="geolocate()" placeholder="Drop Address" autocomplete="off">
                                   <?php echo form_error('autocomplete1', '<span class="text-danger">', '</span>'); ?>
                                

                                <?php echo form_error('drop_location', '<span class="text-danger">', '</span>'); ?>

                                </div>

                                </div>
                             <div class="col-md-4 col-sm-4">

                              <label>Postleitzahl</label><span class="text-danger"> *</span></label>

                                <input name="customer_zip_code" class="form-control" type="text" id="customer_zip_code" value="<?php echo $customer_zip_code; ?>" />

                                <?php echo form_error('customer_zip_code', '<span class="text-danger">', '</span>'); ?>

                           </div>
        
                     </div>
                       <div class="row">


                         <div class="form-group">
                             
                               <div class="col-md-4 col-sm-4">

                              <label>Stadt</label>

                               <input type="text" name="city" value="<?php echo $customer_city; ?>" id="city" class="form-control">
                                <?php echo form_error('city', '<span class="text-danger">', '</span>'); ?>

                                </div>

                           <div class="col-md-4 col-sm-4">

                              <label>Paketnotiz</label>

                                <select name="parcel_information" id="parcel_information" class="form-control">
                                    <option value="">Notiz</option>
                                    <option <?php if($edit_parcel->parcel_information == 'Nich Klingeln Anrufen'){echo 'selected';} ?> value="Nich Klingeln Anrufen">Nich Klingeln Anrufen</option>
                                    <option <?php if($edit_parcel->parcel_information == 'Vor der Tür legen'){echo 'selected';} ?> value="Vor der Tür legen">Vor der Tür legen</option>
                                    
                                    <option <?php if($edit_parcel->parcel_information == 'Legen Sie es in den Brifkasten'){echo 'selected';}?> value="Legen Sie es in den Brifkasten">Legen Sie es in den Brifkasten</option>
                                    <option <?php if($edit_parcel->parcel_information == 'Bezahlt'){echo 'selected';}?> value="Bezahlt">Bezahlt</option>
                                   
                                    <option <?php if($edit_parcel->parcel_information == 'Nicht Bezahlt'){echo 'selected';}?> value="Nicht Bezahlt">Nicht Bezahlt</option>
                                    <option <?php if($edit_parcel->parcel_information == 'Mit Kreditkarte'){echo 'selected';}?> value="Mit Kreditkarte">Mit Kreditkarte</option>
                                    
                                    <option <?php if($edit_parcel->parcel_information == 'Unterschrift'){echo 'selected';}?> value="Unterschrift">Unterschrift</option>
                                    <option <?php if($edit_parcel->parcel_information == 'Eigene'){echo 'selected';}?> value="Eigene">Eigene</option>


                                </select>

                                <?php echo form_error('parcel_information', '<span class="text-danger">', '</span>'); ?>

                                </div>
                            <div class="col-md-4 col-sm-4" id="add_fields_placeholderValue">
                              <label>Eigene Notiz</label>
                               <input type="text" name="own_note"  id="own_note" class="form-control" value="<?php echo $edit_parcel->own_note; ?>">
                                <?php echo form_error('own_note', '<span class="text-danger">', '</span>'); ?>

                                </div>

                                </div>

                            </div>
                          <br>
                       <div class="row">
                          <input type="hidden" name="pickup_latitude" id="pickup_latitude" value="">
                          <input type="hidden" name="pickup_longitude" id="pickup_longitude" value="">
                          <input type="hidden" name="drop_latitude" id="drop_latitude" value="">
                          <input type="hidden" name="drop_longitude" id="drop_longitude" value="">
                       </div>

                  <div class="row">

                     <div class="col-md-1">

                        <input type="submit" name="editsubmit" value="bearbeiten" class="btn btn btn-success margin-top-30">

                     </div>

                     <div class="col-md-1">

                     <a href="<?php echo base_url(); ?>restaurant/Parcel" ><button type="button" class="btn btn-danger margin-top-30 ">Stornieren</button></a>

                     </div>

                  </div>

               </form>

               



            </div>

         </div>

      

      </div>

  

   </div>

</div>

</div>

</div>

</div>

</div>

</div>

</section>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/PlacePicker.js"></script>
  <script>
$(document).ready(function()
{
  var test = $("#parcel_information").val();
  if(test == "Eigene")
  {
     $("#add_fields_placeholderValue").show();
  }  
  else
  {
     $("#add_fields_placeholderValue").hide();
  
  }
 $("#parcel_information").change(function()
 {

  

  if($(this).val() == "Eigene")
  {
   $("#add_fields_placeholderValue").show();
  }
  else
  {
   $("#add_fields_placeholderValue").hide();
  }
 });
});
  </script>
  
   <script>
  function selectcat(id)
	  {
		//alert(id);
		$.ajax({	
		 url: '<?php echo base_url(); ?>restaurant/parcel/getstate',
         type: "POST",
         data:{id:id}, 
        success: function(returncat){ 
         //alert(returncat);
		  $("#state").html(returncat);
		}
		 });
	  }
	  
	 function selectstate(id)
	  {
		//alert(id);
		$.ajax({	
		 url: '<?php echo base_url(); ?>restaurant/parcel/getcity',
         type: "POST",
         data:{id:id}, 
        success: function(returncat){ 
         //alert(returncat);
		  $("#city").html(returncat);
		}
		 });
	  }  
  </script>
  
  <script type="text/javascript">
    

      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;



      var componentForm = {



        //street_number: 'short_name',



        //route: 'long_name',



        //locality: 'long_name',



        //administrative_area_level_1: 'short_name',



        //country: 'long_name',



        //postal_code: 'short_name'



      };



      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),



          {types: ['geocode']});
        autocomplete1 = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),



          {types: ['geocode']});  



        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);
        autocomplete1.addListener('place_changed', fillInAddress);


    }
      


    function fillInAddress() 
    {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();



        for (var component in componentForm) {



          document.getElementById(component).value = '';



          document.getElementById(component).disabled = false;



        }



        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



          var addressType = place.address_components[i].types[0];



          if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



      //alert(val);



    }



  }



}



      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      function geolocate() 
      {


  
        if (navigator.geolocation) {



          navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
             
              

            };
            
            var pickuplat = geolocation.lat;
            var pickuplong = geolocation.lng;
            $('#pickup_latitude').val(pickuplat); 
            $('#pickup_longitude').val(pickuplong); 

            var circle = new google.maps.Circle({



              center: geolocation,



              radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



          });



        }



      }
       

  </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
     
    


<script type="text/javascript">
    $(document).ready(function(){
      $("#pickup").PlacePicker({
        btnClass:"btn btn-xs btn-default",
        key:"AIzaSyBAGm4WfH69Ny5DmWFAA88s26UwiNqHMn8",
        center: {lat: 17.6868, lng: 83.2185},
        success:function(data,address){
          //data contains address elements and
          //address conatins you searched text
          //Your logic here
          $("#pickup").val(data.formatted_address);
        }
      });
    });
</script>

<script>
/*
$("#pickup_country").PlacePicker({

  key:"AIzaSyB6jpjQRZn8vu59ElER36Q2LaxptdAghaA"

});

$("#pickup_country").PlacePicker({

  title: "Popup Title Here"

});

$("#pickup_country").PlacePicker({

  btnClass: "btn btn-secondary btn-sm"

});

$("#pickup_country").PlacePicker({

    center: {lat: -34.397, lng: 150.644}



});

$("#pickup_country").PlacePicker({

   zoom: 10

});

$("#pickup_country").PlacePicker({

  success:function(data,address){

    //data contains address elements and

    //address conatins you searched text

    //Your logic here

    $("#pickup_country").val(data.formatted_address);

  }

});

*/

</script>

