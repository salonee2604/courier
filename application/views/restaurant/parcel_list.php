<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Paketliste
            <small>Paketliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Zuhause</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Paketliste</h3>
                  <div class="pull-right box-tools">
                  <a href="<?php echo base_url();?>restaurant/Parcel/addparcel" class="btn btn-info btn-sm">Neu hinzufügen</a> 
                  </div>
                </div><!-- /.box-header -->
            <div class="box-body">
              
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="the_table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>Name des Kunden</th>
                        <th>Fahrername</th>
                        <th>Nachname des Kunden</th>
                        <th>Kundennummer</th>                        
                        <th style="width: 175px;">Abholort</th>
                        <th>Ablagort</th>
                        <th>Notiz</th>
                        <th>Paket Status</th>
                        <th>Reise Status</th>
                        <th>Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($parcel_details as $value)
                        {
                            $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;
                            $customer_details      =  $this->Parcel_model->getCustomerById($value->parcel_id,$restaurant_id);
                           $trip_details          =  $this->Parcel_model->getDriverByParcelId($value->parcel_id);
                          if(!empty($trip_details->driver_id))
                          {
                             $driver_details       =  $this->Parcel_model->getAllDriverListByDriverID($trip_details ->driver_id,$restaurant_id);
                          }
                          else
                          {

                            $driver_details = '';
                          }
                         

                           if(!empty($driver_details->first_name))
                           {
                             $driver_name = $driver_details->first_name.' '.$driver_details->surname;
                           }
                           else
                           {

                             $driver_name = "";
                           }

                        ?>
                            
                          
                          
                        
                    
                         <tr>
                        <td><?php echo $i; ?> </td>
                          <td>
                          <?php
                          if(!empty($customer_details->first_name))
                          {                          
                              
                              echo $customer_details->first_name; 
                          }
                          else
                          {
                              echo "";
                          } 
                          ?>
                          </td>
                          <td><?php echo $driver_name; ?></td>
                          <td>
                          <?php
                          if(!empty($customer_details->last_name))
                          {                          
                              
                              echo $customer_details->last_name; 
                          }
                          else
                          {
                              echo "";
                          } 
                          ?>
                          </td>
                          <td><?php 
                          if(!empty($customer_details->customer_number))
                          { 
                              echo $customer_details->customer_number;
                              
                          }
                          else
                          {
                              echo "";
                          }
                          ?>
                          </td>

                          <?php
                          if($value->pickup_location_status == 1)
                          {
                            ?>
                             <td><?php echo $value->pic_up_location; ?></td>
                            <?php
                          }
                          else
                          {
                            ?>
                            <td><?php echo ""; ?></td>
                            <?php
                          }
                          ?>
                         
                          <td><?php echo $value->drop_location; ?></td>
                          <td><?php echo $value->parcel_information; ?></td>
                          <td>
                          <?php 
                          if($value->status =='1')
                          {
                              ?>
                              <button class="btn btn-success">Aktiv</button>
                              <?php
                          }
                          elseif($value->status =='2')
                          {
                              ?>
                              <button class="btn btn-info">Liefern</button>
                              <?php
                          }
                          else
                          {
                             ?> 
                            <button class="btn btn-danger">InAktiv</button>
                           <?php
                          }
                          ?></td>
                           <td>
                            <?php 
                          if($value->trip_start_status =='1')
                          {
                              ?>
                              <button class="btn btn-info">Reise Start</button>
                              <?php
                          }
                          elseif($value->trip_start_status =='0')
                          {
                              ?>
                              <button class="btn btn-warning">Ausstehend</button>
                              <?php
                          }
                          elseif($value->trip_start_status =='2')
                          {

                               ?>
                              <button class="btn btn-success">Abholort</button>
                              <?php

                          }
                          elseif($value->trip_start_status =='3')
                          {
                              ?>
                              <button class="btn btn-success">Reise End</button>
                              <?php
                          }
                          else
                          {
                             ?> 
                            <button class="btn btn-danger"></button>
                           <?php
                          }
                          ?>
                        </td>
                          <td>
                              <a href="<?php echo base_url();?>restaurant/parcel/parcelFullView/<?php echo $value->parcel_id; ?>" title="Edit"><i class="fa fa-eye fa-2x "></i></a>&nbsp;<a href="<?php echo base_url();?>restaurant/parcel/addparcel/<?php echo $value->parcel_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>&nbsp;
                          
                          <a class="confirm" onclick="return delete_parcel('<?php echo $value->parcel_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                        </tr>
                      
                      <?php
                      $i++;
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
">
    
</script>
<script type="text/javascript">
    function delete_parcel(parcel_id)
    {
          bootbox.confirm("Are you sure you want to delete Parcel Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>restaurant/parcel/delete_Parcel/"+parcel_id;
            }
        });
    } 
</script>
<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>