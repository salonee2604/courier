<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Restaurant List
            <small>Restaurant List</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Restaurant List</h3>
                  <div class="pull-right box-tools">
                  <a href="<?php echo base_url();?>admin/restaurant/addRestaurant" class="btn btn-info btn-sm">Add New</a> 
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
              
                  <table id="the_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Surname</th>
                        <th>Restaurants name</th>
                        <th>Mobile Number</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Post code</th>
                        <th>Restaurant Image</th>
                        <th>Bank Details</th>
                        <th>Admin Status</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Payment</th>
                        </tr>
                    </thead>
                    <tbody>
             <?php
                    foreach($restaturant_details as $value)
                    {                  
                      ?>
                        <tr>
                          <td><?php echo $value->first_name; ?></td>
                          <td><?php echo $value->surname; ?></td>
                          <td><?php echo  $value->restaurant_name; ?> </td>
                          <td><?php echo $value->mobile_number; ?></td>
                          <td><?php echo $value->email; ?></td>
                          <td><?php echo $value->address; ?></td>
                          <td><?php echo $value->city; ?></td>
                          <td align="center">
                            <?php echo $value->post_code; ?></td>
                          </td>
                          <td><img src="<?php echo $value->restaurant_image; ?>" height="100px" width="100px"></td>
                          <td><?php echo $value->bank_details; ?></td>
                          <td>
                           <?php 
                              if($value->admin_approved_status == '1')
                              { 
                                ?>
                                <button class="btn btn-success">Approved</button>
                               <?php 
                              }
                              else
                              {
                                ?>
                                <button class="btn btn-info btn btn-danger">UnApproved</button>
                               <?php    
                              }
                              ?>
                          </td>
                          <td>
                            <?php 
                              if($value->status == '1')
                              { 
                                ?>
                                <button class="btn btn-success">Active</button>
                               <?php 
                              }
                              else
                              {
                                ?>
                                <button class="btn btn-info btn btn-danger">In Active</button>
                               <?php    
                              }
                              ?>
                          </td>
                          <td><a href="<?php echo base_url();?>admin/restaurant/fullViewRestaurant/<?php echo $value->restaurant_id; ?>" title="View"><i class="fa fa-eye fa-2x "></i></a>&nbsp;&nbsp;
                          <a href="<?php echo base_url();?>admin/restaurant/adddriver/<?php echo $value->restaurant_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>
                          <a class="confirm" onclick="return delete_driver('<?php echo $value->restaurant_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                          <td> <a href="<?php echo base_url(); ?>admin/restaurant/buy/<?php echo $value->restaurant_id; ?>" class="btn"><i class="fab fa-paypal"></i></a></td>
                        </tr>
                      <?php
                         }
            
              ?>                       
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
">
    
</script>
<script type="text/javascript">
    function delete_driver(driverid)
    {
          
          bootbox.confirm("Are you sure you want to delete Driver Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>driver/delete_driver/"+driverid;
            }
        });
    } 
</script>

<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>
