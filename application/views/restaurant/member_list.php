<!DOCTYPE html>

<html>

  <style>

.switch {

  position: relative;

  display: inline-block;

  width: 60px;

  height: 34px;

}



.switch input { 

  opacity: 0;

  width: 0;

  height: 0;

}



.slider {

  position: absolute;

  cursor: pointer;

  top: 0;

  left: 0;

  right: 0;

  bottom: 0;

  background-color: #ccc;

  -webkit-transition: .4s;

  transition: .4s;

}



.slider:before {

  position: absolute;

  content: "";

  height: 26px;

  width: 26px;

  left: 4px;

  bottom: 4px;

  background-color: white;

  -webkit-transition: .4s;

  transition: .4s;

}



input:checked + .slider {

  background-color: #2196F3;

}



input:focus + .slider {

  box-shadow: 0 0 1px #2196F3;

}



input:checked + .slider:before {

  -webkit-transform: translateX(26px);

  -ms-transform: translateX(26px);

  transform: translateX(26px);

}



/* Rounded sliders */

.slider.round {

  border-radius: 34px;

}



.slider.round:before {

  border-radius: 50%;

}

</style>

  <body class="skin-blue sidebar-mini">

    <div class="wrapper">

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->

        <section class="content-header">

          <h1>

            Restaurantdetails

            <small>Restaurantdetails</small>

          </h1>

          <ol class="breadcrumb">

            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Startseite</a></li>

         </ol>

        </section>

  <div id="msg_div">

                 <?php echo $this->session->flashdata('message');?>

                </div>

        <!-- Main content -->

        <section class="content">          

          <div class="row">

            <div class="col-xs-12">

              <div class="box">

              <div id="content" class="padding-20">

                  <div id="msg_div">

                 <?php echo $this->session->flashdata('message');?>

                </div>

   <div class="row">

      <div class="col-md-12">

         <div class="panel panel-default rs-full">

            <div class="panel-heading panel-heading-transparent">

               <strong>            

               Restaurant Details</strong>

                 <div class="pull-right box-tools">

                    <a href="<?php echo base_url();?>restaurant/dashboard" class="btn btn-info btn-sm">Zurück</a>                           

                </div>

                <br>

            <br>

            </div>

             <div class="panel-body member-list">

               <form action="<?php echo base_url(); ?>restaurant/restaurant/addRestaurant/<?php echo $resturent_details->restaurant_id; ?>" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">



                   <div class="row">

                           <div class="col-md-6">

                            <div class="col-md-6">

                             <h4><b>Firma Name</b></h4>

                             </div>

                            <div class="col-md-6">

                              <h4>                              

                              <?php

                               echo $resturent_details->restaurant_name;

                               ?>

                               </h4>

                               </div>

                                

                           </div> 

                           <div class="col-md-6">

                             <div class="col-md-6">

                              <h4><b>Telefonnummer</b></h4>

                              </div>

                              <div class="col-md-6">

                             <h4><?php echo $resturent_details->mobile_number; ?></h4>

                             </div>

                           </div>

                           </div>

                   <div class="row">

                        <div class="col-md-6">

                            <div class="col-md-6">

                            <h4><b>E-Mail</b></h4>

                            </div>

                            <div class="col-md-6">

                            <h4><?php echo $resturent_details->email; ?></h4>

                            </div> 

                        </div>

                        <div class="col-md-6">

                        <div class="col-md-6">

                            <h4><b>Erstelldatum</b></h4>

                        </div>

                        <div class="col-md-6">

                         <h4><?php echo $resturent_details->created_date; ?></h4>

                         </div>

                        </div>

                       </div>

                   <div class="row">

                   <div class="col-md-6">

                    <div class="col-md-6">

                   <h4><b>Letztes Datum (14 Tage kostenlose Testversion)</b></h4>

                   </div>

                   <div class="col-md-6">

                       <?php 



                            $date                   =  $resturent_details->created_date;

                            $date_forteen_days      = date('Y-m-d', strtotime($date. ' + 14 days'));



                            ?>

                            <h4><?php echo $date_forteen_days; ?></h4>

                   </div>

                   </div>

                   <div class="col-md-6">

                   <div class="col-md-6"><h4><b>Notiz</b></h4></div>

                   <div class="col-md-6"><h4><?php echo $resturent_details->note ?></h4></div>



                   </div>

                   </div>

                   <div class="row">

                             <div class="col-md-6">

                              <div class="col-md-6">

                              <h4><b>Administratorstatus</b></h4>

                              </div>

                               <div class="col-md-6">



                                <?php 

                              if($resturent_details->admin_approved_status == '1')

                              { 

                                ?>

                                <h4 style="color:green">Freigegeben</h4>

                               <?php 

                              }

                              else

                              {

                                ?>

                                <h4 style="color:red">Nicht genehmigt</h4>

                               <?php    

                              }

                              ?>

                           </div>

                           </div>

                          

                            <div class="col-md-6">

                             <div class="col-md-6">

                              <h4><b>Status</b></h4>

                              </div>

                              <div class="col-md-6">

                                <?php 

                              if($resturent_details->status == '1')

                              { 

                                ?>

                                <h4 style="color:green">Aktiv</h4>

                               <?php 

                              }

                              else

                              {

                                ?>

                                <h4 style="color:red">Inaktiv</h4>

                               <?php    

                              }

                              ?>                              

                            </div> 

                          

                           </div>

                           

                        </div>

                       <div class="row">

                           <div class="col-md-6">

                               <div class="col-md-6">

                              <h4><b>SMS-Dienst (aktiv oder deaktiviert)<b></h4>

                              </div>

                              <div class="col-md-6">

                           <?php 

                              if($resturent_details->sms_status	== 1)

                              {

                                  

                              ?>

                                   <button type="button"  class="confirm btn btn-success" onclick="return sms_activation('<?php echo $resturent_details->restaurant_id;?>','0');">SMS aktiv</button>

                              <?php

                              }

                              else

                              {

                                

                                 ?>

                                   <button type="button"  class="confirm btn btn-danger" onclick="return sms_activation('<?php echo $resturent_details->restaurant_id;?>','1');">SMS in aktiv</button>



                                <?php  

                              }

                              ?>

                         

                            

                           </div>



                           </div>

                             <div class="col-md-6">

                              <div class="col-md-6">

                              <h4><b>Abo kündigen oder aktiv</b></h4>

                              </div>

                              <div class="col-md-6">

                              <?php 

                              if($resturent_details->termination_staus	== 1)

                              {

                                  

                              ?>

                                   <button type="button"  onclick="return cancel_subscription('<?php echo $resturent_details->restaurant_id;?>',0);" class="btn btn-danger">Abo kündigen</button>

                              <?php

                              }

                              else

                              {

                                

                                 ?>

                                    <button class="confirm btn btn-success" onclick="return cancel_subscription('<?php echo $resturent_details->restaurant_id;?>',1);" type="button">Abonnement aktiv</button>



                                <?php  

                              }

                              ?>

                         

                           </div>

                            

                           </div>

                             

                          </div>

                           <div class="row">

                            <div class="col-md-6">

                                <div class="col-md-5">

                                <h4><b>Paypal</b></h4>

                                </div>

                                <div class="col-md-6">



                               <a href="<?php echo base_url(); ?>restaurant/membership/buy/<?php echo $resturent_details->restaurant_id; ?>" class="btn"><img src="<?php echo base_url();?>assets/ezgif-6-c36878577925.jpg" height="50%" width="50%"></a>                            

                          </div>

                          </div>

                           <div class="col-md-6">

                              <div class="col-md-6">

                             <h4><b>Transaktionsdetails</b></h4>

                             </div>

                             <div class="col-md-6">



                                 <a href="<?php echo base_url(); ?>restaurant/restaurant/transactionDetails/<?php echo $resturent_details->restaurant_id; ?>" class="btn btn-primary">Transaktionsdetails</a>

                                <?php echo form_error('status','<span class="text-danger">','</span>'); ?>

                           </div> 

                           </div>

                       </div>

            

                  

               </form>

            </div>

         </div>

      

      

  

  

</div>

</div>

</div>

</div>

</div>

</div>

</section>

</div>

</div>

</body>

<!-- /MIDDLE -->



 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<script type="text/javascript">

function status_changesd(id,status){

//alert(status);  

 $.ajax({

  url: '<?php echo base_url(); ?>restaurant/Membership/sms_status',

  type: "POST",

  data:{id:id,status:status}, 

  //dataType: "json",

  success: function(data){

   //alert(data);

     setTimeout(function(){

      location.reload(); 

    }, 1000);

  } 

}); 

}

</script>

<script type="text/javascript">

function status_termination(id,status){

//alert(status);  

 $.ajax({

  url: '<?php echo base_url(); ?>restaurant/Membership/status_termination',

  type: "POST",

  data:{id:id,status:status}, 

  //dataType: "json",

  success: function(data){

   //alert(data);

     setTimeout(function(){

      location.reload(); 

    }, 1000);

  } 

}); 

}

</script>

<script type="text/javascript">

    function cancel_subscription(restaurant_id,status)

    {

        

        if(status == 1)

        {

          var ms =  "Are you sure you want to cancel your subscription to GPS KURIER?The notice period is one month.";

        }

        else

        {

            var ms = "Are you sure you want to undo your cancellation at GPS COURIER?";

        }

          

          bootbox.confirm(ms,function(confirmed)

          {            

            if(confirmed)

            {

                location.href="<?php echo base_url();?>restaurant/Membership/status_termination/"+restaurant_id;

            }

        });

    } 

</script>

<script type="text/javascript">

    function sms_activation(restaurant_id,status)

    {

        

        if(status == 1)

        {

          var mssms =  "Are you sure that you want to activate the sending of SMS messages and pay the costs of CHF 0.09 per SMS?";

        }

        else

        {

            var mssms = "Are you sure you want to cancel sending SMS?";

        }

          

          bootbox.confirm(mssms,function(confirmed)

          {            

            if(confirmed)

            {

                location.href="<?php echo base_url();?>restaurant/Membership/sms_status/"+restaurant_id;

            }

        });

    } 

</script>