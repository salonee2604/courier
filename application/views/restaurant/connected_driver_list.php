<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Liste der verbundenen Fahrer
           <small>Liste der verbundenen Fahrer</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Liste der verbundenen Fahrer</h3>
                  
                </div><!-- /.box-header -->
            <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="the_table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Firma Name</th>
                        <th>Fahrername</th>
                        <th>Verbindungsnummer</th>
                        <th>Datum</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach($connected_driver_list as $value)
                        {
                            
                             $resturent_details = $this->db->get_where('restaurant',array('restaurant_id'=>$value->restaurant_id))->row();
                              $driver_details = $this->db->get_where('driver',array('driver_id'=>$value->connecting_driver_id))->row();
                          
                        ?>
                          <tr>
                            <td><?php echo $i ; ?></td>
                            <?php 

                             $resturent_name  =  $resturent_details->restaurant_name;
                             if(!empty($resturent_name))
                             {
                                $resturent_name_main = $resturent_name;
                             }
                             else 
                             {

                               $resturent_name_main = '';
                             }
                             if(!empty($driver_details->first_name))
                             {
                                   
                                   $name = $driver_details->first_name.' '.$driver_details->surname;
                             }
                             else
                             {
                                 
                                 $name = '';
                             }
                            ?>
                            <td><?php echo $resturent_details->restaurant_name; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $value->connectivity_number; ?></td>
                            <td><?php echo $value->connectivity_date; ?></td>
                          </tr>
                      
                      <?php
                      $i++;
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
">
</script>
<script type="text/javascript">
    function delete_parcel(parcel_id)
    {
          bootbox.confirm("Are you sure you want to delete Parcel Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>admin/parcel/delete_Parcel/"+parcel_id;
            }
        });
    } 
</script>
<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>