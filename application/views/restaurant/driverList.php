<!DOCTYPE html>
<html>
   <style>
        .table {
  border: 1px;
  overflow-x: auto;
  display: block;
}
    </style>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Fahrerliste 
            <small>Fahrerliste</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>
  <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Fahrerliste</h3>
                  <div class="pull-right box-tools">
                  <a href="<?php echo base_url();?>restaurant/driver/adddriver" class="btn btn-info btn-sm">Neu hinzufügen</a> 
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
              
                  <table id="the_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>S.Nein</th>
                        <th>Vorname</th>
                        <th>Nachname</th>
                        <th>Telefonnummer</th>
                        <th>E-Mail</th>
                        <th>Postleitzahl</th>
                        <th>Stadt</th>
                        <th>Fahrer Bild</th>
                        <th>Status</th>
                        <th>Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
                   <?php
                    $i=1;
                    foreach($driver_details as $value)
                    {                  
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->first_name; ?></td>
                          <td><?php echo $value->surname; ?></td>
                          <td><?php echo $value->mobile_number; ?></td>
                          <td><?php echo $value->email; ?></td>
                          <td align="center">
                            <?php echo $value->post_code; ?></td>
                          </td>
                          <td><?php echo $value->city; ?></td>
                          <?php 
                          if(!empty($value->driver_image))
                          {
                              ?>
                              <td><img src="<?php echo $value->driver_image; ?>" height="100px" width="100px"></td>
                              <?php
                          }
                          else
                          {
                              ?>
                              <td><?php echo "" ?></td>
                              <?php
                          }
                          ?>
                          <td>
                            <?php 
                              if($value->status == '1')
                              { 
                                ?>
                                <button class="btn btn-success">Aktiv</button>
                               <?php 
                              }
                              else
                              {
                                ?>
                                <button class="btn btn-info btn btn-danger">In Aktiv</button>
                               <?php    
                              }
                              ?>
                          </td>
                          <td><a href="<?php echo base_url();?>restaurant/driver/fullViewDriver/<?php echo $value->driver_id; ?>" title="View"><i class="fa fa-eye fa-2x "></i></a>&nbsp;&nbsp;
                          <a href="<?php echo base_url();?>restaurant/driver/adddriver/<?php echo $value->driver_id; ?>" title="Edit"><i class="fa fa-edit fa-2x "></i></a>
                          <a class="confirm" onclick="return delete_driver('<?php echo $value->driver_id;?>');"  title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></td>
                        </tr>
                      <?php
                      $i++;
                         }
            
              ?>                       
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
">
    
</script>
<script type="text/javascript">
    function delete_driver(driverid)
    {
          
          bootbox.confirm("Are you sure you want to delete Driver Details",function(confirmed)
          {            
            if(confirmed)
            {
                location.href="<?php echo base_url();?>restaurant/driver/delete_driver/"+driverid;
            }
        });
    } 
</script>

<style>
    div#msg_div .content {
    height: auto !important;
    min-height: auto !important;
}
div#msg_div .col-xs-12 {
    padding-left: 0;
}
</style>
