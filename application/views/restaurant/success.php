<html>
    
    
    <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pyment Successfully
            <small>Pyment Successfully</small>
          </h1>
          <br>
          </br>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box"><?php if(!empty($payment)){ ?>
    <h1 class="success">Your Payment has been Successful!</h1>
    <div class="inner-box-pay">
    <h4 class="payment-heading">Payment Information</h4>
    <p>Reference Number:</p><p> #<?php echo $payment['id']; ?></p>
    <p>Transaction ID:</p><p> <?php echo $payment['txn_id']; ?></p>
    <p>Paid Amount:</p><p> <?php echo $payment['payment_gross'].' '.$payment['currency_code']; ?></p>
    <p>Payment Status:</p><p> <?php echo $payment['status']; ?></p>
	
    <h4 class="payment-heading">Payer Information</h4>
    <p>Name:</p><p> <?php echo $payment['payer_name']; ?></p>
    <p>Email:</p><p> <?php echo $payment['payer_email']; ?></p>
	
    <h4  class="payment-heading">Product Information</h4>
    <p>Name:</p><p> <?php echo $product['restaurant_name']; ?></p>
    <p>Price: </p><p><?php echo 2 .'USD'; ?></p>
    </div>
<?php }else{ ?>
    <h1 class="error">Transaction has been failed!</h1>
<?php } ?>

<style>
    .box {
    border-top: 3px solid #106eea;
    padding: 50px;
    float:left;
}
h1.success {
    background-color: #f7f7f7;
    padding: 10px;
    font-size: 19px;
    color: green;
    text-align: center;
    border-radius: 5px;
    margin-bottom: 20px;
}
.error{
    background-color: #f7f7f7;
    padding: 10px;
    font-size: 19px;
    color: red;
    text-align: center;
    border-radius: 5px;
    margin-bottom: 20px; 
}
.main-footer {
    margin-left: 0px !important;
    float: left;
    width: 100%;
    margin-left: 0px !important;
}
.box p {
    float: left;
    width: 50%;
}
footer.main-footer p {
    width: 100%;
}
h4.payment-heading {
    color: #106eea;
    font-size: 19px;
    font-weight: bold;
}
.inner-box-pay {
    float: left;
    width: 100%;
    border: 1px solid #ddd;
    padding: 50px;
}
</style>