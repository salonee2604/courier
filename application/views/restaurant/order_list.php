<!DOCTYPE html>
<html>
  
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bestellliste 
            <small>Bestellliste </small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Heim</a></li>
         </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Bestellliste</h3>
                  <div class="pull-right box-tools">
                  
                  </div>
                </div><!-- /.box-header -->
            <div class="box-body">
                <div id="msg_div">
                 <?php echo $this->session->flashdata('message');?>
                </div>
                  <table id="the_table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>Vorname des Kunden</th>
                        <th>Kundennummer</th>
                        <th>Kundenadresse</th>
                        <th>Abholort</th>
                        <th>Ablageort</th>
                        <th>Notiz</th>
                        <th>Paket Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1; 
                        foreach($customer_details as $value)
                        {
                            
                          $customer_details      =  $this->Parcel_model->getCustomerById($value->parcel_id,$value->restaurant_id);
                          $parcel_details      =  $this->Customer_model->getParcelById($value->parcel_id,$value->restaurant_id);
                        ?>
                         <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $customer_details->first_name.$customer_details->last_name; ?></td>
                          
                         
                          <td><?php 
                          if(!empty($customer_details->customer_number))
                          { 
                              echo $customer_details->customer_number;
                              
                          }
                          else
                          {
                              echo "";
                          }
                          ?>
                          </td>
                          <td>
                              <?php 
                              if(!empty($customer_details->customer_address))
                              {
                                  echo $customer_details->customer_address;
                              }
                              else
                              {
                                  echo "";
                              }
                              ?>
                          </td>
                          <td><?php echo $parcel_details->pic_up_location; ?></td>
                          <td><?php echo $parcel_details->drop_location; ?></td>
                          <td><?php echo $parcel_details->parcel_information; ?></td>
                          <td>
                          <?php 
                          if($parcel_details->status =='1')
                          {
                              ?>
                              <button class="btn btn-success">Aktiv</button>
                              <?php
                          }
                          elseif($parcel_details->status =='2')
                          {
                              ?>
                              <button class="btn btn-info">Liefern</button>
                              <?php
                          }
                          else
                          {
                             ?> 
                            <button class="btn btn-danger">InAktiv</button>
                           <?php
                          }
                          ?></td>
        
                        </tr>
                      
                      <?php
                        }
                        ?>
                                     
            </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            </div>
            </div>
          <!-- /.row -->
        </section><!-- /.content -->
   
    
  </body>
</html>
