<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>GPS Courier</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url();?>front_assets/img/favicon.png" rel="icon">
  <link href="<?php echo base_url();?>front_assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url();?>front_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url();?>front_assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: BizLand - v1.2.1
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto:info@gps-kurier.ch">info@gps-kurier.ch</a>
        <i class="icofont-phone"></i> 044 506 79 79
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="https://votivetech.in/courier/">GPS Courier<span>.</span></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->

      <nav class="nav-menu d-none d-lg-block">
   <ul>
          <li ><a href="<?php echo base_url(); ?>">Zuhause</a></li>
          <li><a href="<?php echo base_url();?>about">ÜBER UNS</a></li>
          <li><a href="<?php echo base_url();?>team">Mannschaft</a></li>
          <li><a href="<?php echo base_url();?>contact">Kontakt</a></li>

          <!--<li><a href="#portfolio">Portfolio</a></li>-->
          <li><a href="<?php echo base_url();?>restaurantLogin">Anmeldung</a></li>
          <li class="active"><a href="<?php echo base_url();?>restaurantSignup">Anmelden</a>
          </li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
 <!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->


    <!-- ======= Clients Section ======= -->
  

    <!-- ======= Testimonials Section ======= -->
    <!-- End Testimonials Section -->

    <!-- ======= Portfolio Section ======= -->
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>RESTAURANT-ANMELDUNG
</h2>
          <div class="row">
            <div class="col-lg-3">

          </div>
          <div class="col-lg-6">
            <div id="msg_div">
          <?php echo $this->session->flashdata('message');?>        
        </div>
          </div>
          <div class="col-lg-3">
          </div>
        </div>
        </div>

       

        <div class="row" data-aos="fade-up" data-aos-delay="100">
  
         <div class="col-lg-12 login-page" style="padding:25px;">
          <form action="<?php echo base_url();?>login/restaurantSignup" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
              <div class="form-row">
           
                <div class="col form-group col-md-6">
                  <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" placeholder="Name"/>
                  <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?></div>
                <div class="col form-group col-md-6">
                   <input name="surname" class="form-control" type="text" id="surname" value="<?php echo set_value('surname'); ?>" placeholder="Nachname"/>
                  <?php echo form_error('surname','<span class="text-danger">','</span>'); ?>
                </div>
                </div>
          
              
          
            <div class="form-row">
              <div class="col form-group col-md-6">
                <input name="mobile_number" class="form-control" type="text" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>" placeholder="Handynummer" />
                <?php echo form_error('mobile_number','<span class="text-danger">','</span>'); ?>
              </div>
              <div class="col form-group col-md-6">
                <input name="restaurant_name" class="form-control" type="text" id="restaurant_name" value="<?php echo set_value('restaurant_name'); ?>" placeholder="Name des Restaurants"/>
              <?php echo form_error('restaurant_name','<span class="text-danger">','</span>'); ?>
              </div>
              </div>
              <div class="form-row">
                   <div class="col form-group col-md-4 ">
                <input type="email" class="form-control" name="email" onchange="jQuery(this).next('input').val(this.value);" placeholder="Email">
              <?php echo form_error('email','<span class="text-danger">','</span>'); ?>
               </div> 
             <div class="col form-group col-md-4">
              <input type="password" name="password" class="form-control" id="password" value="<?php echo set_value('password'); ?>" placeholder="Passwort"/>
              <?php echo form_error('password','<span class="text-danger">','</span>'); ?>
              </div>
               <div class="col form-group col-md-4">
                 <input type="password" name="c_password" class="form-control" id="c_password" value="<?php echo set_value('client_conf_password'); ?>" placeholder="Passwort bestätigen"/>
                <?php echo form_error('c_password','<span class="text-danger">','</span>'); ?>
              </div>
            </div>
          
          
            <div class="form-row">
                <div class="col form-group col-md-4">
                 <input name="city" class="form-control" type="text" id="city" value="<?php echo set_value('city'); ?>" placeholder="Stadt"/>
                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>
              </div>
                 <div class="col form-group col-md-4">
                 <input type="text" class="form-control" name="post_code" value="<?php echo set_value('post_code'); ?>"  placeholder="Postleitzahlen">
                <?php echo form_error('post_code','<span class="text-danger">','</span>'); ?>
              </div>
              <div class="col form-group col-md-4">
                <input type="text" class="form-control pac-target-input" name="address" value="" id="autocomplete1" value="" required="required" onfocus="geolocate()" placeholder="Adresse ablegen
" autocomplete="off">
               <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
               </div> 
            
            </div>
             <div class="form-row">
                   <div class="col form-group col-md-6">
            <select name="status" id="status" class="form-control">
              <option value="1">Aktiv</option>
              <option value="0">Inaktiv</option>
              </select>
            <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
            </div>
            
              <div class="col form-group col-md-6">
                 <input type="file" name="image" class="form-control" id="image" value="<?php echo set_value('image'); ?>" />
                <?php echo form_error('image','<span class="text-danger">','</span>'); ?>
              </div>
            </div>
            <div class="form-row">
              <div class="col form-group col-md-12">
                <textarea name="bank_details" class="form-control" value="" placeholder="Bankdaten" style="height:130px;"></textarea>
                <?php echo form_error('bank_details','<span class="text-danger">','</span>'); ?>
              </div>
            </div>
              <div class="text-center"><input type="submit" name="submit" value="Hinzufügen" class="btn btn-primary margin-top-30"></div>
            </form>
          </div>

        </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
   <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Abonnieren Sie unseren Newsletter
</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-contact">
            <h3>GPS <span>Courier</span></h3>
            <p>
              Kalkofenstrasse 19, 8810 Horgen<br>
             
              <strong>Phone:</strong> 044 506 79 79<br>
              <strong>Email:</strong> info@gps-kurier.ch<br>
            </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>home">Zuhause</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>about">ÜBER UNS</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>team">Mannschaft</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>terms">Nutzungsbedingungen</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>privacy">Datenschutz-Bestimmungen
</a></li>
            </ul>
          </div>

       
       

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright 2021 <strong><span>GPS Courier</span></strong>. Alle Rechte vorbehalten
      </div>
   
   
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url();?>front_assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url();?>front_assets/js/main.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/PlacePicker.js"></script>
  <script type="text/javascript">
    

      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;



      var componentForm = {



        //street_number: 'short_name',



        //route: 'long_name',



        //locality: 'long_name',



        //administrative_area_level_1: 'short_name',



        //country: 'long_name',



        //postal_code: 'short_name'



      };



      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),



          {types: ['geocode']});
        autocomplete1 = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),



          {types: ['geocode']});  



        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);
        autocomplete1.addListener('place_changed', fillInAddress);


    }
      


    function fillInAddress() 
    {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();



        for (var component in componentForm) {



          document.getElementById(component).value = '';



          document.getElementById(component).disabled = false;



        }



        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



          var addressType = place.address_components[i].types[0];



          if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



      //alert(val);



    }



  }



}



      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      function geolocate() 
      {


  
        if (navigator.geolocation) {



          navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
             
              

            };
            
            var pickuplat = geolocation.lat;
            var pickuplong = geolocation.lng;
            $('#pickup_latitude').val(pickuplat); 
            $('#pickup_longitude').val(pickuplong); 

            var circle = new google.maps.Circle({



              center: geolocation,



              radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



          });



        }



      }
       

  </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
     
    


<script type="text/javascript">
    $(document).ready(function(){
      $("#pickup").PlacePicker({
        btnClass:"btn btn-xs btn-default",
        key:"AIzaSyBAGm4WfH69Ny5DmWFAA88s26UwiNqHMn8",
        center: {lat: 17.6868, lng: 83.2185},
        success:function(data,address){
          //data contains address elements and
          //address conatins you searched text
          //Your logic here
          $("#pickup").val(data.formatted_address);
        }
      });
    });
</script>

<script>
/*
$("#pickup_country").PlacePicker({

  key:"AIzaSyB6jpjQRZn8vu59ElER36Q2LaxptdAghaA"

});

$("#pickup_country").PlacePicker({

  title: "Popup Title Here"

});

$("#pickup_country").PlacePicker({

  btnClass: "btn btn-secondary btn-sm"

});

$("#pickup_country").PlacePicker({

    center: {lat: -34.397, lng: 150.644}



});

$("#pickup_country").PlacePicker({

   zoom: 10

});

$("#pickup_country").PlacePicker({

  success:function(data,address){

    //data contains address elements and

    //address conatins you searched text

    //Your logic here

    $("#pickup_country").val(data.formatted_address);

  }

});

*/

</script>

</body>

</html>