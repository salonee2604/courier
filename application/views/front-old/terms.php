<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>GPS Courier</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url();?>front_assets/img/favicon.png" rel="icon">
  <link href="<?php echo base_url();?>front_assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url();?>front_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url();?>front_assets/css/style.css" rel="stylesheet">

  
  
</head>

<body>

  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto:info@gps-kurier.ch">info@gps-kurier.ch</a>
        <i class="icofont-phone"></i> 044 506 79 79
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="https://votivetech.in/courier/">GPS Courier<span>.</span></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->

      <nav class="nav-menu d-none d-lg-block">
       <ul>
          <li class="active"><a href="<?php echo base_url(); ?>">Zuhause</a></li>
          <li><a href="<?php echo base_url();?>about">ÜBER UNS</a></li>
          <li><a href="<?php echo base_url();?>team">Mannschaft</a></li>
          <li><a href="<?php echo base_url();?>contact">Kontakt</a></li>

          <!--<li><a href="#portfolio">Portfolio</a></li>-->
          <li><a href="<?php echo base_url();?>restaurantLogin">Anmeldung</a></li>
          <li><a href="<?php echo base_url();?>restaurantSignup">Anmelden</a>
          </li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
 <!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->


 <!-- ======= Banner start Section ======= -->
 <div class="container-fluid banner-bg">
     <div class="banner">
         <h3>Terms & amp; Bedingungen
</h3>
     </div>
 </div>
 <!-- ======= Banner end Section ======= -->
    
<section id="team" class="team section-bg">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        
        
        <div class="row">
            <div class="col-md-12">
<h3>Overview</h3>
<p>The website and the solutions are operated and managed by separate entities. Refsnes Data offers the website (w3schools.com) including all its content, information, and related tools.</p><p> The solutions (Such as Spaces, Courses, and My learning) and billing system are operated by W3schools Network. The two entities are separately responsible for the services that they provide, manage and operate.</p>



<h3>Site Usage Terms</h3>
<p>By agreeing to these Terms of Service, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.</p>





<h3>General Conditions</h3>
<p>We reserve the right to refuse service to anyone for any reason at any time.</p>

<p>You understand that your content may be transferred unencrypted and involve (a) transmissions over various networks, and (b) changes to conform and adapt to technical requirements of connecting networks or devices.</p>

<p>Credit card information is always encrypted during transfer over networks, and are processed and handled by our selected Payment Service Providers. We only preserve the last four-digit and expiry date of your Credit card. This is, among other things, to let you know in advance if your Credit card is soon to expire, so that we can assure the continuous running of your Services with us.</p>





      
      

 
 

         
         

        </div>

      </div>
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
   <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Abonnieren Sie unseren Newsletter
</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-contact">
            <h3>GPS <span>Courier</span></h3>
            <p>
              Kalkofenstrasse 19, 8810 Horgen<br>
             
              <strong>Phone:</strong> 044 506 79 79<br>
              <strong>Email:</strong> info@gps-kurier.ch<br>
            </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>home">Zuhause</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>about">ÜBER UNS</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>team">Mannschaft</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>terms">Nutzungsbedingungen</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>privacy">Datenschutz-Bestimmungen
</a></li>
            </ul>
          </div>

       
       

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright 2021 <strong><span>GPS Courier</span></strong>. Alle Rechte vorbehalten
      </div>
   
   
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url();?>front_assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url();?>front_assets/js/main.js"></script>

</body>
<style>
.section-bg {
    background-color: #fff;
    float: left;
    width: 100%;
}
section#team h3 {
    float: left;
    width: 100%;
}
</style>
</html>