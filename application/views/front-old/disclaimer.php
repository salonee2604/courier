<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>GPS Courier</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url();?>front_assets/img/favicon.png" rel="icon">
  <link href="<?php echo base_url();?>front_assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url();?>front_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url();?>front_assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url();?>front_assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: BizLand - v1.2.1
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
 <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto:info@gps-kurier.ch">info@gps-kurier.ch</a>
        <i class="icofont-phone"></i> 044 506 79 79
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="https://votivetech.in/courier/">GPS Courier<span>.</span></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->

      <nav class="nav-menu d-none d-lg-block">
       <ul>
          <li class="active"><a href="<?php echo base_url(); ?>">Zuhause</a></li>
          <li><a href="<?php echo base_url();?>about">ÜBER UNS</a></li>
          <li><a href="<?php echo base_url();?>team">Mannschaft</a></li>
          <li><a href="<?php echo base_url();?>contact">Kontakt</a></li>

          <!--<li><a href="#portfolio">Portfolio</a></li>-->
          <li><a href="<?php echo base_url();?>restaurantLogin">Anmeldung</a></li>
          <li><a href="<?php echo base_url();?>restaurantSignup">Anmelden</a>
          </li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
 <!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->


 <!-- ======= Banner start Section ======= -->
 <div class="container-fluid banner-bg">
     <div class="banner">
         <h3>Haftungsausschluss</h3>
     </div>
 </div>
 <!-- ======= Banner end Section ======= -->
    
<section id="team" class="team section-bg">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        
        
        <div class="row">
            <div class="col-md-12">
<h3>Kontakt-Adresse</h3>
<p>valmirFejzullahu</p>
<p>Kalkofenstrasse 19</p>
<p>8810 Horgen
Schweiz</p>
<p>E-Mail:v.fejzullahu@gps-kurier.ch</p>



<h3>Haftungsausschluss</h3>
<p>Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und Vollständigkeit der Informationen.</p>
<p>Haftungsansprüche gegen den Autor wegen Schäden materieller oder immaterieller Art, welche aus dem Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der Verbindung oder durch technische Störungen entstanden sind, werden ausgeschlossen.</p>
<p>Alle Angebote sind unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne besondere Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.</p>



<h3>Haftungsausschluss für Links</h3>
<p>Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres Verantwortungsbereichs. Es wird jegliche Verantwortung für solche Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten erfolgen auf eigene Gefahr des jeweiligen Nutzers.</p>



<h3>Urheberrechte</h3>
<p>Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf dieser Website, gehören ausschliesslich valmirFejzullahu oder den speziell genannten Rechteinhabern. Für die Reproduktion jeglicher Elemente ist die schriftliche Zustimmung des Urheberrechtsträgers im Voraus einzuholen.</p>


<h3>Quelle: SwissAnwalt</h3>


 

         
         

        </div>

      </div>
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <!-- ======= Footer ======= -->
   <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Abonnieren Sie unseren Newsletter
</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-contact">
            <h3>GPS <span>Courier</span></h3>
            <p>
              Kalkofenstrasse 19, 8810 Horgen<br>
             
              <strong>Phone:</strong> 044 506 79 79<br>
              <strong>Email:</strong> info@gps-kurier.ch<br>
            </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>home">Zuhause</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>about">ÜBER UNS</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>team">Mannschaft</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>terms">Nutzungsbedingungen</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>privacy">Datenschutz-Bestimmungen
</a></li>
            </ul>
          </div>

       
       

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright 2021 <strong><span>GPS Courier</span></strong>. Alle Rechte vorbehalten
      </div>
   
   
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url();?>front_assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url();?>front_assets/js/main.js"></script>

</body>
<style>
.section-bg {
    background-color: #fff;
    float: left;
    width: 100%;
}
section#team h3 {
    float: left;
    width: 100%;
}
</style>
</html>