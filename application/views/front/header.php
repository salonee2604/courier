<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>GPS KURIER</title>
      <meta content="" name="description">
      <meta content="" name="keywords">
      <!-- Favicons -->  
      <link href="<?php echo base_url();?>front_assets/img/favicon.png" rel="icon">
      <link href="<?php echo base_url();?>front_assets/img/apple-touch-icon.png" rel="apple-touch-icon">
      <!-- Google Fonts -->  
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
      <!-- Vendor CSS Files -->  
      <link href="<?php echo base_url();?>front_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>front_assets/vendor/icofont/icofont.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>front_assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>front_assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>front_assets/vendor/venobox/venobox.css" rel="stylesheet">
      <link href="<?php echo base_url();?>front_assets/vendor/aos/aos.css" rel="stylesheet">
      <!-- Template Main CSS File -->  
      <link href="<?php echo base_url();?>front_assets/css/style.css" rel="stylesheet">
      <!-- =======================================================  * Template Name: BizLand - v1.2.1  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/  * Author: BootstrapMade.com  * License: https://bootstrapmade.com/license/  ======================================================== -->
   </head>
   <body>
      <!-- ======= Top Bar ======= -->  
      <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
         <div class="container d-flex">
            <div class="contact-info mr-auto">        <i class="icofont-envelope"></i> <a href="mailto:info@gps-kurier.ch">info@gps-kurier.ch</a>        <i class="icofont-phone"></i> 044 506 79 79      </div>
            <div class="social-links">        
               <a href="https://www.facebook.com/profile.php?id=100070673572348" class="facebook"><i class="icofont-facebook"></i></a>        
               <a href="https://www.instagram.com/gps.kurier/" class="instagram"><i class="icofont-instagram"></i></a>
               <a href="https://www.linkedin.com/in/gps-kurier-a2378b217" class="linkedin"><i class="bx bxl-linkedin"></i></a>        
                     </div>
         </div>
      </div>
      <!-- ======= Header ======= -->  
      <header id="header" class="fixed-top">
         <div class="container d-flex align-items-center">
            <h1 class="logo mr-auto"><a href="https://votivetech.in/courier/">GPS KURIER<span>.</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->      
            <nav class="nav-menu d-none d-lg-block">
               <ul>
                  <li class="active"><a href="<?php echo base_url(); ?>">Zuhause</a></li>
                  <li><a href="<?php echo base_url();?>about">über uns</a></li>
                  <li><a href="<?php echo base_url();?>team">Mannschaft</a></li>
				   <li><a href="<?php echo base_url();?>referenzen">Referenzen</a></li>
                  <li><a href="<?php echo base_url();?>contact">Kontakt</a></li>
                  <!--<li><a href="#portfolio">Portfolio</a></li>-->          
                  <li><a href="<?php echo base_url();?>restaurantLogin">Anmeldung</a></li>
                  <li><a href="<?php echo base_url();?>restaurantSignup">Registrieren</a>          </li>
               </ul>
            </nav>
            <!-- .nav-menu -->    
         </div>
      </header>
      <!-- End Header -->