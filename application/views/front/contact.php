<style>
    ul.contact_time {
    margin: 0 0 0 -40px;
}
ul.contact_time li {
    list-style: none;
}
.btWorkingHoursInnerTitle {
    font-weight: 600;
}
.btWorkingHoursInnerRow {
    margin-bottom: 7px;
}
</style>

<main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->


 <!-- ======= Banner start Section ======= -->
 <div class="container-fluid banner-bg">
     <div class="banner">
         <h3>Kontakt</h3>
     </div>
 </div>
 <!-- ======= Banner end Section ======= -->
    
 <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <!-- <div class="section-title">     
          <h3><span>Kontaktiere uns</span></h3>
          <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
        </div> -->

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-md-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3> Adresse</h3>
              <p>Kalkofenstrasse 19, 8810 Horgen</p>
            </div>
          </div>
           <div class=" col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>
              <h3>E-Mail</h3>
              <p><a href="mailto:info@gps-kurier.ch">info@gps-kurier.ch</a></p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box mb-4">
            <i class="bx bxs-time"></i>


              <h3>Öffnungszeiten</h3>
              
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Montag:</div><div class="btWorkingHoursInnerContent">17:00-22:00 Uhr</div></div>
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Dienstag:</div><div class="btWorkingHoursInnerContent">11:00-14:00 Uhr
	17:00-22:00	Uhr</div></div>
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Mittwoch:</div><div class="btWorkingHoursInnerContent">17:00-22:00 Uhr</div></div>
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Donnerstag:</div><div class="btWorkingHoursInnerContent">17:00-22:00 Uhr</div></div>
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Freitag:</div><div class="btWorkingHoursInnerContent">17:00-22:00 Uhr</div></div>
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Samstag:</div><div class="btWorkingHoursInnerContent">11:00-14:00 Uhr
	17:00-22:00	Uhr</div></div>
              <div class="btWorkingHoursInnerRow"><div class="btWorkingHoursInnerTitle">Sonntag:</div><div class="btWorkingHoursInnerContent">17:00-22:00 Uhr</div></div>
              
              
  </div>
</div>

         

          <div class="col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Telefonnummer</h3>
              <p>044 506 79 79</p>
            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

          <div class="col-lg-6 ">
            <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2708.489721376956!2d8.605518615111801!3d47.246126379162405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479aaf297917a48f%3A0x7f04c5cd427648de!2sKalkofenstrasse%2019%2C%208810%20Horgen%2C%20Switzerland!5e0!3m2!1sen!2sin!4v1620027308292!5m2!1sen!2sin" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
           
           
          </div>
          
          <div class="col-lg-6">
                        <?php echo $this->session->flashdata('message');?>        

            <form action="<?php echo base_url();?>contact" method="post">
              <div class="form-row">
                <div class="col form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Name, Vorname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                   <?php echo form_error('name','<span class="text-danger">','</span>'); ?>                </div>
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="E-Mail" data-rule="email" data-msg="Please enter a valid email" />
                      <?php echo form_error('email','<span class="text-danger">','</span>'); ?>                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Betrifft" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
<?php echo form_error('subject','<span class="text-danger">','</span>'); ?>              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mitteilung"></textarea>
                <?php echo form_error('message','<span class="text-danger">','</span>'); ?>    
                
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Wird geladen</div>
                <div class="error-message"></div>
                <div class="sent-message">Ihre Nachricht wurde gesendet. Vielen Dank!</div>
              </div>
              <div class="text-center"><input class="btn btn-primary" type="submit" name="addContactsubmit" value="Nachricht senden"></div>
            </form>
          </div>
        </div>
      </div>
    </section><!-- End Contact Section -->
  </main><!-- End #main -->