<!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Abonnieren Sie unseren Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Abonnieren">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-contact">
            <h3>GPS <span>KURIER</span></h3>
            <p>
              Kalkofenstrasse 19, 8810 Horgen<br>
             
              <strong>Phone:</strong> 044 506 79 79<br>
              <strong>Email:</strong> info@gps-kurier.ch<br>
            </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Nützliche Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>home">Zuhause</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>about">ÜBER UNS</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url();?>team">Mannschaft</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>terms">Nutzungsbedingungen</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>privacy">Datenschutz-Bestimmungen</a></li>
			  <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>disclaimer">Haftungsausschluss</a></li>
			   <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url(); ?>imressum">Impressum</a></li>
            </ul>
          </div>

       
       

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Unsere sozialen Netzwerke</h4>
            <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
            <div class="social-links mt-3">
              
              <a href="https://www.facebook.com/profile.php?id=100070673572348" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="https://www.instagram.com/gps.kurier/" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="https://www.linkedin.com/in/gps-kurier-a2378b217" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright 2021 <strong><span>GPS KURIER</span></strong>. Alle Rechte vorbehalten
      </div>
   
   
    </div>
  </footer><!-- End Footer -->

  <!-- <div id="preloader"></div> -->
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url();?>front_assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url();?>front_assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url();?>front_assets/js/main.js"></script>

</body>

</html>