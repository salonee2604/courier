<main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->


    <!-- ======= Clients Section ======= -->
  

    <!-- ======= Testimonials Section ======= -->
    <!-- End Testimonials Section -->

    <!-- ======= Portfolio Section ======= -->
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Passwort zurücksetzen</h2>
          <div class="row">
            <div class="col-lg-3">

          </div>
          <div class="col-lg-6 ">
            <div id="msg_div">
          <?php echo $this->session->flashdata('message');?>        
        </div>
          </div>
          <div class="col-lg-3">
          </div>
        </div>
        </div>

       

        <div class="row" data-aos="fade-up" data-aos-delay="100">

          
          <div class="col-lg-3">

          </div>

          <div class="col-lg-6 login-page">
            <form action="<?php echo base_url();?>Home/driverresetpasspa" method="post">
               <input id="user_id" type="hidden" class="form-control " name="user_id" value="<?php echo $this->uri->segment(3); ?>">
				<div class="form-row">
                <div class="col form-group">
                  <input type="text" class="form-control" name="token_id" id="token_id" placeholder="Verifizierungs-Schlüssel" required/>
                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>
                </div>
				 </div>
				 <div class="form-row">
				 <div class="col form-group">
                  <input type="password" class="form-control" name="password" id="password" placeholder="Passwort" required/>
                  <?php echo form_error('password','<span class="text-danger">','</span>'); ?>
                </div>
				 </div>
				
				 <div class="form-row">
				 <div class="col form-group">
                  <input type="password" class="form-control" name="c_password" id="c_password" placeholder="Kennwort bestätigen" required/>
                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>
                </div>
				 </div>
			
                          
              <div class="col-lg-3">
              </div>
              <div class="text-center"><input type="submit" name="submit" value="Link zum Zurücksetzen des Passworts senden" class="btn btn-primary"></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->