<main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->


 <!-- ======= Banner start Section ======= -->
 <div class="container-fluid banner-bg">
     <div class="banner">
         <h3>Haftungsausschluss</h3>
     </div>
 </div>
 <!-- ======= Banner end Section ======= -->
    
<section id="team" class="team section-bg">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        
        
        <div class="row">
            <div class="col-md-12">
<h3>Kontakt-Adresse</h3>
<p>VS Fejzullahu
</p>
<p>Kalkofenstrasse 19</p>
<p>8810 Horgen
Schweiz</p>
<p>E-Mail: info@gps-kurier.ch</p>
<h3>Haftungsausschluss</h3>
<p>Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und Vollständigkeit der Informationen.</p>
<p>Haftungsansprüche gegen den Autor wegen Schäden materieller oder immaterieller Art, welche aus dem Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der Verbindung oder durch technische Störungen entstanden sind, werden ausgeschlossen.</p>
<p>Alle Angebote sind unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne besondere Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.</p>



<h3>Haftungsausschluss für Links</h3>
<p>Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres Verantwortungsbereichs. Es wird jegliche Verantwortung für solche Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten erfolgen auf eigene Gefahr des jeweiligen Nutzers.</p>



<h3>Urheberrechte</h3>
<p>Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf dieser Website, gehören ausschliesslich valmirFejzullahu oder den speziell genannten Rechteinhabern. Für die Reproduktion jeglicher Elemente ist die schriftliche Zustimmung des Urheberrechtsträgers im Voraus einzuholen.</p>


<h3>Quelle: SwissAnwalt</h3>


 

         
         

        </div>

      </div>
      </div>
    </section>

  </main><!-- End #main -->  <style>.section-bg {    background-color: #fff;    float: left;    width: 100%;}section#team h3 {    float: left;    width: 100%;}</style>