<style>
.title {
  text-decoration: underline;
}
</style>
<main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">	  </div>
    </section><!-- End Featured Services Section -->
 <!-- ======= Banner start Section ======= -->
 <div class="container-fluid banner-bg">
     <div class="banner">
         <h3>Mannschaft </h3>
     </div>
 </div>
 <!-- ======= Banner end Section ======= -->
    
<section id="team" class="team section-bg">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        <div class="section-title">
        
        
          <h3>Unsere fleißige
 <span>Mannschaft
</span></h3>
          <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="member-img">
                <img src="front_assets/img/team-1.jpg" class="img-fluid" alt="">
             
             
              </div>
              <div class="member-info">
                <h4 class="title">Walter White</h4>
                <span>Geschäftsführer</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="member-img">
                <img src="front_assets/img/team-2.jpg" class="img-fluid" alt="">
                
                
              </div>
              <div class="member-info">
                <h4 class="title">Sarah Jhonson</h4>
                <span>Produktmanager</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
                <img src="front_assets/img/team-3.jpg" class="img-fluid" alt="">
                
                
              </div>
              <div class="member-info">
                <h4 class="title">William Anderson</h4>
                <span>CTO</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
            <div class="member">
              <div class="member-img">
                <img src="front_assets/img/team-4.jpg" class="img-fluid" alt="">
                
                
              </div>
              <div class="member-info">
                <h4 class="title">Amanda Jepson</h4>
                <span>Buchhalterin</span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>

  </main><!-- End #main -->
<style>.section-bg {    background-color: #fff;    float: left;    width: 100%;}</style> 