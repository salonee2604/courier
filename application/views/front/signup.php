<main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->


    <!-- ======= Clients Section ======= -->
  

    <!-- ======= Testimonials Section ======= -->
    <!-- End Testimonials Section -->

    <!-- ======= Portfolio Section ======= -->
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>RESTAURANT-ANMELDUNG
</h2>
          <div class="row">
            <div class="col-lg-3">

          </div>
          <div class="col-lg-6">
            <div id="msg_div">
          <?php echo $this->session->flashdata('message');?>        
        </div>
          </div>
          <div class="col-lg-3">
          </div>
        </div>
        </div>

       

        <div class="row" data-aos="fade-up" data-aos-delay="100">
  
         <div class="col-lg-12 login-page" style="padding:25px;">
          <form action="<?php echo base_url();?>login/restaurantSignup" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!">
              <div class="form-row">
           
                <div class="col form-group col-md-6">
                  <input name="first_name" class="form-control" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" placeholder="Name"/>
                  <?php echo form_error('first_name','<span class="text-danger">','</span>'); ?></div>
                <div class="col form-group col-md-6">
                   <input name="surname" class="form-control" type="text" id="surname" value="<?php echo set_value('surname'); ?>" placeholder="Nachname"/>
                  <?php echo form_error('surname','<span class="text-danger">','</span>'); ?>
                </div>
                </div>
          
              
          
            <div class="form-row">
              <div class="col form-group col-md-6">
                <input name="mobile_number" class="form-control" type="text" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>" placeholder="Handynummer" />
                <?php echo form_error('mobile_number','<span class="text-danger">','</span>'); ?>
              </div>
              <div class="col form-group col-md-6">
                <input name="restaurant_name" class="form-control" type="text" id="restaurant_name" value="<?php echo set_value('restaurant_name'); ?>" placeholder="Name der Firma"/>
              <?php echo form_error('restaurant_name','<span class="text-danger">','</span>'); ?>
              </div>
              </div>
              <div class="form-row">
                   <div class="col form-group col-md-4 ">
                <input type="email" class="form-control" name="email" onchange="jQuery(this).next('input').val(this.value);" placeholder="Email">
              <?php echo form_error('email','<span class="text-danger">','</span>'); ?>
               </div> 
             <div class="col form-group col-md-4">
              <input type="password" name="password" class="form-control" id="password" value="<?php echo set_value('password'); ?>" placeholder="Passwort"/>
              <?php echo form_error('password','<span class="text-danger">','</span>'); ?>
              </div>
               <div class="col form-group col-md-4">
                 <input type="password" name="c_password" class="form-control" id="c_password" value="<?php echo set_value('client_conf_password'); ?>" placeholder="Passwort bestätigen"/>
                <?php echo form_error('c_password','<span class="text-danger">','</span>'); ?>
              </div>
            </div>
          
          
            <div class="form-row">
               
                <div class="col form-group col-md-4">
                <input type="text" class="form-control pac-target-input" name="address"  id="autocomplete" value="" required="required" onfocus="geolocate()" placeholder="Strasse" autocomplete="off">
               <?php echo form_error('address','<span class="text-danger">','</span>'); ?>
               </div> 
			   <div class="col form-group col-md-4">
                 <input name="zipcode" class="form-control" type="text" id="zipcode" value="<?php echo set_value('zipcode'); ?>" placeholder="PLZ"/>
                <?php echo form_error('zipcode','<span class="text-danger">','</span>'); ?>
              </div>
                <div class="col form-group col-md-4">
                 <input name="city" class="form-control" type="text" id="city" value="<?php echo set_value('city'); ?>" placeholder="Ort"/>
                <?php echo form_error('city','<span class="text-danger">','</span>'); ?>
              </div>
              </div>
			
             <div class="form-row">
              <div class="col form-group col-md-4">
                 <input name="country" class="form-control" type="text" id="country" value="<?php echo set_value('country'); ?>" placeholder="Land"/>
                <?php echo form_error('country','<span class="text-danger">','</span>'); ?>
              </div>
              <div class="col form-group col-md-4">
                 <input type="file" name="image" class="form-control" id="image" value="<?php echo set_value('image'); ?>" />
                <?php echo form_error('image','<span class="text-danger">','</span>'); ?>
              </div>
             </div>
            <div class="form-row">
               <input type="hidden" name="bank_details"  value="bankdetail">
			  <h5>Bankverbindung</h5> </div>
             <!--  <div class="col form-group col-md-12">

                <textarea name="bank_details" class="form-control" value="" placeholder="Bankdaten" style="height:130px;"></textarea>

                <?php echo form_error('bank_details','<span class="text-danger">','</span>'); ?>

              </div> -->
			   <div class="form-row">
			   <div class="col form-group col-md-6">
                 <input name="name_holder" class="form-control" type="text" id="name_holder" value="<?php echo set_value('name_holder'); ?>" placeholder="Name auf der Karte"/>
                <?php echo form_error('name_holder','<span class="text-danger">','</span>'); ?>
              </div>
			  
			   <div class="col form-group col-md-6">
                 <input name="card_no" class="form-control" type="text" id="card_no" value="<?php echo set_value('card_no'); ?>" placeholder="Kartennummer"/>
                <?php echo form_error('card_no','<span class="text-danger">','</span>'); ?>
              </div>
			  </div>
			  
			   <div class="form-row">
			  
			   <div class="col form-group col-md-4">
                 <input name="expire_date" class="form-control" type="text" id="expire_date" value="<?php echo set_value('expire_date'); ?>" placeholder="gültig bis -02/2021"/>
                <?php echo form_error('expire_date','<span class="text-danger">','</span>'); ?>
              </div>
              <div class="col form-group col-md-4">
                 <input name="security_code" class="form-control" type="text" id="security_code" value="<?php echo set_value('security_code'); ?>" placeholder="Sicherheitscode"/>
                <?php echo form_error('security_code','<span class="text-danger">','</span>'); ?>
              </div>
			 <div class="col form-group col-md-4">
            <select name="status" id="status" class="form-control">
              <option value="1">Aktiv</option>
              <option value="0">Inaktiv</option>
              </select>
            <?php echo form_error('status','<span class="text-danger">','</span>'); ?>
            </div>

                  
			  </div>
			  <input type="hidden" name="pickup_latitude" id="pickup_latitude" value="">
			  <input type="hidden" name="pickup_longitude" id="pickup_longitude" value="">
              <div class="text-center"><input type="submit" name="submit" value="Hinzufügen" class="btn btn-primary margin-top-30"></div>
            </form>
          </div>

        </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/PlacePicker.js"></script>
  <script type="text/javascript">
    

      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;



      var componentForm = {



        //street_number: 'short_name',



        //route: 'long_name',



        //locality: 'long_name',



        //administrative_area_level_1: 'short_name',



        //country: 'long_name',



        //postal_code: 'short_name'



      };



      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),



          {types: ['geocode']});
        autocomplete1 = new google.maps.places.Autocomplete(



          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),



          {types: ['geocode']});  



        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);
        autocomplete1.addListener('place_changed', fillInAddress);


    }
      


    function fillInAddress() 
    {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();



        for (var component in componentForm) {



          document.getElementById(component).value = '';



          document.getElementById(component).disabled = false;



        }



        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



          var addressType = place.address_components[i].types[0];



          if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



      //alert(val);



    }



  }



}



      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      function geolocate() 
      {


  
        if (navigator.geolocation) {



          navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
             
              

            };
            
            var pickuplat = geolocation.lat;
            var pickuplong = geolocation.lng;
            $('#pickup_latitude').val(pickuplat); 
            $('#pickup_longitude').val(pickuplong); 

            var circle = new google.maps.Circle({



              center: geolocation,



              radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



          });



        }



      }
       

  </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
     
    


<script type="text/javascript">
    $(document).ready(function(){
      $("#pickup").PlacePicker({
        btnClass:"btn btn-xs btn-default",
        key:"AIzaSyBAGm4WfH69Ny5DmWFAA88s26UwiNqHMn8",
        center: {lat: 17.6868, lng: 83.2185},
        success:function(data,address){
          //data contains address elements and
          //address conatins you searched text
          //Your logic here
          $("#pickup").val(data.formatted_address);
        }
      });
    });
</script>

<script>
/*
$("#pickup_country").PlacePicker({

  key:"AIzaSyB6jpjQRZn8vu59ElER36Q2LaxptdAghaA"

});

$("#pickup_country").PlacePicker({

  title: "Popup Title Here"

});

$("#pickup_country").PlacePicker({

  btnClass: "btn btn-secondary btn-sm"

});

$("#pickup_country").PlacePicker({

    center: {lat: -34.397, lng: 150.644}



});

$("#pickup_country").PlacePicker({

   zoom: 10

});

$("#pickup_country").PlacePicker({

  success:function(data,address){

    //data contains address elements and

    //address conatins you searched text

    //Your logic here

    $("#pickup_country").val(data.formatted_address);

  }

});

*/

</script>