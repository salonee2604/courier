<main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->


    <!-- ======= Clients Section ======= -->
  

    <!-- ======= Testimonials Section ======= -->
    <!-- End Testimonials Section -->

    <!-- ======= Portfolio Section ======= -->
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Restaurant Login</h2>
          <div class="row">
            <div class="col-lg-3">

          </div>
          <div class="col-lg-6 ">
            <div id="msg_div">
          <?php echo $this->session->flashdata('message');?>        
        </div>
          </div>
          <div class="col-lg-3">
          </div>
        </div>
        </div>

       

        <div class="row" data-aos="fade-up" data-aos-delay="100">

          
          <div class="col-lg-3">

          </div>

          <div class="col-lg-6 login-page">
            <form action="<?php echo base_url();?>login/restaurantLogin" method="post">
              <div class="form-row">
                
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Deine E-Mail
" data-rule="email" data-msg="Please enter a valid email" />
                  <?php echo form_error('email','<span class="text-danger">','</span>'); ?>

                </div>
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="password" id="password" placeholder="Passwort" data-rule="minlen:4" data-msg="Please Enter Password" />
                <?php echo form_error('password','<span class="text-danger">','</span>'); ?>

              </div>
              
              <div class="col-lg-3">
              </div>
              <div class="text-center"><input type="submit" name="submit" value="Anmeldung" class="btn btn-primary"></div>
			  <a href="<?php echo base_url(); ?>passwordreset">Passwort vergessen?</a>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->