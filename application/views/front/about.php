  <main id="main">  <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        

      </div>
    </section><!-- End Featured Services Section -->


 <!-- ======= Banner start Section ======= -->
 <div class="container-fluid banner-bg">
     <div class="banner">
         <h3>über uns</h3>
     </div>
 </div>
 <!-- ======= Banner end Section ======= -->
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
    
      <div class="container" data-aos="fade-up">

     <div class="col-md-12">
         
<section class="about-us py-5 " id="about-us">
    <div class="container mt-5">
	<div class="row">
		<div class="col-md-6">
		    <h1 class='' style="color:#106eea;">Herzlich willkommen!
!</h1>
		    <h2>GPS KURIER</h2>
		    <hr>
		    <p>GPS KURIER ist ein Junges Startup-Unternehmen, welches sich auf das Kurieren von Restaurants spezialisiert. Da wir selbst aus der Gastronomie mitLieferdiensten kommen, kennen wir die Problematik aus Sicht der Restaurants so wie auch aus der Sicht der Kuriere. Deshalb hat GPS KURIER ein Produkt auf die Beine gestellt, welches für die Restaurants so wie auch für die Kuriere die Arbeit und die Organisation erleichtern wird.  </p>
		    <p>Ihre bisherige Organisation wird nun durch GPS Kurier wesentlich erleichtert. Sie sehen nicht nur wo Ihre Kuriere sind, sondern auch wann Sie die bestellte Ware zubereiten können.
Testen Sie unser Angebot für 14 Tage kostenlos und lassen Sie sich von GPS Kurier überzeugen.
</p>
		    
		    
		    <p>Falls wir Ihr Interesse geweckt haben, können Sie sich für ein Beratungsgespräch sehr gerne bei uns per Telefon oder per Mail melden.</p>
		   
		   

		</div>
		<div class="col-md-6">
		    <img src="front_assets/img/ab-12.png "alt="" style="width:100%;" class="about-img-part">
		</div>
	</div>
</div>
</section>
     </div>
      </div>
    </section><!-- End Contact Section -->
  </main><!-- End #main -->