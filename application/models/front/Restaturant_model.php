<?php

class Restaturant_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	public function addRestaurant($post)
	{
	    
	    $this->db->insert('restaurant', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
	}
	
	public function getRestaurantList()
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('user_type','restaurant');
		$this->db->order_by('restaurant_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}
	
	function deleteDriver($driver_id)
	{
		$this->db->delete('driver', array('driver_id' => $driver_id));		
		return 1;		
	}
	
	public function getRestaturantbyid($restaurant_id)
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('restaurant_id',$restaurant_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function updateRestaurant($post,$restaurant_id)
	{
	    $this->db->where('restaurant_id', $restaurant_id);
		$this->db->update('restaurant', $post);
		return true;
	}
	
   public function getreferenzenList()
   {            
   $this->db->select('*');	
   $this->db->from('referenzenpage');
   $this->db->order_by('rp_id','desc');
   $query = $this->db->get(); 
   return $query->result() ; 
   }
   
   public function getrestaurantlogo()
    {
     
        $this->db->select('*');
		$this->db->from('restaurantlogo');
		$this->db->order_by('rs_id','desc');
	    $query = $this->db->get();
        return $query->result() ;
        
    }
	
	 public function email($to, $subject, $msg) {
        $this->load->library('email');

        $config['protocol'] = 'sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('info@votivetech.in', 'Kourier');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->send();         
    

    }
	
	function email_exists($email)
    {
    $this->db->select('*');
    $this->db->from('restaurant');
    $this->db->where('email', $email);
    return $this->db->get()->result();
	// return $this->db->get()->result();
		
    }
	
	function token_exists($token)
    {
    $this->db->select('*');
    $this->db->from('restaurant');
    $this->db->where('vrfn_code', $token);
    return $this->db->get()->result();
	// return $this->db->get()->result();
		
    }
    function driver_token_exists($token)
    {
    $this->db->select('*');
    $this->db->from('driver');
    $this->db->where('vrfn_code', $token);
    return $this->db->get()->result();
	// return $this->db->get()->result();
		
    }

	

}
?>