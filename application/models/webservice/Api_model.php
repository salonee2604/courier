<?php
class Api_model extends CI_Model 
{
	function __construct()
	{
       parent::__construct();
	   $this->load->database();
	}
	

	public function addSocialUser($post)
	{
		$this->db->insert('tbl_user', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result;
	}
 	public function checkSocialUser($check)
 	{		
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('user_email', $check['user_email']);
		$this->db->or_where('user_phone', $check['user_phone']);
		if(!empty($this->session->userdata("user_id")))
		{
			$this->db->where("FIND_IN_SET('".$this->session->userdata("user_id")."', user_all_level)");
		}
		$query = $this->db->get();
		return $query->result() ;
 	} 

 	public function loginUser($post)
 	{		
		$user_email_phone = $post['user_email_phone'];
		$this->db->select('a.*,b.state_name,c.customer_unic_id,c.customer_type_name');
		$this->db->from('tbl_user a');
		$this->db->join('state b','a.user_state_id = b.state_id','left');
		$this->db->join('tbl_customer_type c','a.customer_type_id = c.customer_type_id','left');
		$this->db->where("(a.user_email = '".$user_email_phone."' OR a.user_phone = '".$user_email_phone."')");
		$this->db->where('a.user_password', $post['user_password']);
		$this->db->where('a.user_status', '1');
		$this->db->where('a.user_status_type', 'Approved');
		if(!empty($this->session->userdata("user_id")))
		{
			$this->db->where("FIND_IN_SET('".$this->session->userdata("user_id")."', a.user_all_level)");
		}
		$query = $this->db->get();		

		return $query->result() ;
 	} 

 	public function loginPosUser($post)
 	{		
		$user_email_phone = $post['user_email_phone'];
		$this->db->select('a.*,b.state_name');
		$this->db->from('tbl_user a');
		$this->db->join('state b','a.user_state = b.id','left');		
		$this->db->where("(a.user_email = '".$user_email_phone."' OR a.user_phone = '".$user_email_phone."')");
		$this->db->where('a.user_password', $post['user_password']);
		$this->db->where('a.user_status', '1');
		if(!empty($this->session->userdata("user_id")))
		{
			$this->db->where("FIND_IN_SET('".$this->session->userdata("user_id")."', a.user_all_level)");
		}
		$query = $this->db->get();	
		return $query->result();
 	} 

 	public function showProfile($user_id)
 	{		
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('user_id', $user_id);
		$this->db->where('user_status', '1');
		if(!empty($user_id))
		{
			$this->db->where("FIND_IN_SET('".$user_id."', user_all_level)");
		}
		$query = $this->db->get();
		return $query->result() ;
 	}

 	public function getLeadByUserId($user_id ,$sync_date_time = NULL)
 	{
 		$this->db->select('*');
 		$this->db->from('tbl_leads a');
 		$this->db->join('tbl_assign_user b', 'a.lead_id = b.lead_id', 'inner');
 		$this->db->where('b.user_id',  $user_id);
 		if($sync_date_time)
 		$this->db->where('a.sync_date_time >', $sync_date_time);
 	    if(!empty($user_id))
		{
			$this->db->where("FIND_IN_SET('".$user_id."', a.user_all_level)");
		}
 		$query = $this->db->get();
 		return $query->result();
 	}

 	public function updateProfile($post,$driver_id)
 	{
 		
		$this->db->where('driver_id', $driver_id);
		$this->db->update('driver', $post);
		return true;
 	}
 	public function changePassword($post)
 	{
 		$data['user_password'] = $post['user_password'];
		$data['user_updated_date'] = $post['user_updated_date'];
		$this->db->where('user_id', $post['user_id']);
		$this->db->update('tbl_user', $data);
		return true;
 	}

 	public function updateloginStatus($post)
 	{
 		$data['user_log_status'] = $post['user_log_status'];
		$this->db->where('user_id', $post['user_id']);
		$this->db->update('tbl_user', $data);
		return true;
 	}

 	public function getLeadMeetingList($user_id , $sync_date_time = 0)
 	{
 		$this->db->select('c.*');
 		$this->db->from('tbl_assign_user a');
 		$this->db->join('tbl_lead_meeting c', 'a.lead_id = c.lead_id', 'inner');
 		$this->db->where('a.user_id', $user_id);
 		if($sync_date_time)
 		$this->db->where('c.sync_date_time >', $sync_date_time);
 	    if(!empty($this->session->userdata("user_id")))
		{
			$this->db->where("FIND_IN_SET('".$this->session->userdata("user_id")."', a.user_all_level)");
		}
 		$query = $this->db->get(); 
 		 //echo $this->db->last_query(); die;
 		return $query->result();
 	}
 	/*Radha Edited*/
 	public function getClientLeadByUserId($user_id ,$sync_date_time = NULL)
 	{
 		$this->db->select('c.*');
 		$this->db->from('tbl_leads a');
 		$this->db->join('tbl_assign_user b', 'a.lead_id = b.lead_id', 'inner');
 		$this->db->join('tbl_lead_client c', 'c.lead_id = a.lead_id', 'inner');
 		$this->db->where('b.user_id',  $user_id);
 		if($sync_date_time)
 		$this->db->where('c.sync_date_time >', $sync_date_time);
 	    if(!empty($user_id))
		{
			$this->db->where("FIND_IN_SET('".$user_id."', a.user_all_level)");
		}
 		$query = $this->db->get();
 		return $query->result();
 	}

	public function getLeadMeetingByUserId($user_id ,$sync_date_time = NULL)
 	{
 		$this->db->select('c.*');
 		$this->db->from('tbl_leads a');
 		$this->db->join('tbl_assign_user b', 'a.lead_id = b.lead_id', 'inner');
 		$this->db->join('tbl_lead_meeting c', 'c.lead_id = a.lead_id', 'inner');
 		$this->db->where('b.user_id',  $user_id);
 		if($sync_date_time)
 		$this->db->where('c.sync_date_time >', $sync_date_time);
 	  	if(!empty($user_id))
		{
			$this->db->where("FIND_IN_SET('".$user_id."', a.user_all_level)");
		}
 		$query = $this->db->get();
 		return $query->result();
 	}

	public function getLeadProductByUserId($user_id ,$sync_date_time = NULL)
 	{
 		$this->db->select('c.*');
 		$this->db->from('tbl_leads a');
 		$this->db->join('tbl_assign_user b', 'a.lead_id = b.lead_id', 'inner');
 		$this->db->join('tbl_lead_product c', 'c.lead_id = a.lead_id', 'inner');
 		$this->db->where('b.user_id',  $user_id);
 		if($sync_date_time)
 		$this->db->where('c.sync_date_time >', $sync_date_time);
 	    if(!empty($user_id))
		{
			$this->db->where("FIND_IN_SET('".$user_id."', a.user_all_level)");
		}
 		$query = $this->db->get();
 		return $query->result();
 	}
 	/* expense approval data */

 	public function getdesignation_approval($designation_id,$department_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('cm_designation_approval');
		$this->db->where('designation_id' , $designation_id);	
		$this->db->where('department_id' , $department_id);	
		$this->db->where('type' , 'Approval');	
	    $this->db->where("FIND_IN_SET('".$user_id."', apply_user_id)");
		$query = $this->db->get();
		return $query->result() ;
	}

	public function get_expense_id($exp_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('tbl_expenses');
		$this->db->where('id' , $exp_id);	
		//$this->db->where("FIND_IN_SET('".$this->session->userdata("user_id")."', user_all_level)");
	    $this->db->where("FIND_IN_SET('".$user_id."', user_all_level)");
	    $query = $this->db->get();
		return $query->row() ;
	}
	
	public function checkData($driver_id)
	{
		$this->db->select('*');
		$this->db->from('livelocation');
		$this->db->where('driver_id', $driver_id);
		$query = $this->db->get();
		return $query->result() ;
	}
	
	public function updateData($post)
	{
		$this->db->where('driver_id', $post['driver_id']);
		$this->db->update('livelocation', $post);
		return true;
	}
    
    public function addData($post)
	{
		$this->db->insert('livelocation', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
	}
	
	public function getParcelDropLocationByDriverId($driver_id,$trip_id)
	{
	   
	    $this->db->select('*');
		$this->db->from('trip');
		$this->db->where('driver_id', $driver_id);
	    $this->db->where('trip_id', $trip_id);
	    $query = $this->db->get();
		return $query->result() ;
	}
	
	public function getAllRestaurantList($offeset)
	{
    	    $this->db->select('*');
    		$this->db->from('restaurant');
    		$this->db->where('user_type !=','admin');
    		$this->db->where('admin_approved_status',1);
    		$this->db->limit(10,$offeset);
            $this->db->order_by('restaurant_id','DESC');
    		$query = $this->db->get();
    		return $query->result() ;
	    
	}
	
	public function parcelListByRestaurantId($restaurant_id)
	{
	    
	        $this->db->select('a.*,b.*');
    		$this->db->from('parcel a');
    	    $this->db->join('customer b','a.parcel_id = b.parcel_id');
            $this->db->where('a.status !=','2');
    		$this->db->where('a.restaurant_id',$restaurant_id);
    		$this->db->order_by('a.parcel_id','DESC');
    		$query = $this->db->get();
    		return $query->result() ;
	    
	}
	
	public function getRestaurantsById($restaurant_id)
	{
	        $this->db->select('*');
    		$this->db->from('restaurant');
    		$this->db->where('user_type !=','admin');
    		$this->db->where('restaurant_id',$restaurant_id);
    		$query = $this->db->get();
    		return $query->row() ;  
	}
	public function addParcel($post)
	{
		$this->db->insert('parcel', $post);
		$this->result = $this->db->insert_id() ; 
		
		return $this->result ;
	}
	
	public function addCustomer($post_customer)
	{
		$this->db->insert('customer', $post_customer);
		$this->result = $this->db->insert_id() ; 
		
		return $this->result ;
	}
    public function updateParcelIdByTrip($post_trip,$trip_id)
	{
		$this->db->where('trip_id', $trip_id);
		$this->db->update('trip', $post_trip);
		return true;
	}
	public function getTripById($trip_id)
	{
	    
        $this->db->select('*');
        $this->db->from('trip');
        $this->db->where('trip_id',$trip_id);
        $this->db->order_by('trip_id','DESC');
        $query = $this->db->get();
        return $query->row();
	    
	}
	
	public function getDriverByEmailId($email)
	{
	  
	    $this->db->select('*');
        $this->db->from('driver');
        $this->db->where('email',$email);
        $query = $this->db->get();
        return $query->row();
	    
	}
	
	public function getDriverListByDriverId($driver_id,$password)
	{
	  
	    $this->db->select('*');
        $this->db->from('driver');
        $this->db->where('driver_id',$driver_id);
        $this->db->where('password',$password);
        $query = $this->db->get();
        return $query->row();
	    
	}
	
	public function getRestaurantById($restaurant_id)
	{
	    $this->db->select('*');
        $this->db->from('restaurant');
        $this->db->where('restaurant_id',$restaurant_id);
        $query = $this->db->get();
        return $query->row();    
	}
	
	public function getCountryList()
	{
	    $this->db->select('*');
        $this->db->from('countries');
        $query = $this->db->get();
        return $query->result();    
	}
	
	public function getStateList($country_id)
	{
	    
	    $this->db->select('*');
        $this->db->from('regions');
        $this->db->where('country_id',$country_id);
        $query = $this->db->get();
        return $query->result();     
	    
	}
	
	public function getCityList($region_id)
	{
	    
	    $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('region_id',$region_id);
        $query = $this->db->get();
        return $query->result();     
	    
	}

	public function getAllRestaurantListByresurentName($restaurant_name,$offeset )
	{
    	    $this->db->select('*');
    		$this->db->from('restaurant');
    		$this->db->where('user_type !=','admin');
    		$this->db->where('admin_approved_status',1);
            $this->db->like('restaurant_name', $restaurant_name,'both');
            $this->db->limit(10,$offeset );
            $this->db->order_by('restaurant_id','DESC');
    		$query = $this->db->get();
    		return $query->result() ;
	    
	}

	public function getAllConnectingData($restaurant_id,$connectivity_number)
	{

	    $this->db->select('*');
	    $this->db->from('restaurant');
        $this->db->where('admin_approved_status',1);
        $this->db->where('restaurant_id',$restaurant_id);
        $this->db->where('connectivity_number',$connectivity_number);
        $query = $this->db->get();
        return $query->result();    

	}

	public function updateConectivity($post)
	{

		$this->db->where('restaurant_id', $post['restaurant_id']);
		$this->db->update('restaurant', $post);
		return true;
	}

   public function insertConectivity($post)
   {
      
        $this->db->insert('connectivity_table', $post);
		$this->result = $this->db->insert_id() ; 
	    return $this->result ;
   }

   public function getconnectivity($connectivityrestaurant_id,$connectivitydriver_id,$connectivitydate )
   {
        $this->db->select('*');
	    $this->db->from('connectivity_table');
        $this->db->where('restaurant_id',$connectivityrestaurant_id);
        $this->db->where('connecting_driver_id',$connectivitydriver_id);
        $this->db->where('connectivity_date',$connectivitydate);
        $this->db->order_by('id','DESC');
        $query = $this->db->get();
        return $query->row();  
   }

    public function getAllTripByDriverId($driver_id)
	{
	    
        $this->db->select('a.*,b.restaurant_name');
        $this->db->from('trip a');
        $this->db->join('restaurant b', 'b.restaurant_id=a.restaurant_id', 'inner');
        $this->db->where('a.driver_id',$driver_id);
        $this->db->order_by('a.trip_id','DESC');
        $query = $this->db->get();
        return $query->result();
	    
	}
	public function getAllTripByDriverIdAndtripId($trip_id)
	{
	    
        $this->db->select('a.*,b.restaurant_name');
        $this->db->from('trip a');
        $this->db->join('restaurant b', 'b.restaurant_id=a.restaurant_id', 'inner');
        // $this->db->where('a.driver_id',$driver_id);
        $this->db->where('a.trip_id',$trip_id);
        $this->db->order_by('a.trip_id','DESC');
        $query = $this->db->get();
        return $query->result();
	    
	}

	public function getAllParcelByParcelId($parcel_id)
	{
	    
        $this->db->select('a.*,b.restaurant_name,c.first_name,c.last_name,c.customer_number');
        $this->db->from('parcel a');
        $this->db->join('restaurant b', 'b.restaurant_id=a.restaurant_id', 'inner');
        $this->db->join('customer c', 'c.parcel_id=a.parcel_id', 'inner');
        $this->db->where('a.parcel_id',$parcel_id);
        $this->db->order_by('a.parcel_id','DESC');
        $query = $this->db->get();
        return $query->row();
	    
	}


	public function updateTripStart($post)
	{

		$this->db->where('parcel_id', $post['parcel_id']);
		$this->db->update('parcel', $post);
		return true;
	}

	public function getTripStartEnd($parcel_id)
	{

        $this->db->select('*');
	    $this->db->from('parcel');
        $this->db->where('parcel_id',$parcel_id);
        $this->db->order_by('parcel_id','DESC');
        $query = $this->db->get();
        return $query->row();  

	}




	
	
	
	

 	/* expense approval data */
}
?>
