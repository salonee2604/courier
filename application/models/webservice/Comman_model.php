<?php
class Comman_model extends CI_Model
{
    public $_table_name = '';
    public $_primary_key = 'id';
    public $_primary_filter = 'intval';
    public $_order_by = '';
    public $rules = array();
    public $_timestamps = FALSE;

    function __construct()
    {
        parent::__construct();
    } 
    public function AddData($post)
    {
        $this->db->insert('driver', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
    }
    public function AddTrip($post)
    {
        $this->db->insert('trip', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
    }
    public function getData($tbl_name , $where_array = NULL , $fetch_type = 'multi', $order_by = NULL, $user_id = NULL, $limit = NULL , $offset = NULL , $check_query = NULL , $order_by_clm = 'id')
    {


       
       $table_arr = array('tbl_expenses','tbl_client_quotation','tbl_lead_meeting','tbl_lead_product','tbl_quotation_products','tbl_quotation','tbl_leads');

        $this->db->select('*');
        $this->db->from($tbl_name);
        if($where_array != NULL)
        {
            $this->db->where($where_array);
        }
        if(in_array($tbl_name,$table_arr))
        {
            $this->db->where("FIND_IN_SET('".$user_id."', user_all_level)");
        }
        if($order_by != NULL)
        {
            $this->db->order_by($order_by_clm , $order_by);
        }   
        if($limit != NULL && $offset != NULL)
        {
            $this->db->limit($limit,$offset);
        }
        if($limit != NULL)
        {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
       
        if($check_query != NULL)
        {
            echo $this->db->last_query(); die;
        }
        if($fetch_type == 'single')
        {
            return $query->row();
        }
        else if( $fetch_type == 'multi')
        {
            return $query->result();
        }
        else if( $fetch_type == 'result_array')
        {
            return $query->result_array();
        }
        else if( $fetch_type == 'count')
        {
            return $query->num_rows();
        }
    }
    public function updateData($table_name , $where_array = NULL, $post)
    {
        if($where_array != NULL)
        {
            $this->db->where($where_array);
        }
        $this->db->update($table_name, $post);
        return true;
    }
    
    public function getAllParcelData()
    {
        $this->db->select('*');
        $this->db->from('parcel');
        $this->db->where('status','1');
        $this->db->order_by('parcel_id','desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getAllTripData($driver_id,$restaurant_id)
    {
        $this->db->select('*');
        $this->db->from('trip');
        $this->db->where('status','1');
        $this->db->where('driver_id',$driver_id);
        $this->db->where('restaurant_id',$restaurant_id);
        $this->db->order_by('trip_id','desc');
        $query = $this->db->get();
        return $query->result();
        
    }
    
    public function getTripById($trip_id)
    {
      
        $this->db->select('*');
        $this->db->from('trip');
        $this->db->where('status','1');
        $this->db->where('trip_id',$trip_id);
        $this->db->order_by('trip_id','desc');
        $query = $this->db->get();
        return $query->row();      
      
    }
    public function getAllParcelDataByParcelid($parcel_id)
    {
        $this->db->select('a.*,b.*');
        $this->db->from('parcel a');
        $this->db->join('customer b','a.parcel_id = b.parcel_id');
        $this->db->where('a.status !=','0');
        $this->db->where('a.status !=','2');
        $this->db->where('a.parcel_id',$parcel_id);
        $this->db->order_by('a.parcel_id','desc');
        $query = $this->db->get();
        return $query->row();
    }
    public function updateDriver($post,$driver_id)
	{
	    $this->db->where('driver_id', $driver_id);
		$this->db->update('driver', $post);
		return true;
	}
	public function updateTrip($post,$trip_id)
	{
	    $this->db->where('trip_id', $trip_id);
		$this->db->update('trip', $post);
		return true;
	}
    public function updateParcel($post,$parcel_id)
    {
        $this->db->where('parcel_id', $parcel_id);
        $this->db->update('parcel', $post);
        if ($this->db->affected_rows() > 0)
        return TRUE;
        else
        return FALSE;
        
    }
	public function getAlldriverEmail($email,$driver_id)
    {
        $this->db->select('*');
        $this->db->from('driver');
        $this->db->where('email',$email);
        $this->db->where('driver_id !=',$driver_id);
        $query = $this->db->get();
        return $query->row();
    }
    public function getAlldriverByDriverId($driver_id)
    {
        $this->db->select('*');
        $this->db->from('driver');
        $this->db->where('driver_id',$driver_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getParcelByTripId($p_id)
    {
     

        $this->db->select('*');
        $this->db->from('parcel');
        $this->db->where('status !=',2);
        $this->db->where('parcel_id',$p_id);
        $query = $this->db->get();
        return $query->row();

    }

    public function getTripIdByParcelId($parcel_id)
    {

        $this->db->select('*');
        $this->db->from('trip');
        $this->db->where("FIND_IN_SET('".$parcel_id."', parcel_id)");
        $query = $this->db->get();
        return $query->row();           


    }

    public function tripByTripId($trip_id)
    {
        $this->db->select('*');
        $this->db->from('trip');
        $this->db->where('trip_id',$trip_id);
        $query = $this->db->get();
        return $query->row();  
    }

    

}
?>
