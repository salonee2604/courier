<?php

class Driver_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	public function adddriver($post)
	{
		$this->db->insert('driver', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
	}
	
	public function getClientList()
	{
	    $this->db->select('*');
		$this->db->from('driver');
		$this->db->order_by('driver_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}
	
	function deleteDriver($driver_id)
	{
		$this->db->delete('driver', array('driver_id' => $driver_id));		
		return 1;		
	}
	
	public function getdriverbyid($driver_id)
	{
	    $this->db->select('*');
		$this->db->from('driver');
		$this->db->where('driver_id',$driver_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function updateDriver($post,$driver_id)
	{
	    $this->db->where('driver_id', $driver_id);
		$this->db->update('driver', $post);
		return true;
	}		public function getcountryList()	{	    $this->db->select('*');		$this->db->from('country');		$this->db->order_by('cid','DESC');		$query = $this->db->get();		return $query->result() ;	}


}
?>