<?php

class Trip_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	public function addTrip($post)
	{
		$this->db->insert('trip', $post);
		$this->result = $this->db->insert_id() ; 
		
		return $this->result ;
	}
	
	public function getAllTripData()
	{
	  $this->db->select('*');  
      $this->db->from('trip'); 
	  $this->db->join('restaurant','restaurant.restaurant_id  = trip.restaurant_id');
	  $this->db->order_by('trip_id','DESC');
	  $query = $this->db->get();	
	  return $query->result();
		
		/* $this->db->select('*');
		$this->db->from('trip');
		$this->db->order_by('trip_id','DESC');
		$query = $this->db->get();
		return $query->result() ; */
	}
	
	public function getAllDriverData()
	{
	    $this->db->select('*');
		$this->db->from('driver');
		$this->db->order_by('driver_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}
	public function getDriverDataById($driver_id)
	{
	    $this->db->select('*');
		$this->db->from('driver');
		$this->db->where('driver_id',$driver_id);
		$this->db->order_by('driver_id','DESC');
		$query = $this->db->get();
		return $query->row() ;
	}
	public function getAllParcelData()
	{
	    $this->db->select('*');
		$this->db->from('parcel');
	    $this->db->where('status !=',2);

		$this->db->order_by('parcel_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}
	
	
	function deleteTrip($trip_id)
	{
		$this->db->delete('trip', array('trip_id' => $trip_id));		
		return 1;		
	}
	
	public function getTripById($trip_id)
	{
	    $this->db->select('*');
		$this->db->from('trip');
		$this->db->where('trip_id',$trip_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function updateTrip($post,$trip_id)
	{
	    $this->db->where('trip_id', $trip_id);
		$this->db->update('trip', $post);
		return true;
	}
	
   public function gettrackingtrip($tripid)
   {
	   /* $this->db->select('*');  
      $this->db->from('trip'); 
	  $this->db->join('livelocation','livelocation.trip_id = trip.trip_id');
	  $this->db->where('trip.trip_id',$tripid); 
	  $query = $this->db->get();	
	  return $query->result();	 */
	  
	  $this->db->select('*');  
      $this->db->from('livelocation'); 
	  $this->db->join('trip','trip.trip_id = livelocation.trip_id');
	  $this->db->where('livelocation.trip_id',$tripid); 
	  $query = $this->db->get();	
	  return $query->result();
       
   } 
   
   public function getCustomerByParcelId($parcel_id)
   {
      
      $this->db->select('*');  
      $this->db->from('customer'); 
	  $this->db->where('parcel_id',$parcel_id); 
	  $query = $this->db->get();
	  return $query->row();	
       
       
   }
   	public function getRestaurantList()
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('user_type','restaurant');
		$this->db->where('admin_approved_status',1);
        $this->db->order_by('restaurant_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}

   }
?>