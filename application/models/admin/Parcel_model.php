<?php

class Parcel_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	public function addParcel($post)
	{
	    
		$this->db->insert('parcel', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
	}
	
	public function addCustomer($post_customer)
	{
		$this->db->insert('customer', $post_customer);
		$this->result = $this->db->insert_id() ; 
		
		return $this->result ;
	}
	public function getAllParcelData()
	{
	    $this->db->select('*');
		$this->db->from('parcel');
// 		$this->db->join('restaurant','restaurant.restaurant_id  = parcel.restaurant_id');
		$this->db->order_by('parcel_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}
	
	function deleteParcel($parcel_id)
	{
		$this->db->delete('parcel', array('parcel_id' => $parcel_id));		
		return 1;		
	}
	
	public function getParcelid($parcel_id)
	{
	    $this->db->select('*');
		$this->db->from('parcel');
		$this->db->where('parcel_id',$parcel_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function updateParcel($post,$parcel_id)
	{
	    
	    $this->db->where('parcel_id', $parcel_id);
		$this->db->update('parcel', $post);
		return true;
	}
	
	public function updateCustomer($post_customer,$parcel_id)
	{
	    
	    $this->db->where('parcel_id', $parcel_id);
		$this->db->update('customer', $post_customer);
	    return true;
	}
	
	public function getLiveLocation()
	{
	    $this->db->select('*');
		$this->db->from('livelocation');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function gettrackingtrip()
   {	  
	  $this->db->select('*');  
      $this->db->from('livelocation');
	   $this->db->join('driver','driver.driver_id  = livelocation.driver_id');
	  $query = $this->db->get();	
	  return $query->result();
       
   } 
	
	public function getAllDriverListByDriverID($driver_id)
	{
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->where('driver_id', $driver_id);
	    $query = $this->db->get();
        return $query->row() ;
		
	}
    
    public function getTripByDriverId($driver_id)
    {
        
        $this->db->select('*');
		$this->db->from('trip');
		$this->db->where('driver_id', $driver_id);
		$this->db->order_by('trip_id','desc');
	    $query = $this->db->get();
        return $query->row() ;
		
        
    }
    
    public function getParcelByParcelId($parcel_id)
    {
       
        $this->db->select('*');
		$this->db->from('parcel');
		$this->db->where('parcel_id', $parcel_id);
		$this->db->where('status',4);
		$this->db->order_by('parcel_id','desc');
	    $query = $this->db->get();
        return $query->row() ;
        
    }
    
    public function getCustomerById($parcel_id)
    {
     
         $this->db->select('*');
		$this->db->from('customer');
		$this->db->where('parcel_id', $parcel_id);
		$this->db->where('status',1);
		$this->db->order_by('customer_id','desc');
	    $query = $this->db->get();
        return $query->row() ;
        
    }

	
	public function getcountry()
	{	
	$this->db->select('*');
	$this->db->from('countries');
	$this->db->where('country_id',212);
	$query = $this->db->get(); 
	return $query->result();
	}	

	public function getcitys()	
	{		
	$this->db->select('*');	
	$this->db->from('cities');	
	$query = $this->db->get(); 
	return $query->result();
	}
	
	public function getstates()
	{	
	$this->db->select('*');	
	$this->db->from('regions');	
	$query = $this->db->get(); 
	return $query->result();
	}	
	
	public function getstate($id)
	{	
	$this->db->select('*');	
	$this->db->from('regions');
	$this->db->where('country_id',$id);
	$query = $this->db->get(); 
	return $query->result();
	}
	
	public function getcity($id)
	{	
	$this->db->select('*');	
	$this->db->from('cities');
	$this->db->where('region_id',$id);
	$query = $this->db->get();
	return $query->result();
	}
	
	public function getResturentById($restaurant_id)
	{
	  
	  
        	$this->db->select('*');	
        	$this->db->from('restaurant');
        	$this->db->where('restaurant_id',$restaurant_id);
        	$query = $this->db->get(); 
        	return $query->row();  
	    
	}
	public function getRestaurantList()
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('user_type','restaurant');
		$this->db->where('admin_approved_status',1);
		$this->db->order_by('restaurant_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}

	public function getDriverByParcelId($parcel_id)
	{
        $this->db->select('*');
		$this->db->from('trip');
		$this->db->where("FIND_IN_SET('".$parcel_id."', parcel_id)");
		$query = $this->db->get();
		return $query->row() ;
	}
	
	

}
?>