<?php

class Restaturant_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	public function addRestaurant($post)
	{
	    
	    
		$this->db->insert('restaurant', $post);
		$this->result = $this->db->insert_id() ; 
		return $this->result ;
	}
	
	public function getRestaurantList()
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('user_type','restaurant');
		$this->db->order_by('restaurant_id','DESC');
		$query = $this->db->get();
		return $query->result() ;
	}
	
	function delete_restaurant($restaurant_id)
	{
		$this->db->delete('restaurant', array('restaurant_id' => $restaurant_id));		
		return 1;		
	}
	
	public function getRestaturantbyid($restaurant_id)
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('restaurant_id',$restaurant_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function updateRestaurant($post,$resturent_id_post)
	{
	    $this->db->where('restaurant_id', $resturent_id_post);
		$this->db->update('restaurant', $post);
		
		return true;
	}
	
	public function getPaymentDetails()
	{
	    
	    $this->db->select('a.*,b.*');    
        $this->db->from('restaurant a');
        $this->db->join('payments b', 'a.restaurant_id = b.user_id');
        $this->db->group_by('b.user_id');
        $query = $this->db->get();
        return $query->result();

	}
	
	public function getTransectionDetails($user_id)
	{
	    
	    $this->db->select('a.*,b.*');    
        $this->db->from('restaurant a');
        $this->db->join('payments b', 'a.restaurant_id = b.user_id');
        $this->db->where('b.user_id',$user_id);
        $query = $this->db->get();
        return $query->result();

	}


}
?>