<?php

class Customer_model extends CI_Model 
{

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	
    
    public function getCustomerList()
    {
     
        $this->db->select('*');
		$this->db->from('customer');
		$this->db->where('status',1);
	    $this->db->group_by('customer_number');
        $this->db->order_by('customer_id','desc');
	    $query = $this->db->get();
        return $query->result() ;
        
    }
    
    public function getCustomerListByCustomerNumber($mobilenumber)
    {    
        $this->db->select('*');
		$this->db->from('customer');
		$this->db->where('status',1);
	    $this->db->where('customer_number',$mobilenumber);
        $this->db->order_by('customer_id','desc');
	    $query = $this->db->get();
        return $query->result() ;
    } 		
	
	public function getcontactList()  
	{   
	$this->db->select('*');	
	$this->db->from('contactdata');	
	$this->db->order_by('cd_id','desc');	
    $query = $this->db->get(); 
	return $query->result() ;   
	}		
    
	public function getreferenzenList()
    {
     
        $this->db->select('*');
		$this->db->from('referenzenpage');
		$this->db->order_by('rp_id','desc');
	    $query = $this->db->get();
        return $query->result() ;
        
    }
    
	  public function insertCommon($table,$data){
       // echo 'hello'; die;
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
	
	public function deleteByNoImageId($table , $pid, $id){
        $this->db->where($pid, $id);
        $this->db->delete($table); 
        return true;
    }
	
	public function getrestaurantlogo()
    {
     
        $this->db->select('*');
		$this->db->from('restaurantlogo');
		$this->db->order_by('rs_id','desc');
	    $query = $this->db->get();
        return $query->result() ;
        
    }
    
    public function getCustomerById($parcel_id)
    {
      
        $this->db->select('*');
		$this->db->from('customer');
	    $this->db->where('parcel_id',$parcel_id);
		$this->db->order_by('parcel_id','desc');
	    $query = $this->db->get();
        return $query->result() ;
        
    }
    
    public function getParcelById($parcel_id)
    {
        $this->db->select('*');
		$this->db->from('parcel');
	    $this->db->where('parcel_id',$parcel_id);
		$this->db->order_by('parcel_id','desc');
	    $query = $this->db->get();
        return $query->row() ;
    }

    public function getRestaurantListById($resturent_id)
	{
	    $this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('user_type','restaurant');
		$this->db->where('admin_approved_status',1);
		$this->db->where('restaurant_id',$resturent_id);
        $this->db->order_by('restaurant_id','DESC');
		$query = $this->db->get();
		return $query->row() ;
	}
    

}
?>