<?php

class Login_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   
	}

	/*	Get all Country List  */
	public function checkUserLogin($email,$password)
	{
		$this->db->select('*');
		$this->db->from('restaurant');
		$this->db->where('email', $email);
	    $this->db->where('password', $password);
	    $this->db->where('user_type','admin');
		$this->db->where('status', '1');
		$query = $this->db->get();
        return $query->result() ;
	}
	
	public function getDriverCount()
	{
	    
	    $this->db->select('*');
		$this->db->from('driver');
		$this->db->where('status', '1');
		$query = $this->db->get();
        return $query->num_rows() ;
	    
	}
	
	public function getParcelCount()
	{
	    
	    $this->db->select('*');
		$this->db->from('parcel');
		$query = $this->db->get();
        return $query->num_rows() ;
	    
	}
	
	public function getTripCount()
	{
	    
	    $this->db->select('*');
		$this->db->from('trip');
		$query = $this->db->get();
        return $query->num_rows() ;
	    
	}
   
   public function getTopDriver()
   {    
        $this->db->select("count(driver_id) as count");
        $this->db->from('trip');
        $this->db->group_by('driver_id'); 
		$query = $this->db->get();
        return $query->row() ;
    }
    
    



}
?>