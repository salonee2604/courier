<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller  

{

    

    

	function __construct()

	{

		parent::__construct();

        $this->load->model('front/Login_model');

        $this->load->model('front/Restaturant_model');

        $this->load->library('form_validation');

        $this->load->helper('string');


    }

	protected $validation_rules = array

        (

        'restaturantAdd' => array(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),

            

             array(

                'field' => 'restaurant_name',

                'label' => 'Restaurant Name',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required|is_unique[restaurant.email]'

            ),

			array(

                'field' => 'mobile_number',

                'label' => 'Mobile Number',

                'rules' => 'trim|required'

            ),

             array( 

				'field' => 'password', 

				'label' => 'Password',   

				'rules' => 'trim|required'  

			),

			array(  

				'field' => 'c_password',

				'label' => 'Confirm Password', 

				'rules' => 'trim|required|matches[password]'

            ),			

            array(

                'field' => 'surname',

                'label' => 'surname',

                'rules' => 'trim|required'

            ),



            array(

                'field' => 'address',

                'label' => 'Strasse',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'city',

                'label' => 'City',

                'rules' => 'trim|required'

            ),

            

			array(

                'field' => 'name_holder',

                'label' => 'Name auf der Karte',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'card_no',

                'label' => 'Kartennummer',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'security_code',

                'label' => 'security Code',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'expire_date',

                'label' => 'g���ltig bis',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'country',

                'label' => 'Land',

                'rules' => 'trim|required'

            ),

			

			array(

                'field' => 'zipcode',

                'label' => 'Postleitzahl',

                'rules' => 'trim|required'

            )

                     

                     

                  

        ),

		'restaturantEdit' => array

		(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),

             array(

                'field' => 'surname',

                'label' => 'Surname',

                'rules' => 'trim|required'

            ),

             array(

                'field' => 'restaurant_name',

                'label' => 'Restaurant Name',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'mobile_number',

                'label' => 'Mobile Number',

                'rules' => 'trim|required'

            ),

             array( 

				'field' => 'password', 

				'label' => 'Password',   

				'rules' => 'trim|required'  

			),

			array(  

				'field' => 'c_password',

				'label' => 'Confirm Password', 

				'rules' => 'trim|required|matches[password]'

            ),			

            array(

                'field' => 'surname',

                'label' => 'surname',

                'rules' => 'trim|required'

            ),



            array(

                'field' => 'address',

                'label' => 'Address',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'city',

                'label' => 'City',

                'rules' => 'trim|required'

            )

            

        ),

        'login' => array(

            array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required'

            ),

             array(

                'field' => 'password',

                'label' => 'Password',

                'rules' => 'trim|required'

            )

        )



    );

	

	

		

	/* Login */

	public function index()

	{

	   

	   $this->load->view('front/home');

		

    }

    public function about()

	{

	   

	   $this->load->view('front/about');

		

    }

    	 public function disclaimer()	{	   	   $this->load->view('front/disclaimer');		    }

    public function team()

	{

	   

	   $this->load->view('front/team');

		

    }

    

    public function contact()

	{

	   

	   $this->load->view('front/contact');

		

    }

    

    public function privacy()

	{

	   

	   $this->load->view('front/privacy');

		

    }

    

    public function terms()

    {

       $this->load->view('front/terms');

  

    }

    

    

    

    

    public function restaurantLogin()

    {

        if(isset($_POST['submit']))

			{	

				$this->form_validation->set_rules($this->validation_rules['login']);

				if ($this->form_validation->run()) 

				{

				    $email    = $_POST['email'];

					$password = md5($_POST['password']);

					$user_details = $this->Login_model->checkUserLogin($email,$password);

				

					if(!empty($user_details))

					{

                         $uid = $user_details[0]->restaurant_id;

                         $termination_staus = $user_details[0]->termination_staus;

                         $termination_date = $user_details[0]->termination_date;

                         $datecre = $user_details[0]->created_date;

						 $date = date('Y-m-d',strtotime($datecre));

						 $datecc = date('Y-m-d');						

					     $dated = date('Y-m-d', strtotime("+14 day", strtotime($date)));

						 $getpaymentdetail = $this->Login_model->getpayment($uid);

						 

						 /* print_r($user_details);

                         $uid = $user_details[0]->restaurant_id;

						 $datecre = $user_details[0]->created_date;

						 $date = date('Y-m-d',strtotime($datecre));

						 $datecc = date('Y-m-d');						

					     $dated = date('Y-m-d', strtotime("+14 day", strtotime($date)));

						

						$getpaymentdetail = $this->Login_model->getpayment($uid);

						if(!empty($getpaymentdetail)){

							//echo 'yes';

						$this->session->set_userdata('web_admin' , $user_details);

						redirect(base_url().'restaurant/dashboard');

						}else {

							echo 'no';

							//$this->load->view('restaurant/dashbordnoplan');

						}

						 */

						if($user_details[0]->admin_approved_status == 1)

						{

						if($dated >= $datecc  or $dated >= $datecc && $termination_staus == 1 && $termination_date >=  $datecc){

							

							$this->session->set_userdata('web_admin' , $user_details);

						    redirect(base_url().'restaurant/dashboard');

						}

						else if($termination_staus == 1 && $termination_date <=  $datecc)

							{

							    

							   $this->session->set_userdata('web_admin' , $user_details);

    							redirect(base_url().'restaurant/dashboardtermination'); 

							}

						else 

						{

							if(!empty($getpaymentdetail))

							{

							$this->session->set_userdata('web_admin' , $user_details);

						    redirect(base_url().'restaurant/dashboard');

							}

							

							else 

							{

    							$this->session->set_userdata('web_admin' , $user_details);

    							redirect(base_url().'restaurant/dashbordnoplan');

							}

							$this->session->set_userdata('web_admin' , $user_details);

							redirect(base_url().'restaurant/dashbordnoplan');

						}

						}

						else

						{

						 

						   $msg = 'Admin Not Approved';

						  $this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');

						redirect(base_url().'login/restaurantLogin');   

						}

						

						

						/* $this->session->set_userdata('web_admin' , $user_details);

						redirect(base_url().'restaurant/dashboard'); */

					}

					else

					{

						$msg = 'Invalid Email And Password';

						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');

						redirect(base_url().'login/restaurantLogin');

					}

				}

				else

				{					

                     $this->load->view('front/header');

                     $this->load->view('front/login');

					 $this->load->view('front/footer');

				}

			}

			else

			{

				// To Load second database and perform query operation.

                     $this->load->view('front/header');

                     $this->load->view('front/login');

					 $this->load->view('front/footer');

			}

    }



    public function restaurantSignup($driver_id='')

	{

	    if($driver_id)

		{

			

			        

				if (isset($_POST['submit']) && $_POST['submit'] == "Edit") 

				{

				    $this->form_validation->set_rules($this->validation_rules['restaturantEdit']);

					if($this->form_validation->run())

					{

					    

                        $post['first_name'] = $this->input->post('first_name');

                        $post['restaurant_name'] = $this->input->post('restaurant_name');

                         $post['bank_details']    = $this->input->post('bank_details');

                        $post['address']    = $this->input->post('address');

                        $post['user_type'] = 'restaurant';

                        $post['surname'] = $this->input->post('surname');

						$post['mobile_number'] = $this->input->post('mobile_number');

						$post['password'] = md5($this->input->post('password'));

						$post['email'] = $this->input->post('email');

						$post['city'] = $this->input->post('city');

						// $post['post_code'] = $this->input->post('post_code');

						$post['status'] = $this->input->post('status');

						$post['create_date'] = date('Y-m-d H:i:s');

						$post['update_date'] = date('Y-m-d H:i:s');

						if ($_FILES["image"]["name"])



                        {



                                $image = 'image';



                                $fieldName = "image";



                                $Path = 'image/restorent/';



                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);



                              $post['restaurant_image'] = base_url().$Path.''.$image;



                                



                        }

                        $driver_id =  $this->Restaturant_model->updateDriver($post,$driver_id);

                        if($driver_id)

						{					

							$msg = 'Restaturant Updated successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurant/restaurant');

						}

                        

					}

					else

					{	

					    

                            $driver_id        = $this->uri->segment(3);

                            $this->data['edit_driver'] = $this->Driver_model->getdriverbyid($driver_id);

                            $driver_details  = $this->Driver_model->getdriverbyid($driver_id);

                            $qr_image=rand().'.png';

                            $params['data']   = $driver_id;

                            $params['level']  = 'H';

                            $params['size']   = 8;

                            $params['savename'] =FCPATH."image/".$qr_image;

                            if($this->ciqrcode->generate($params))

                            {

                            $this->data['img_url']=$qr_image;	

                            }

						$this->data['edit_driver'] = $this->Restaturant_model->getdriverbyid($driver_id);

						$this->show_view_admin('restaurant/edit_driver', $this->data);

					}		

				}

				else

				{

				    

                    $driver_id        = $this->uri->segment(3);

                    $this->data['edit_driver'] = $this->Restaturant_model->getdriverbyid($driver_id);

                    $driver_details  = $this->Restaturant_model->getdriverbyid($driver_id);

                    $qr_image=rand().'.png';

                    $params['data']   = $driver_id;

                    $params['level']  = 'H';

                    $params['size']   = 8;

                    $params['savename'] =FCPATH."image/".$qr_image;

                    if($this->ciqrcode->generate($params))

                    {

                    $this->data['img_url']=$qr_image;	

                    }

				    $this->data['edit_driver'] = $this->Restaturant_model->getdriverbyid($driver_id);

					$this->show_view_admin('restaurant/edit_driver',$this->data);

				}

			

			

		}

		else

		{		





           

		    if (isset($_POST['submit'])) 

		    {

		        

		        



				    $this->form_validation->set_rules($this->validation_rules['restaturantAdd']);

					if($this->form_validation->run())

					{

                        $post['first_name'] = $this->input->post('first_name');

					    $post['restaurant_name'] = $this->input->post('restaurant_name');

					    $post['restaurant_name'] = $this->input->post('restaurant_name');

                        $post['bank_details']    = $this->input->post('bank_details');

						$post['name_holder']    = $this->input->post('name_holder');

						$post['card_no']    = $this->input->post('card_no');

						$post['expire_date']    = $this->input->post('expire_date');

						$post['security_code']    = $this->input->post('security_code');

                        $post['country']    = $this->input->post('country');

					    $post['zipcode']    = $this->input->post('zipcode');

						$post['surname'] = $this->input->post('surname');

				        $post['address']    = $this->input->post('address');

                        $post['user_type'] = 'restaurant';

						$post['mobile_number'] = $this->input->post('mobile_number');

						$post['password'] = md5($this->input->post('password'));

						$post['email'] = $this->input->post('email');

						$post['city'] = $this->input->post('city');

						// $post['post_code'] = $this->input->post('post_code');

						$post['status'] = $this->input->post('status');

						$post['latitude'] = $this->input->post('pickup_latitude');

						$post['longitude'] = $this->input->post('pickup_longitude');
                        $connecnumber = random_int(100000, 999999);
                        $post['connectivity_number'] = $connecnumber;
						$post['created_date'] = date('Y-m-d H:i:s');

						$post['updated_date'] = date('Y-m-d H:i:s');

						if ($_FILES["image"]["name"])



                        {



                                $image = 'image';



                                $fieldName = "image";



                                $Path = 'image/restorent/';



                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);

                                 $post['restaurant_image'] = base_url().$Path.''.$image;



                                



                        }

                      

                        $driver_id =  $this->Restaturant_model->addRestaurant($post);	

						if($driver_id)

						{					

							$msg = 'Restaurant added successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurantSignup');

						}

					}

					else

					{				



                     $this->load->view('front/header');

					 $this->load->view('front/signup');

					 $this->load->view('front/footer');

					}		

				}

		    else

		    {

                     $this->load->view('front/header');

					 $this->load->view('front/signup');

					 $this->load->view('front/footer');

	       }

	   }

			

		

		

    }





    

    



}

/* End of file */