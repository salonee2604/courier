<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trip extends MY_Controller  

{

    

    

	public function __construct()

	{

		parent::__construct();
        $this->load->model('restaurant/Trip_model');
        $this->load->library('form_validation'); 
        $this->load->library('Ciqrcode');


	}

	

	/*	Validation Rules */

	 protected $validation_rules = array

        (

        'tripAdd' => array(

           array(

                'field' => 'name',

                'label' => 'Name',

                'rules' => 'trim|required'

            ),

			
            

            array(

                'field' => 'parcel_id[]',

                'label' => 'Parcel',

                'rules' => 'trim|required'

            )

			

			

             			

        ),

		'tripEdit' => array

		(

           array(

                'field' => 'name',

                'label' => 'Name',

                'rules' => 'trim|required'

            ),

			
            

            array(

                'field' => 'parcel_id[]',

                'label' => 'Parcel',

                'rules' => 'trim|required'

            )

		

        )

    );

		

	/* Login */

	public function index()
    {
       $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;
       $this->data['trip_details'] =  $this->Trip_model->getAllTripData($restaurant_id);
       $this->show_view_restaurant('restaurant/trip_list',$this->data);
    }

	public function addTrip($trip_id='')
    {
         $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;
         $trip_id_qr = $this->uri->segment(4);
	     if($trip_id)
         {
        if (isset($_POST['edittripsubmit'])) 

				{
				    
				    
				    $this->form_validation->set_rules($this->validation_rules['tripEdit']);
                    if($this->form_validation->run())
                      {

				        $post['name']               = $this->input->post('name');

						$post['brand']              = $this->input->post('brand');

						$post['driver_id']          = $this->input->post('driver_id');

						$parcel_id_array            = $this->input->post('parcel_id');

						 $post['parcel_id']         = implode(",",$parcel_id_array);

                         $post['trip_assign_status'] = 1;
                         $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;
                        $post['restaurant_id']      = $restaurant_id;
                        $post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 

                        if ($_FILES["image"]["name"])

                        {

                                $image = 'image';

                                $fieldName = "image";

                                $Path = 'image/';

                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);

                              $post['image'] = base_url().$Path.''.$image;

                                

                        }
						$trip_id_main                 =  $this->Trip_model->updateTrip($post,$trip_id);

                        if($trip_id_main)

						{					

							$msg = 'Trip Updated successfully!!';		

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurant/trip');

						}

                        

					}

					else

					{				
                        
                        
                        $qr_image=rand().'.png';
                        $params['data'] = $trip_id_qr;
                        $params['level'] = 'H';
                        $params['size'] = 8;
                        $params['savename'] =FCPATH."image/".$qr_image;
                        if($this->ciqrcode->generate($params))
                        {
                        	$this->data['img_url']=$qr_image;	
                        }
                        
						$this->data['edit_trip'] = $this->Trip_model->getTripById($trip_id,$restaurant_id);

						$this->data['driver_list']  = $this->Trip_model->getAllDriverData($restaurant_id);

            		      $this->data['parcel_details']  = $this->Trip_model->getAllParcelData($restaurant_id);

						$this->show_view_restaurant('restaurant/edit_trip', $this->data);

					}		

				}

				else

				{

				    
                        $qr_image=rand().'.png';
                        $params['data'] = $trip_id_qr;
                        $params['level'] = 'H';
                        $params['size'] = 8;
                        $params['savename'] =FCPATH."image/".$qr_image;
                        if($this->ciqrcode->generate($params))
                        {
                        	$this->data['img_url']=$qr_image;	
                        }
                    $this->data['driver_list']  = $this->Trip_model->getAllDriverData($restaurant_id);
                    $this->data['parcel_details']  = $this->Trip_model->getAllParcelData($restaurant_id);

				    $this->data['edit_trip'] = $this->Trip_model->getTripById($trip_id,$restaurant_id);

					$this->show_view_restaurant('restaurant/edit_trip',$this->data);

				}

			

			

		}

		else

		{		

		    if (isset($_POST['addtripsubmit'])) 

		    {

		        
		    	$this->form_validation->set_rules($this->validation_rules['tripAdd']);

					if($this->form_validation->run())

					{

					    $post['name']               = $this->input->post('name');

						$post['brand']              = $this->input->post('brand');

						$post['driver_id']          = $this->input->post('driver_id');

						$parcel_id_array            = $this->input->post('parcel_id');

						 $post['parcel_id']         = implode(",",$parcel_id_array);

                         $post['trip_assign_status'] = 1;
                          $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;
                         $post['restaurant_id']      = $restaurant_id;
                      	 $post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 

                        if ($_FILES["image"]["name"])

                        {

                                $image = 'image';

                                $fieldName = "image";

                                $Path = 'image/';

                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);

                              $post['image'] = base_url().$Path.''.$image;

                                

                        }

						$parcel_id =  $this->Trip_model->addTrip($post);

					

						if($parcel_id)

						{					

							$msg = 'Trip added successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurant/trip');

						}

					}

					else

					{				


                        
                          $this->data['driver_list']  = $this->Trip_model->getAllDriverData($restaurant_id);

            		      $this->data['parcel_details']  = $this->Trip_model->getAllParcelData($restaurant_id);

                          $this->show_view_restaurant('restaurant/addtrip',$this->data);					
					    
					}		

				}

		    else

		    {

		      $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;
              $this->data['driver_list']  = $this->Trip_model->getAllDriverData($restaurant_id);
              $this->data['parcel_details']  = $this->Trip_model->getAllParcelData($restaurant_id);
              $this->show_view_restaurant('restaurant/addtrip',$this->data);

	       }

	   }

			

		

		

    }

    public function dashboard()

    {

      $this->show_view_restaurant('restaurant/dashbord');

        

    }

   /* trackingtrip */
	public function trackingtrip($trip_id='')
	{
		$tripid = $this->uri->segment(4); 
        $this->data['trackingtrip'] =  $this->Trip_model->gettrackingtrip($tripid);
      
		$this->show_view_restaurant('restaurant/trackingtrip',$this->data);
	}
	/* trackingtrip */
    

    public function logout() 

	{        

        $this->session->sess_destroy();		

        redirect(base_url());

    }

    

    public function delete_trip()

	{

			$trip_id = $this->uri->segment(4);	
            $this->Trip_model->deleteTrip($trip_id);
            $msg = 'Trip remove successfully...!';					
            $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
            redirect(base_url().'restaurant/trip');

   }
   
   public function tripfullView()
   {
             $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;

            $trip_id_qr = $this->uri->segment(4);  
            $qr_image=rand().'.png';
            $params['data'] = $trip_id_qr;
            $params['level'] = 'H';
            $params['size'] = 8;
            $params['savename'] =FCPATH."image/".$qr_image;
            if($this->ciqrcode->generate($params))
            {
            $this->data['img_url']=$qr_image;	
            }
            
            $this->data['edit_trip'] = $this->Trip_model->getTripById($trip_id_qr,$restaurant_id);
            
            $this->data['driver_list']  = $this->Trip_model->getAllDriverData($restaurant_id);
            
            $this->data['parcel_details']  = $this->Trip_model->getAllParcelData($restaurant_id);
            
            $this->data['driver_list']  = $this->Trip_model->getAllDriverData($restaurant_id);
            $this->data['parcel_details']  = $this->Trip_model->getAllParcelData($restaurant_id);
            $this->show_view_restaurant('restaurant/trip_view',$this->data);

       
   }



}

    

    

    





/* End of file */