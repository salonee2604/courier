<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MY_Controller  
{
    
    
	function __construct()
	{
		parent::__construct();
		$this->load->model('restaurant/Login_model');
        $this->load->helper('string');
	
	}
	
	/*	Validation Rules */
	 protected $validation_rules = array
        (
        'login' => array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ),
			 array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            )
        ),

		'adminEdit' => array(
            array(
                'field' => 'admin_name',
                'label' => ' admin name',
                'rules' => 'trim|required'
            ),      
      		array(
                'field' => 'admin_phone',
                'label' => 'admin phone',
                'rules' => 'trim|required'
            ),
        )

       
        

		
    );
		
	/* Login */
	public function index()
	{
		
		    
			if(isset($_POST['submit']) && $_POST['submit'] =='Login')
			{	
				$this->form_validation->set_rules($this->validation_rules['login']);
				if ($this->form_validation->run()) 
				{
				    $email    = $_POST['email'];
					$password = md5($_POST['password']);
					$user_details = $this->Login_model->checkUserLogin($email,$password);
					if(!empty($user_details))
					{

						$this->session->set_userdata('web_admin' , $user_details);
						redirect(base_url().'admin/dashboard');
					}
					else
					{
						$msg = 'Invalid Email And Password';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'admin/login');
					}
				}
				else
				{					
					$this->load->view('admin/login', $this->data);
				}
			}
			else
			{
				// To Load second database and perform query operation.
				$this->load->view('admin/login');
			}
		
    }
    public function dashboard()
    {
      
      
         $restaurant_id                 =  $_SESSION[web_admin][0]->restaurant_id;
         $restaurant_details            = $this->db->get_where(' restaurant',array('restaurant_id'=> $restaurant_id))->row();
         $this->data['restaurant_details'] = $restaurant_details;
         $this->data['driver_count']    =  $this->Login_model->getDriverCount($restaurant_id);
         $this->data['parcel_count']    =  $this->Login_model->getParcelCount($restaurant_id);
         $this->data['trip_count']      =  $this->Login_model->getTripCount($restaurant_id);
         $this->data['get_top_driver']  =  $this->Login_model->getTopDriver($restaurant_id);
	     $this->data['smscoundata']  = $this->db->get_where('smsmonthdata',array('restr_id'=> $restaurant_id))->result_array();
         $this->show_view_restaurant('restaurant/dashbord',$this->data);
        
    }		
    public function dashboardno()    
    {      
        $restaurant_id                 =                 $_SESSION[web_admin][0]->restaurant_id;         $this->data['driver_count']    =  $this->Login_model->getDriverCount($restaurant_id);         $this->data['parcel_count']    =  $this->Login_model->getParcelCount($restaurant_id);         $this->data['trip_count']      =  $this->Login_model->getTripCount($restaurant_id);      $this->data['get_top_driver']  =  $this->Login_model->getTopDriver($restaurant_id);      $this->show_view_restaurant('restaurant/dashbordnoplan',$this->data);           
   }
   public function dashboardtermination()    
    {      
        
        
            $restaurant_id                 =                 $_SESSION[web_admin][0]->restaurant_id;         $this->data['driver_count']    =  $this->Login_model->getDriverCount($restaurant_id);         $this->data['parcel_count']    =  $this->Login_model->getParcelCount($restaurant_id);         $this->data['trip_count']      =  $this->Login_model->getTripCount($restaurant_id);      $this->data['get_top_driver']  =  $this->Login_model->getTopDriver($restaurant_id);      $this->show_view_restaurant('restaurant/dashbordtermination',$this->data);           
   }
    
    public function logout() 
	{        
        $this->session->sess_destroy();		
        redirect(base_url());
    }
    
    public function genereteRandomNumber()
    {
            $resturent_id    = $_POST['resturent_id'];
            $generete_number = random_int(100000, 999999);

            // $generete_number = random_string('alnum',6);
            $data = ['connectivity_number' => $generete_number,'connectivity_status'=>0,'connectivity_date'=>date('Y-m-d H:i:s'),'connecting_driver_id'=>'']; 
        	$this->db->where('restaurant_id',$resturent_id);  
        	$this->db->update('restaurant', $data); 
        	redirect(base_url());
    }
    
    
    

}
/* End of file */