<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restaurant extends MY_Controller  

{

    

    

	function __construct()

	{

		parent::__construct();

		$this->load->model('restaurant/Restaturant_model');

		$this->load->library('form_validation');

		$this->load->library('Ciqrcode');





	}

	

	/*	Validation Rules */

	 protected $validation_rules = array

        (

        'restaturantAdd' => array(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),

            

             array(

                'field' => 'restaurant_name',

                'label' => 'Restaurant Name',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required|is_unique[restaurant.email]'

            ),

			array(

                'field' => 'mobile_number',

                'label' => 'Mobile Number',

                'rules' => 'trim|required'

            ),

             array( 

				'field' => 'password', 

				'label' => 'Password',   

				'rules' => 'trim|required'  

			),

			array(  

				'field' => 'c_password',

				'label' => 'Confirm Password', 

				'rules' => 'trim|required|matches[password]'

            ),			

            array(

                'field' => 'surname',

                'label' => 'surname',

                'rules' => 'trim|required'

            ),



            array(

                'field' => 'address',

                'label' => 'Address',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'city',

                'label' => 'City',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'post_code',

                'label' => 'post code',

                'rules' => 'trim|required'

            )

                     

                  

        ),

		'restaturantEdit' => array

		(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),

             array(

                'field' => 'surname',

                'label' => 'Surname',

                'rules' => 'trim|required'

            ),

             array(

                'field' => 'restaurant_name',

                'label' => 'Restaurant Name',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'mobile_number',

                'label' => 'Mobile Number',

                'rules' => 'trim|required'

            ),

//              array( 

// 				'field' => 'password', 

// 				'label' => 'Password',   

// 				'rules' => 'trim|required'  

// 			),

// 			array(  

// 				'field' => 'c_password',

// 				'label' => 'Confirm Password', 

// 				'rules' => 'trim|required|matches[password]'

//             ),			

            array(

                'field' => 'surname',

                'label' => 'surname',

                'rules' => 'trim|required'

            ),



            array(

                'field' => 'address',

                'label' => 'Address',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'city',

                'label' => 'City',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'post_code',

                'label' => 'post code',

                'rules' => 'trim|required'

            )    

        )

    );

		

	/* Login */

	public function index()

	{



         

	     $this->data['restaturant_details'] = $this->Restaturant_model->getRestaurantList();

	     $this->show_view_admin('restaurant/restaurant_list',$this->data);

  

	}

	public function addRestaurant($restaurant_id='')

	{

	   if($restaurant_id)

		{

			

			        

				if (isset($_POST['editsubmit'])) 

				{

				    $this->form_validation->set_rules($this->validation_rules['restaturantEdit']);

					if($this->form_validation->run())

					{

					    

                        $post['first_name'] = $this->input->post('first_name');

					    $post['restaurant_name'] = $this->input->post('restaurant_name');

					    $post['restaurant_name'] = $this->input->post('restaurant_name');

                        $post['bank_details']    = $this->input->post('bank_details');

						$post['surname'] = $this->input->post('surname');

				        $post['address']    = $this->input->post('address');

				        $latitude                   = $this->input->post('latitude');

				        if(!empty($latitude))

				        {

				           $post['latitude']    = $latitude;

   

				        }                   

				        $longitude                      = $this->input->post('longitude');

				        if(!empty($longitude))

				        {

				          

				          $post['longitude']    = $longitude;

  

				        }

                        $post['user_type'] = 'restaurant';

						$post['mobile_number'] = $this->input->post('mobile_number');

						$password    = $this->input->post('password');

						if(!empty($password))

						{

						   

						    $post['password'] = md5($password);

   

						}

						$post['email'] = $this->input->post('email');

						$post['city'] = $this->input->post('city');

						$post['zipcode'] = $this->input->post('post_code');

						$post['status'] = 1;

						$post['name_holder'] = $this->input->post('name_holder');

						$post['card_no'] = $this->input->post('card_no');

                        $post['security_code'] = $this->input->post('security_code');

						$post['expire_date'] = $this->input->post('expire_date');

						$post['created_date'] = date('Y-m-d H:i:s');

						$post['updated_date'] = date('Y-m-d H:i:s');

						if ($_FILES["image"]["name"])



                        {



                                $image = 'image';



                                $fieldName = "image";



                                $Path = 'image/restorent/';



                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);

                                 $post['restaurant_image'] = base_url().$Path.''.$image;



                                



                        }

                        $driver_id =  $this->Restaturant_model->updateRestaurant($post,$restaurant_id);

                        if($driver_id)

						{					

							$msg = 'Profile Updated successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							$resturent_id =  $_SESSION['web_admin'][0]->restaurant_id;



							redirect(base_url().'restaurant/restaurant/addRestaurant/'.$resturent_id.'');

						}

                        

					}

					else

					{	

					    

                            

						$this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);

						$this->show_view_restaurant('restaurant/restaturant_edit', $this->data);

					}		

				}

				else

				{

				    

                   

				    $this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);

					$this->show_view_restaurant('restaurant/restaurant_edit',$this->data);

				}

			

			

		}

		else

		{		

		    if (isset($_POST['submit']) && $_POST['submit'] == "Add") 

		    {

		        

		        

		          

				    $this->form_validation->set_rules($this->validation_rules['restaturantAdd']);

					if($this->form_validation->run())

					{

						$post['first_name'] = $this->input->post('first_name');

					    $post['restaurant_name'] = $this->input->post('restaurant_name');

					    $post['restaurant_name'] = $this->input->post('restaurant_name');

                        $post['bank_details']    = $this->input->post('bank_details');

						$post['surname'] = $this->input->post('surname');

				        $post['address']    = $this->input->post('address');

                        $post['user_type'] = 'restaurant';

						$post['mobile_number'] = $this->input->post('mobile_number');

						$post['password'] = md5($this->input->post('password'));

						$post['email'] = $this->input->post('email');

						$post['city'] = $this->input->post('city');

						$post['post_code'] = $this->input->post('post_code');

						$post['status'] = $this->input->post('status');

						$post['created_date'] = date('Y-m-d H:i:s');

						$post['updated_date'] = date('Y-m-d H:i:s');

						if ($_FILES["image"]["name"])



                        {



                                $image = 'image';



                                $fieldName = "image";



                                $Path = 'image/restorent/';



                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);

                                 $post['restaurant_image'] = base_url().$Path.''.$image;



                                



                        }

                        $driver_id =  $this->Restaturant_model->addRestaurant($post);	

						if($driver_id)

						{					

							$msg = 'Restaurant added successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurant/restaurant/');

						}

					}

					else

					{				



						$this->show_view_admin('restaurant/addrestaurant');

					}		

				}

		    else

		    {

              $this->show_view_admin('restaurant/addrestaurant');

	       }

	   }

			

		

		

    }

    public function dashboard()

    {

        

      

      $this->show_view_admin('admin/dashbord');

        

    }

    

    public function logout() 

	{        

        $this->session->sess_destroy();		

        redirect(base_url());

    }

    

    public function delete_driver()

	{

			$driver_id = $this->uri->segment(3);	

			$this->Driver_model->deletedriver($driver_id);

			$msg = 'Driver remove successfully...!';					

		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

		    redirect(base_url().'driver');

			

		

				

	}

	

	public function fullViewRestaurant()

	{

	    $restaurant_id        = $this->uri->segment(4);

        $this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);

        $this->show_view_admin('admin/restaurant_view',$this->data);

	    

    }

    

    public function transactionDetails()

    {

        

        $resturent_id                     = $this->uri->segment(4);

        $this->data['membership_details'] = $this->Restaturant_model->getTransectionDetails($resturent_id);

        $this->show_view_restaurant('restaurant/transection_details',$this->data);

        

    

    }



}

    

    

    





/* End of file */