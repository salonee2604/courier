<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Membership extends MY_Controller  

{

    

    

	public function __construct()

	{
        parent::__construct();
        
		$this->load->model('restaurant/Membership_model');
        $this->load->library('Paypal_lib');

    }
    public function index()
	{
	   
	   $restaurant_id                    =     $_SESSION['web_admin'][0]->restaurant_id;
       $this->data['resturent_details']  =      $this->Membership_model->getRestaurantListById($restaurant_id);
       $this->show_view_restaurant('restaurant/member_list',$this->data);
	
        
    }
    
    public function buy($id)
    { 
        
        // Set variables for paypal form 
        $returnURL = base_url().'restaurant/paypal/success';
       //payment success url 
        
        $cancelURL = base_url().'restaurant/paypal/cancel'; //payment cancel url 
        $notifyURL = base_url().'restaurant/paypal/ipn'; //ipn url 
        
        // Get product data from the database 
        $restaurant_id      =     $_SESSION['web_admin'][0]->restaurant_id;
        
        $resturent_details  = $this->Membership_model->getRestaurantListById($restaurant_id);
        // Add fields to paypal form 
        $this->paypal_lib->add_field('return', $returnURL); 
        $this->paypal_lib->add_field('cancel_return', $cancelURL); 
        $this->paypal_lib->add_field('notify_url', $notifyURL); 
        $this->paypal_lib->add_field('item_name', $resturent_details->restaurant_name); 
        $this->paypal_lib->add_field('custom', $resturent_details->restaurant_id); 
        $this->paypal_lib->add_field('item_number',  1); 
        $this->paypal_lib->add_field('amount',  2); 
       
        // Render paypal form 
        $this->paypal_lib->paypal_auto_form(); 
    } 
    
    public function sms_status()
    {
        
        $resturent_id      = $this->uri->segment(4);
        $resturent_details =  $this->db->get_where('restaurant',array('restaurant_id'=>$resturent_id))->row();
        $status                 =  $resturent_details->sms_status;
    	if($status == 0){  
    	$institution_status= 1;  
    	}        else{  
    	$institution_status= 0; 
    	}	
    	$data = ['sms_status' => $institution_status,]; 
    	$this->db->where('restaurant_id',$resturent_id);  
    	$this->db->update('restaurant', $data); 
    	redirect(base_url().'restaurant/Membership');
        
    }
    
    public function status_termination()
    {
        
        $resturent_id  = $this->uri->segment(4);
         $resturent_details =  $this->db->get_where('restaurant',array('restaurant_id'=>$resturent_id))->row();
        $status                 =  $resturent_details->termination_staus;
        $date = date('Y-m-d', strtotime("+30 day"));
    	if($status == 0){  
    	$institution_status= 1;  
    	}        
    	else
    	{  
    	  $institution_status= 0; 
    	}	
    	$data = ['termination_staus' => $institution_status,'termination_date' =>$date];
    
    	$this->db->where('restaurant_id',$resturent_id);  
    	$this->db->update('restaurant', $data);
    	redirect(base_url().'restaurant/Membership');

        
    }
    
    

}
    
    
    
    






    

    

    





/* End of file */