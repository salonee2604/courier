<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends MY_Controller  

{

    

    

	public function __construct()

	{
        parent::__construct();
        
		$this->load->model('restaurant/Parcel_model');
		$this->load->library('Twilio');

    }

	



		

	/* Login */

	public function message()
	{
	    
	    
		/*load registration view form*/
		$this->show_view_admin('message_view');
	    
		/*Check submit button */
		if($this->input->post('save'))
		{
		    
		$phone=$this->input->post("phone");
        $user_message=$this->input->post("message");
        $success     = $this->sendMessage($phone,$user_message);
        echo 
        $success;
        
		}

    }
    public function liveLocationall()
    {
		 $resid = $_SESSION['web_admin'][0]->restaurant_id;
		 $this->data['trackingtrip'] =  $this->Parcel_model->gettrackingtrip($resid);	
		 $this->show_view_restaurant('restaurant/trackingtripall',$this->data);	
	}

	 public function demo()
    {
		 $resid = $_SESSION['web_admin'][0]->restaurant_id;
		 $this->data['trackingtrip'] =  $this->Parcel_model->gettrackingtrip($resid);	
		 $this->show_view_restaurant('restaurant/demo',$this->data);	
	}

	public function connectNearestDriver()
	{

	    $resid = $_SESSION['web_admin'][0]->restaurant_id;

		$this->data['trackingtrip'] =  $this->Parcel_model->gettrackingtrip($resid);
		$this->data['map_parcel_list'] =  $this->Parcel_model->getParcelList($resid);
		$this->data['driver_list']  = $this->Parcel_model->getAllDriverData($	     $resid);
        $this->data['parcel_list']   = $this->Parcel_model->getParcelList($resid);
        $this->show_view_restaurant('restaurant/connect_nearest_driver',$this->data);	
	}
    public function liveLocation()
    {


            $restaurant_id              = $_SESSION['web_admin'][0]->restaurant_id;
            $loc_res        = $this->Parcel_model->getLiveLocation($restaurant_id);
        	$loc_res_1_arr = array();
                foreach($loc_res as $res)
				{
				   
					$driver_details = $this->Parcel_model->getAllDriverListByDriverID($res->driver_id,$restaurant_id);
					
					$driver_id = $driver_details->driver_id;
					$trip_details = $this->Parcel_model->getTripByDriverId($driver_id,$restaurant_id);
				    $parcel_id    = $trip_details->parcel_id;
				    $parcel_array = explode(",",$parcel_id);
				    $parceldeatils = array();
				    foreach($parcel_array as $parcel_id_one)
				    {
				         
				        $parceldeatils[]= $this->Parcel_model->getParcelByParcelId($parcel_id_one,$restaurant_id);
				    }
				    
				    $drop_location =  $parceldeatils[1]->drop_location;
				      $drop_location_main            = '<p style="color: green";>'.$drop_location.'<p>';
						
				    $status = $res->parcel_status;
				    if($status == 1)
					{
					    $assignstatus = 'Ready to take order';
					}
					elseif($status == 2)
					{
					    $assignstatus = 'Parcel Delivered';
					}
					elseif($status == 3)
					{
					 
					   $assignstatus = 'Ready to delivery';
   
					}
					elseif($status == 4)
					{
					    
					    $assignstatus  = 'On the way';
					    
					}
					elseif($status == 5)
					{
					  
					  $assignstatus  = 'Pending for delivery';

					}
					else
					{
					    $assignstatus = 'Parcel delivery';
   
					}
					if(!empty($driver_details))
					{
						
						 $driverdetails    = '<p style="color: Blue";>'.$driver_details->first_name.' '.$driver_details->surname.'<p>';
						
						$loc_res_1_arr[] = array($res->location_lat,$res->location_long,$driverdetails.' '.($assignstatus).$drop_location_main);
						
						
				    }
			        $loc_res_1_json = json_encode($loc_res_1_arr); 

			    }
			    $i++;

			   $this->data['details'] =  $loc_res_1_json;
			  $this->show_view_restaurant('restaurant/location_list',$this->data);
    }

    public function addParcel()
    {


            if (isset($_POST['addParcelsubmit']))

		    {
		        

					    $post['pic_up_location']   = $this->input->post('pickup_location');

						$input_pickup_location     = $this->input->post('pickup_location');

						$post['parcel_added_by']   = 'web';

						$address                   =  $_SESSION['web_admin'][0]->address;

					    if(!empty($input_pickup_location))
						{
                            
                             $pickup_location                = $input_pickup_location;
                             $post['pic_up_location']        = $pickup_location;

                             $post['pickup_location_status'] =  1;

						}
						else
						{
                               
                            $pickup_location                = $address;
                            $post['pic_up_location']        = $address;
                            $post['pickup_location_status'] =  0;

						}

						$drop_location               = $this->input->post('drop_location');

						$post['drop_location']       = $this->input->post('drop_location');

						$post['parcel_information']  = $this->input->post('parcel_information');
						
						$post['own_note']            = $this->input->post('own_note');

                        $post['pickup_latitude']     = $this->input->post('pickup_latitude');

                        $post['pickup_longitude']    = $this->input->post('pickup_longitude');

                        $post['drop_latitude']       = $this->input->post('drop_latitude');

                        $post['drop_longitude']     = $this->input->post('drop_longitude');

                        $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;
                        $post['restaurant_id'] =      $restaurant_id;
						$post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 

						$address = $pickup_location; // Address

						$address2 = $drop_location;

                        $apiKey = 'AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw'; // Google maps now requires an API key.

                        // Get JSON results from this request

                        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);

                        $geo = json_decode($geo, true); // Convert the JSON to an array

                        $drop_location = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address2).'&sensor=false&key='.$apiKey);

                        $drop_location = json_decode($drop_location, true); // Convert the JSON to an array

                        if (isset($geo['status']) && ($geo['status'] == 'OK')) {

	                        $latitude = $geo['results'][0]['geometry']['location']['lat']; 

	                        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude

	                        $drop_latitude = $drop_location['results'][0]['geometry']['location']['lat']; 

	                        $drop_longitude = $drop_location['results'][0]['geometry']['location']['lng']; // Longitude



	                        $post['pickup_latitude']   = $latitude;

	                        $post['pickup_longitude']  = $longitude;

	                        $post['drop_latitude']     = $drop_latitude;

	                        $post['drop_longitude']    = $drop_longitude;

                        }


                        //echo "<pre>"; print_r($post);die;

						$parcel_id =  $this->Parcel_model->addParcel($post);

					    $post_customer['first_name'] = $this->input->post('first_name');
                        $post_customer['last_name'] = $this->input->post('last_name');
                        $post_customer['customer_number']    = $this->input->post('customer_number');
                        $post_customer['customer_address']  = $this->input->post('drop_location');
                        $post_customer['created_date']      = date('y-m-d H:i:s');
                        $post_customer['updated_date']      = date('y-m-d H:i:s');
                        $post_customer['parcel_id']         = $parcel_id;
                        $post_customer['customer_zip_code']   = $this->input->post('customer_zip_code');
                        $post_customer['customer_country']   = 210;
						$post_customer['customer_state']   = '';
                        $post_customer['customer_city']   = $this->input->post('city');
                        $post_customer['restaurant_id']     =  $_SESSION['web_admin'][0]->restaurant_id;
                        $customer_id                          =  $this->Parcel_model->addCustomer($post_customer);

						if($parcel_id)
                        {
						   
						   
						  $smsDetails = $this->Parcel_model->getSmsDetails($restaurant_id); 
						  
    						 if($smsDetails->sms_status == 1)
    						 {
        						 $sms_sender ="19737989690"; 
                                 $sms_reciever = $this->input->post('customer_number');
                                 $sms_message = "Dear customer your order will arrive in 7 minutes.";
                                 $from = '+'.$sms_sender; //trial account twilio number
                                 $to = '+91'.$sms_reciever; //sms recipient number
                                //echo $from; die;        						
                                 $response = $this->twilio->sms($from, $to,$sms_message);
								 if($response){
																		 $uid = $_SESSION['web_admin'][0]->restaurant_id;
							        $sql = $this->db->get_where('smsmonthdata',array('restr_id'=>$uid))->result_array();
									$mo = explode('-',date("m-d-Y"));
									
									if($mo[0]==01){
									if(!empty($sql)){
									$smsdata = $sql[0]['january'];
									$dd = ['january' => $smsdata+1,];}
									else{
									$dd = ['january' =>1,'restr_id' =>$uid];
									}
									}
									
									if($mo[0]==02){
									if(!empty($sql)){	
									$smsdata = $sql[0]['february'];
									$dd = ['february' => $smsdata+1,];}else{
									$dd = ['february' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==03){
									if(!empty($sql)){		
									$smsdata = $sql[0]['march'];
									$dd = ['march' => $smsdata+1,];}else{
									$dd = ['march' =>1,'restr_id' =>$uid];		
									}
									}
									
									if($mo[0]==04){
									if(!empty($sql)){	
									$smsdata = $sql[0]['april'];
									$dd = ['april' => $smsdata+1,];}else{
									$dd = ['april' =>1,'restr_id' =>$uid];			
									}
									}
									
									if($mo[0]==05){
									if(!empty($sql)){	
									$smsdata = $sql[0]['may'];
									$dd = ['may' => $smsdata+1,];}else{
									$dd = ['may' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==06){
									if(!empty($sql)){		
									$smsdata = $sql[0]['june'];
									$dd = ['june' => $smsdata+1,];}else{
									$dd = ['june' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==07){	
									if(!empty($sql)){		
									$smsdata = $sql[0]['july'];
									$dd = ['july' => $smsdata+1,];}
									else{
									$dd = ['july' =>1,'restr_id' =>$uid];		
									}
									}
									
									
									 if($mo[0]>= 07){
									if(!empty($sql)){		
									$smsdata = $sql[0]['august'];
									$dd = ['august' => $smsdata+1];}else{
									$dd = ['august' =>1,'restr_id' =>$uid];		
									}
									}								
									
									
									/*if($mo[0]==09){
									if(!empty(!$sql)){	
									$smsdata = $sql[0]['september'];
									$dd = ['september' => $smsdata+1,];}else{
									$dd = ['september' =>1,'restr_id' =>$uid];		
									}
									}
									
									if($mo[0]==10){
									if(!empty(!$sql)){		
									$smsdata = $sql[0]['october'];
									$dd = ['october' => $smsdata+1,];}else{
									$dd = ['october' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==11){
									if(!empty(!$sql)){	
									$smsdata = $sql[0]['november'];
									$dd = ['november' => $smsdata+1,];
									}else{
									$dd = ['november' =>1,'restr_id' =>$uid];	
									}
									}
									
									else($mo[0]==12){
									if(!empty(!$sql)){	
									$smsdata = $sql[0]['december'];
									$dd = ['december' => $smsdata+1,];}else{
									$dd = ['december' =>1,'restr_id' =>$uid];		
									}
									} */
							     //echo 'hello'; die;

									if(!empty($sql)){										
									 $data = $dd;
									 $this->db->where('restr_id', $_SESSION['web_admin'][0]->restaurant_id);
									 $this->db->update('smsmonthdata', $data);
									
									 }
									 else {										 
									 $data = $dd;
									 $this->db->insert('smsmonthdata',$data);
									 //echo $this->db->last_query(); die;
									 }
								 }
    						 }
                             
                            echo "1";
							// $msg = 'Parcel added successfully!!';		
							// $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							//  redirect(base_url().'restaurant/parcel');

						}

					

					

			}
			else
            {
            	echo "0";
            }




    }

    public function delete_Parcel()

	{

			$parcel_id                          = $this->uri->segment(4);	
            $post['connecting_nearest_status']  = 1;
			$this->Parcel_model->updateParcel($post,$parcel_id);
			$msg = 'Parcel remove successfully...!';					
            $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
            redirect(base_url().'restaurant/message/connectNearestDriver');
    }

    public function connected_driver_list()
    {
       $this->data['connected_driver_list'] = $this->Parcel_model->connected_driver_list();
       $this->show_view_restaurant('restaurant/connected_driver_list',$this->data);	
	
    }

    
    
    




}

    

    

    





/* End of file */