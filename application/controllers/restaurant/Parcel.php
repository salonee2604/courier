<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parcel extends MY_Controller  

{

    

    

	function __construct()

	{

		parent::__construct();

		$this->load->model('restaurant/Parcel_model');

		$this->load->library('form_validation');
        
		$this->load->library('Twilio');


	}

	

	/*	Validation Rules */

	 protected $validation_rules = array

        (

        'parcelAdd' => array(

           

			array(

                'field' => 'first_name',

                'label' => 'First name',

                'rules' => 'trim|required'


            ),
            array(

                'field' => 'last_name',

                'label' => 'Last Name',

                'rules' => 'trim|required'


            ),
			array(

                'field' => 'customer_number',

                'label' => 'customer Number',

                'rules' => 'trim|required'


            ),
			
			
			// array(

   //              'field' => 'pickup_location',

   //              'label' => 'Pickup Location',

   //              'rules' => 'trim|required'

   //          ),
            array(

                'field' => 'drop_location',

                'label' => 'Drop Location',

                'rules' => 'trim|required'

            )

             
					

        ),

		'parcelEdit' => array

		(

           array(

                'field' => 'first_name',

                'label' => 'first_name',

                'rules' => 'trim|required'


            ),
            array(

                'field' => 'last_name',

                'label' => 'Last Name',

                'rules' => 'trim|required'


            ),
			array(

                'field' => 'customer_number',

                'label' => 'Customer Number',

                'rules' => 'trim|required'

            ),
            
		
			// array(

   //              'field' => 'pickup_location',

   //              'label' => 'Pickup Location',

   //              'rules' => 'trim|required'

   //          ),
            array(

                'field' => 'drop_location',

                'label' => 'Drop Location',

                'rules' => 'trim|required'

            )


             

			
        )

    );

		

	/* Login */

	public function index()

	{
       
       
       $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;
       $this->data['parcel_details'] =  $this->Parcel_model->getAllParcelData($restaurant_id);
       $this->show_view_restaurant('restaurant/parcel_list',$this->data);

	    

	}
	
	public function getstate()
    {
        $id =  $this->input->post('id'); 
        $this->data['statelist']= $this->Parcel_model->getstate($id);
        $data1                        ='<option value="">Select One</option>';
        foreach ($this->data['statelist'] as $plist){							  
    	 $data .=   '<option value="'.$plist->region_id .'">'.$plist->name.'</option>';
    } 
    
      echo $data1.$data;
    }
	
	public function getcity()
    {
    $id =  $this->input->post('id'); 
    $this->data['citylist']= $this->Parcel_model->getcity($id);
    foreach ($this->data['citylist'] as $plist){							  
	echo $data .=   '<option value="'.$plist->city_id .'">'.$plist->name.'</option>';
    }   
    }
	public function smsmonthlist()
    {
	 $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;	
	$this->data['smscoundata']  = $this->db->get_where('smsmonthdata',array('restr_id'=> $restaurant_id))->result_array();
    $this->show_view_restaurant('restaurant/smsmonthrList',$this->data);
        
    }

	public function addparcel($parcel_id='')

	{ 
       
	    if($parcel_id)

		{  

				if (isset($_POST['editsubmit'])) 

				{
				     
                     $this->form_validation->set_rules($this->validation_rules['parcelEdit']);

					if($this->form_validation->run())

					{ 

                        
                        $address                   = $_SESSION['web_admin'][0]->address;
						$pickup_location           = $this->input->post('pickup_location');
                        if(!empty($pickup_location))
						{


							$post['pic_up_location']   = $pickup_location;

						}
                        else
						{

						    $post['pic_up_location'] = $address;
						}
						

						$post['drop_location']     = $this->input->post('drop_location');
                         $post['own_note'] = $this->input->post('own_note');
						$post['parcel_information'] = $this->input->post('parcel_information');
                        $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;
                        $post['restaurant_id'] = $restaurant_id;
					    $post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s');
						$pickup_latitude          = $this->input->post('pickup_latitude');
						if(!empty($pickup_latitude))
						{
						   $post['pickup_latitude']     = $this->input->post('pickup_latitude'); 
						}
						else
                        {


                        	$post['pickup_longitude']    = $_SESSION['web_admin'][0]->latitude;
                        }
						$pickup_longitude = $this->input->post('pickup_longitude');
						if(!empty($pickup_longitude))
						{
						    $post['pickup_longitude']     = $this->input->post('pickup_longitude');
                        }
                        else
                        {

                        	$post['pickup_longitude']    = $_SESSION['web_admin'][0]->longitude;

                        	
                        }


						

						$drop_latitude          = $this->input->post('drop_latitude');
						if(!empty($drop_latitude))
						  {
						    
						      $post['drop_latitude']     = $this->input->post('drop_latitude');

						      
						  }
						  $drop_longitude             =  $this->input->post('drop_longitude');
                        if(!empty($drop_longitude))
                        {
                           
                           $post['drop_longitude']     = $this->input->post('drop_longitude');
                            
                        }
                    
                        $parcel_id_update =  $this->Parcel_model->updateParcel($post,$parcel_id);
                        $post_customer['first_name'] = $this->input->post('first_name');
                        $post_customer['last_name'] = $this->input->post('last_name');
                        $post_customer['customer_number']    = $this->input->post('customer_number');
                        $post_customer['customer_address']  = $this->input->post('drop_location');
                        $post_customer['created_date']      = date('y-m-d H:i:s');
                        $post_customer['updated_date']      = date('y-m-d H:i:s');
                        $post_customer['parcel_id']         = $parcel_id;
                        $post_customer['restaurant_id']     =       $_SESSION['web_admin'][0]->restaurant_id;
                        $post_customer['customer_zip_code']   = $this->input->post('customer_zip_code');
                        $post_customer['customer_country']   =  '';
						$post_customer['customer_state']   = $this->input->post('state');
                        $post_customer['customer_city']   = $this->input->post('city');
                        $customer_id                          =  $this->Parcel_model->updateCustomer($post_customer,$parcel_id);
                        if($parcel_id)

						{					

							$msg = 'Parcel Updated successfully!!';		
							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
                             redirect(base_url().'restaurant/parcel');

						}

                        

					}

					else

					{				

						$this->data['edit_parcel'] = $this->Parcel_model->getParcelid($parcel_id);
                        $this->data['countrylist'] = $this->Parcel_model->getcountry();
						$this->data['statelist'] = $this->Parcel_model->getstates();
						$this->data['citylist'] = $this->Parcel_model->getcitys();
						$this->show_view_restaurant('restaurant/edit_parcel', $this->data);

					}		

				}

				else

				{

				    $this->data['edit_parcel'] = $this->Parcel_model->getParcelid($parcel_id);
					    $this->data['countrylist'] = $this->Parcel_model->getcountry();
						$this->data['statelist'] = $this->Parcel_model->getstates();
						$this->data['citylist'] = $this->Parcel_model->getcitys();
                    $this->show_view_restaurant('restaurant/edit_parcel',$this->data);

				}

			

			

		}

		else

		{		

		    if (isset($_POST['addsubmit'])) 

		    {
		        

					$this->form_validation->set_rules($this->validation_rules['parcelAdd']);

					if($this->form_validation->run())

					{

		                $address = $_SESSION['web_admin'][0]->address;
						$pickup_location_input           = $this->input->post('pickup_location');
                        if(!empty($pickup_location_input))
						{

                            $pickup_location                = $pickup_location_input;
							$post['pic_up_location']        = $pickup_location;
							$post['pickup_location_status'] =  1;

						}
                        else
						{
                            $pickup_location                = $address;
							$post['pic_up_location']        = $address;
							$post['pickup_location_status'] =  0;
						}

						// $pickup_location           = $this->input->post('pickup_location');

						$drop_location           = $this->input->post('drop_location');

						$post['drop_location']     = $this->input->post('drop_location');

						$post['parcel_information'] = $this->input->post('parcel_information');

						$post['parcel_added_by']    = 'web';
						
						$post['own_note'] = $this->input->post('own_note');


					    $post['pickup_latitude']     = $this->input->post('pickup_latitude');

                        $post['pickup_longitude']     = $this->input->post('pickup_longitude');

                        $post['drop_latitude']     = $this->input->post('drop_latitude');

                        $post['drop_longitude']     = $this->input->post('drop_longitude');

                        $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;
                        $post['restaurant_id'] =      $restaurant_id;
						$post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 

						$address = $pickup_location; // Address

						$address2 = $drop_location;

                        $apiKey = 'AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw'; // Google maps now requires an API key.

                        // Get JSON results from this request

                        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);

                        $geo = json_decode($geo, true); // Convert the JSON to an array

                        $drop_location = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address2).'&sensor=false&key='.$apiKey);

                        $drop_location = json_decode($drop_location, true); // Convert the JSON to an array

                        if (isset($geo['status']) && ($geo['status'] == 'OK')) {

	                        $latitude = $geo['results'][0]['geometry']['location']['lat']; 

	                        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude

	                        $drop_latitude = $drop_location['results'][0]['geometry']['location']['lat']; 

	                        $drop_longitude = $drop_location['results'][0]['geometry']['location']['lng']; // Longitude



	                        $post['pickup_latitude']   = $latitude;

	                        $post['pickup_longitude']  = $longitude;

	                        $post['drop_latitude']     = $drop_latitude;

	                        $post['drop_longitude']    = $drop_longitude;

                        }


                        //echo "<pre>"; print_r($post);die;

						$parcel_id =  $this->Parcel_model->addParcel($post);

					    $post_customer['first_name'] = $this->input->post('first_name');
                        $post_customer['last_name'] = $this->input->post('last_name');
                        $post_customer['customer_number']    = $this->input->post('customer_number');
                        $post_customer['customer_address']  = $this->input->post('drop_location');
                        $post_customer['created_date']      = date('y-m-d H:i:s');
                        $post_customer['updated_date']      = date('y-m-d H:i:s');
                        $post_customer['parcel_id']         = $parcel_id;
                        $post_customer['customer_zip_code']   = $this->input->post('customer_zip_code');
                        $post_customer['customer_country']   = 210;
						$post_customer['customer_state']   = '';
                        $post_customer['customer_city']   = $this->input->post('city');
                        $post_customer['restaurant_id']     =  $_SESSION['web_admin'][0]->restaurant_id;
                        $customer_id                          =  $this->Parcel_model->addCustomer($post_customer);

						if($parcel_id)
                        {
						   

						  $smsDetails = $this->Parcel_model->getSmsDetails($restaurant_id); 
						  
    						 if($smsDetails->sms_status == 1)
    						 {


        						 $sms_sender ="19737989690"; 
                                 $sms_reciever = $this->input->post('customer_number');
                                 $sms_message = "Dear customer your order will arrive in 7 minutes.";
                                 $from = '+'.$sms_sender; //trial account twilio number
                                 $to = '+91'.$sms_reciever; //sms recipient number
                                //echo $from; die;        						
                                 $response = $this->twilio->sms($from, $to,$sms_message);
								 if($response){


				                    $uid = $_SESSION['web_admin'][0]->restaurant_id;
							        $sql = $this->db->get_where('smsmonthdata',array('restr_id'=>$uid))->result_array();
									$mo = explode('-',date("m-d-Y"));
									
									if($mo[0]==01){
									if(!empty($sql)){
									$smsdata = $sql[0]['january'];
									$dd = ['january' => $smsdata+1,];}
									else{
									$dd = ['january' =>1,'restr_id' =>$uid];
									}
									}
									
									if($mo[0]==02){
									if(!empty($sql)){	
									$smsdata = $sql[0]['february'];
									$dd = ['february' => $smsdata+1,];}else{
									$dd = ['february' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==03){
									if(!empty($sql)){		
									$smsdata = $sql[0]['march'];
									$dd = ['march' => $smsdata+1,];}else{
									$dd = ['march' =>1,'restr_id' =>$uid];		
									}
									}
									
									if($mo[0]==04){
									if(!empty($sql)){	
									$smsdata = $sql[0]['april'];
									$dd = ['april' => $smsdata+1,];}else{
									$dd = ['april' =>1,'restr_id' =>$uid];			
									}
									}
									
									if($mo[0]==05){
									if(!empty($sql)){	
									$smsdata = $sql[0]['may'];
									$dd = ['may' => $smsdata+1,];}else{
									$dd = ['may' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==06){
									if(!empty($sql)){		
									$smsdata = $sql[0]['june'];
									$dd = ['june' => $smsdata+1,];}else{
									$dd = ['june' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==07){	
									if(!empty($sql)){		
									$smsdata = $sql[0]['july'];
									$dd = ['july' => $smsdata+1,];}
									else{
									$dd = ['july' =>1,'restr_id' =>$uid];		
									}
									}
									
									
									 if($mo[0]>= 07){
									if(!empty($sql)){		
									$smsdata = $sql[0]['august'];
									$dd = ['august' => $smsdata+1];}else{
									$dd = ['august' =>1,'restr_id' =>$uid];		
									}
									}								
									
									
									/* if($mo[0]==09){
									if(!empty(!$sql)){	
									$smsdata = $sql[0]['september'];
									$dd = ['september' => $smsdata+1,];}else{
									$dd = ['september' =>1,'restr_id' =>$uid];		
									}
									}
									
									if($mo[0]==10){
									if(!empty(!$sql)){		
									$smsdata = $sql[0]['october'];
									$dd = ['october' => $smsdata+1,];}else{
									$dd = ['october' =>1,'restr_id' =>$uid];	
									}
									}
									
									if($mo[0]==11){
									if(!empty(!$sql)){	
									$smsdata = $sql[0]['november'];
									$dd = ['november' => $smsdata+1,];
									}else{
									$dd = ['november' =>1,'restr_id' =>$uid];	
									}
									}
									
									else($mo[0]==12){
									if(!empty(!$sql)){	
									$smsdata = $sql[0]['december'];
									$dd = ['december' => $smsdata+1,];}else{
									$dd = ['december' =>1,'restr_id' =>$uid];		
									}
									} */
							     //echo 'hello'; die;

									if(!empty($sql)){
									
									 									
									 $data = $dd;
									 $this->db->where('restr_id', $_SESSION['web_admin'][0]->restaurant_id);
									 $this->db->update('smsmonthdata', $data);
									
									
									 }
									 else {										 
									 $data = $dd;
									 $this->db->insert('smsmonthdata',$data);
									 //echo $this->db->last_query(); die;
									 }
								 }
    						 }


							$msg = 'Parcel added successfully!!';		
							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							 redirect(base_url().'restaurant/parcel');

						}

					}

					else

					{				
                        $this->data['countrylist'] = $this->Parcel_model->getcountry();
						$this->show_view_restaurant('restaurant/addparcel',$this->data);

					}		

			}

		    else

		    {
              $this->data['countrylist'] = $this->Parcel_model->getcountry();
              $this->show_view_restaurant('restaurant/addparcel',$this->data);

	       }

	   }

			

		

		

    }

    public function dashboard()

    {

      $this->show_view_restaurant('restaurant/dashbord');

        

    }

    

    public function logout() 

	{        

        $this->session->sess_destroy();		
        redirect(base_url());

    }
    
    public function parcelFullView()
    {
        $parcel_id = $this->uri->segment(4);
        $this->data['edit_parcel'] = $this->Parcel_model->getParcelid($parcel_id);
        $this->show_view_restaurant('restaurant/parcel_view',$this->data);                  
        
    }

    

    public function delete_Parcel()

	{

			$parcel_id = $this->uri->segment(4);	

			$this->Parcel_model->deleteParcel($parcel_id);

			$msg = 'Parcel remove successfully...!';					

		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

		    redirect(base_url().'restaurant/parcel');
    }



}

    

    

    





/* End of file */