<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver extends MY_Controller  

{

    

    

	function __construct()

	{

		parent::__construct();

		$this->load->model('restaurant/Driver_model');

		$this->load->library('form_validation');

		$this->load->library('Ciqrcode');





	}

	

	/*	Validation Rules */

	 protected $validation_rules = array

        (

        'driverAdd' => array(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required|is_unique[ driver.email]'

            ),

			array(

                'field' => 'mobile_number',

                'label' => 'Mobile Number',

                'rules' => 'trim|required'

            ),

             array( 

				'field' => 'password', 

				'label' => 'Password',   

				'rules' => 'trim|required'  

			),

			array(  

				'field' => 'c_password',

				'label' => 'Confirm Password', 

				'rules' => 'trim|required|matches[password]'

            ),			

            array(

                'field' => 'surname',

                'label' => 'surname',

                'rules' => 'trim|required'

            ),



            

            array(

                'field' => 'city',

                'label' => 'City',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'post_code',

                'label' => 'post code',

                'rules' => 'trim|required'

            )

                     

                  

        ),

		'driverEdit' => array

		(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'email',

                'label' => 'Email',

                'rules' => 'trim|required'

            ),

			array(

                'field' => 'mobile_number',

                'label' => 'Mobile Number',

                'rules' => 'trim|required'

            ),

             array( 

				'field' => 'password', 

				'label' => 'Password',   

				'rules' => 'trim'  

			),

			array(  

				'field' => 'c_password',

				'label' => 'Confirm Password', 

				'rules' => 'trim|matches[password]'

            ),			

            array(

                'field' => 'surname',

                'label' => 'surname',

                'rules' => 'trim|required'

            ),



            

            array(

                'field' => 'city',

                'label' => 'City',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'post_code',

                'label' => 'post code',

                'rules' => 'trim|required'

            )    

        )

    );

		

	/* Login */

	public function index()

	{

	     $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;

	     $this->data['driver_details'] = $this->Driver_model->getClientList($restaurant_id);

	     $this->show_view_restaurant('restaurant/driverList',$this->data);

  

	}

	public function adddriver($driver_id='')

	{



	   

	    if($driver_id)

		{

			

			        

				if (isset($_POST['editdriversubmit'])) 

				{

				    $this->form_validation->set_rules($this->validation_rules['driverEdit']);

					if($this->form_validation->run())

					{

					    

                        $post['first_name'] = $this->input->post('first_name');

						$post['address']    = $this->input->post('address');

						$post['surname'] = $this->input->post('surname');

						$post['mobile_number'] = $this->input->post('mobile_number');

						$password = md5($this->input->post('password'));
						if(!empty($password))
						{
							$post['password']    = $password;
						}
                        
						$post['email'] = $this->input->post('email');

						$post['city'] = $this->input->post('city');

						$post['post_code'] = $this->input->post('post_code');

						$post['status'] = $this->input->post('status');

						$restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;

					    $post['restaurant_id'] = $restaurant_id; 

						$post['create_date'] = date('Y-m-d H:i:s');

						$post['update_date'] = date('Y-m-d H:i:s');

						if ($_FILES["image"]["name"])



                        {



                                $image = 'image';



                                $fieldName = "image";



                                $Path = 'image/driver/';



                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);



                              $post['driver_image'] = base_url().$Path.''.$image;



                                



                        }

                        $driver_id =  $this->Driver_model->updateDriver($post,$driver_id);

                        if($driver_id)

						{					

							$msg = 'Driver Updated successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurant/driver');

						}

                        

					}

					else

					{	

					    

                            $driver_id        = $this->uri->segment(4);

                            $this->data['edit_driver'] = $this->Driver_model->getdriverbyid($driver_id);

                            $driver_details  = $this->Driver_model->getdriverbyid($driver_id);

                            $qr_image=rand().'.png';

                            $params['data']   = $driver_id;

                            $params['level']  = 'H';

                            $params['size']   = 8;

                            $params['savename'] =FCPATH."image/".$qr_image;

                            if($this->ciqrcode->generate($params))

                            {

                            $this->data['img_url']=$qr_image;	

                            }

						$this->data['edit_driver'] = $this->Driver_model->getdriverbyid($driver_id);

						$this->show_view_restaurant('restaurant/edit_driver', $this->data);

					}		

				}

				else

				{

				    

                    $driver_id        = $this->uri->segment(4);

                    $this->data['edit_driver'] = $this->Driver_model->getdriverbyid($driver_id);

                    $driver_details  = $this->Driver_model->getdriverbyid($driver_id);

                    $qr_image=rand().'.png';

                    $params['data']   = $driver_id;

                    $params['level']  = 'H';

                    $params['size']   = 8;

                    $params['savename'] =FCPATH."image/".$qr_image;

                    if($this->ciqrcode->generate($params))

                    {

                    $this->data['img_url']=$qr_image;	

                    }

				    $this->data['edit_driver'] = $this->Driver_model->getdriverbyid($driver_id);

					$this->show_view_restaurant('restaurant/edit_driver',$this->data);

				}

			

			

		}

		else

		{		

		    if (isset($_POST['adddriversubmit'])) 

		    {

					

				    $this->form_validation->set_rules($this->validation_rules['driverAdd']);

					if($this->form_validation->run())

					{

						$post['first_name'] = $this->input->post('first_name');

						$post['address']    = $this->input->post('address');

						$post['surname'] = $this->input->post('surname');

						$post['mobile_number'] = $this->input->post('mobile_number');

						$post['password'] = md5($this->input->post('password'));

						$post['email'] = $this->input->post('email');

						$post['city'] = $this->input->post('city');

						$post['post_code'] = $this->input->post('post_code');

						$post['revenue_per_order'] = $this->input->post('revenue_per_order');

					    $restaurant_id = $_SESSION['web_admin'][0]->restaurant_id;

					    $post['restaurant_id'] = $restaurant_id; 

                        $post['status'] = $this->input->post('status');

						$post['create_date'] = date('Y-m-d H:i:s');

						$post['update_date'] = date('Y-m-d H:i:s');

						if ($_FILES["image"]["name"])



                        {



                                $image = 'image';



                                $fieldName = "image";



                                $Path = 'image/driver/';



                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);

                                 $post['driver_image'] = base_url().$Path.''.$image;



                                



                        }

                        $driver_id =  $this->Driver_model->adddriver($post);	

						if($driver_id)

						{					

							$msg = 'Driver added successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'restaurant/driver');

						}

					}

					else

					{				



						$this->show_view_restaurant('restaurant/adddriver');

					}		

				}

		    else

		    {

              $this->show_view_restaurant('restaurant/adddriver');

	       }

	   }

			

		

		

    }

    public function dashboard()

    {

        

      

      $this->show_view_restaurant('dashbord');

        

    }

    

    public function logout() 

	{        

        $this->session->sess_destroy();		

        redirect(base_url());

    }

    

    public function delete_driver()

	{

			$driver_id = $this->uri->segment(4);	

			$this->Driver_model->deletedriver($driver_id);

			$msg = 'Driver remove successfully...!';					

		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

		    redirect(base_url().'restaurant/driver');

			

		

				

	}

	

	public function fullViewDriver()

	{

	    $driver_id        = $this->uri->segment(4);

        $this->data['edit_driver'] = $this->Driver_model->getdriverbyid($driver_id);

        $driver_details  = $this->Driver_model->getdriverbyid($driver_id);

        $qr_image=rand().'.png';

        $params['data']   = $driver_id;

        $params['level']  = 'H';

        $params['size']   = 8;

        $params['savename'] =FCPATH."image/".$qr_image;

        if($this->ciqrcode->generate($params))

        {

        	$this->data['img_url']=$qr_image;	

        }

	    $this->show_view_restaurant('restaurant/driver_view',$this->data);

	    

    }



}

    

    

    





/* End of file */