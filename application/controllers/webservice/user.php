<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('webservice/api_model');	
		$this->load->model('webservice/webservice_comman_model');
		$this->load->model('webservice/comman_model');
		$this->load->model('webservice/attendance_model');
	}	
	/*User login logout*/
	
	public function login()
	{
    	$post['user_email'] = $_POST['email'];
    	$post['user_password'] = md5($_POST['password']);
    	$user_detail = $this->webservice_comman_model->getData('tbl_user',$post);

    	
    	if(!empty($user_detail))
		{			
			
			echo json_encode(array("status"=>1,'data'=>$user_detail));
		}
		else
		{
			echo json_encode(array("status"=>0 , 'data'=> array()));
		}
	}

	public function logout()
	{	
		echo json_encode(array("status"=>1)); 
	}

	/*TableHSN, TableBrand, TableLeadSource, TableLeadStatus
	TableCategorySubCategory*/
	public function getCombineData()
	{
		if(isset($_POST['sync_date_time']))
		{
			$res = array();
			$sync_date_time = $_POST['sync_date_time'];
			if($sync_date_time == 0)
			{
				$res['brand'] = $this->webservice_comman_model->getData('tbl_brand',NULL , 'multi' , NULL);
				$res['hsn'] = $this->webservice_comman_model->getData('tbl_hsn',NULL , 'multi' , NULL);
				$res['leadSource'] = $this->webservice_comman_model->getData('tbl_lead_source',NULL , 'multi' , NULL);
				$res['leadStatus'] = $this->webservice_comman_model->getData('tbl_lead_status',NULL , 'multi' , NULL);
				$res['catSubCategory'] = $this->webservice_comman_model->getData('tbl_category_subcategory',NULL , 'multi' , NULL);
			}
			else
			{
				$res['brand'] = $this->webservice_comman_model->getData('tbl_brand', array('sync_date_time >'=> $sync_date_time) , 'multi', NULL);
				$res['hsn'] = $this->webservice_comman_model->getData('tbl_hsn', array('sync_date_time >'=> $sync_date_time) , 'multi', NULL);
				$res['leadSource'] = $this->webservice_comman_model->getData('tbl_lead_source', array('sync_date_time >'=> $sync_date_time) , 'multi', NULL);
				$res['leadStatus'] = $this->webservice_comman_model->getData('tbl_lead_status', array('sync_date_time >'=> $sync_date_time) , 'multi', NULL);
				$res['catSubCategory'] = $this->webservice_comman_model->getData('tbl_category_subcategory', array('sync_date_time >'=> $sync_date_time) , 'multi', NULL);
			} 
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}

	/*Attendance Start*/
	public function getAttendance()
	{
		if(isset($_POST['user_id']))
		{


			$res =  $this->webservice_comman_model->getData('tbl_attendance' , array('attendance_date'=>date("Y-m-d"),'user_id' => $_POST['user_id']), 'multi' , NULL, $_POST['user_id']);

           
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
    }

    public function addUpdateAttendance()
    {
    	//add extra column
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['attendance_id'] 				= $val->attendance_id; 
					$post['user_id'] 					= $val->user_id;
					$post['attendance_login_time'] 		= $val->attendance_login_time; 
					$post['attendance_logout_time'] 	= $val->attendance_logout_time; 
					$post['attendance_date'] 			= $val->attendance_date; 
					$post['attendance_login_lat'] 		= $val->attendance_login_lat; 
					$post['attendance_login_long'] 		= $val->attendance_login_long; 
					$post['attendance_logout_lat'] 		= $val->attendance_logout_lat; 
					$post['attendance_logout_long'] 	= $val->attendance_logout_long;
					$post['attendance_status'] 			= $val->attendance_status; 
					$post['create_date'] 				= $val->create_date; 
					$post['update_date'] 				= $val->update_date; 
					$post['sync_date_time']   			= $val->sync_date_time;
					$post['user_all_level']   			= $val->user_all_level;
					$check_res = $this->attendance_model->checkData($post['attendance_id']);
					if(!empty($check_res))
					{
						$this->attendance_model->updateData($post);
						$u_id_arr[] = array('attendance_id'=>$post['attendance_id']);
					}
					else
					{
						$attendance_id_x = $this->attendance_model->addData($post);
						if($attendance_id_x)
						{
							$u_id_arr[] = array('attendance_id'=>$post['attendance_id']);
						}
					}
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }
	/*Attendance End*/

	/*Country State*/
	public function getCountryList()
	{
    	$country_list =  $this->webservice_comman_model->getData('tbl_country', array('country_id' =>99));
    	if (!empty($country_list))
    	{
    		echo json_encode(array("status" =>1, 'data'=> $country_list));
    	}
    	else
    	{
    		echo json_encode(array("status"=>0 , 'data'=> array()));
    	}
	}

	public function getStateList()
	{
    	$state =  $this->webservice_comman_model->getData('tbl_state', array('country_id' => 99));
    	if (!empty($state))
    	{
    		echo json_encode(array("status" =>1, 'data'=> $state ));
    	}
    	else
    	{
    		echo json_encode(array("status"=>0 , 'data'=> array()));
    	}
	}

	/*Expense Start*/
	public function getExpense()
	{
		//add user_id
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time 	= $_POST['sync_date_time'];
			$user_id 			= $_POST['user_id'];
			if($sync_date_time == 0)
			{
				$res = $this->webservice_comman_model->getData('tbl_expenses',array('user_id'=>$user_id) , 'multi' , NULL, $user_id);	
			}
			else
			{
				$res = $this->webservice_comman_model->getData('tbl_expenses', array('sync_date_time >' => $sync_date_time) , 'multi', NULL, $user_id);
			}

			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
	}

	public function addUpdateExpense()
	{	
		$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		$u_id_arr = array();
		if(!empty($st_data->data))
		{	
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					
					$post['expenses_id']			= $val->expenses_id;
					$post['expenses_category_id']   = $val->expenses_category_id;
					$post['expenses_amt']          	= $val->expenses_amt;
					$post['expenses_narration']     = $val->expenses_narration;
					$post['expenses_attachment']    = $val->expenses_attachment;
					$post['user_id']          		= $val->user_id;
					$post['expenses_lat']     		= $val->expenses_lat;
					$post['expenses_long']     		= $val->expenses_long;
					$post['expenses_date']     		= $val->expenses_date;
					$post['expenses_status']     	= $val->expenses_status;
					$post['create_date']     		= (isset($val->create_date)) ? $val->create_date : $val->sync_date_time;
					$post['update_date']     		= $val->update_date;
					$post['sync_date_time']     	= $val->sync_date_time;
					$post['user_all_level']     	= $val->user_all_level;
					$post['create_by']     			= $val->create_by;
					$post['update_by']     			= $val->update_by;
					

					$check_res = $this->webservice_comman_model->check_by(array('expenses_id' => $post['expenses_id']),'tbl_expenses',$post['create_by']);

					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_expenses', array('expenses_id' => $post['expenses_id']),$post);
						$u_id_arr[] = array('expenses_id'=>$post['expenses_id']);
					}
					else
					{
						$expenses_id_x = $this->webservice_comman_model->addData('tbl_expenses',$post);
						if($expenses_id_x)
						{
							$u_id_arr[] = array('expenses_id'=>$post['expenses_id']);

							/* add  expense Approval data */

                                
								//$exp_id = $this->api_model->get_expense_id($expenses_id_x,$post['create_by']);

								$exp_id = $this->webservice_comman_model->getData('tbl_expenses', array('id'=>$expenses_id_x), 'single',NULL,$post['create_by']);
								$emp_res = $this->webservice_comman_model->getData('tbl_user', array('user_id'=>$post['user_id']), 'single', NULL, $post['create_by']);

								

								if(!empty($emp_res))
								{
									if($emp_res->designation_id != '1')
									{
										
										$designation_approval_res = $this->api_model->getdesignation_approval($emp_res->designation_id,$emp_res->department_id,$emp_res->user_id);

										if(!empty($designation_approval_res))
										{
											foreach($designation_approval_res as $da_val) 
											{
												$designation_res = $this->webservice_comman_model->getData('cm_designation', array('designation_id'=>$da_val->designation_id), 'single');
												if(!empty($designation_res))
												{

													
													if($da_val->approval_department_id == $emp_res->department_id)
													{
														$post_ap['expense_id'] = $exp_id->expenses_id;
														//$post_ap['designation_level_no'] = $designation_res->designation_level_no;

														$post_ap['designation_level_no'] =$da_val->approval_designation_id;

														$post_ap['department_id'] = $da_val->approval_department_id;
														$post_ap['designation_id'] = $da_val->approval_designation_id;
														$post_ap['approval_by_id'] = $da_val->approval_emp_id;
														$post_ap['approval_status'] = '2';
														$post_ap['approval_sequence_no'] = '1';
														$post_ap['approval_note'] = '';
														
														$post_ap['expense_approval_created_date'] = date('Y-m-d');
														$post_ap['expense_approval_updated_date'] = date('Y-m-d');
														$this->webservice_comman_model->addData('tbl_expense_approval', $post_ap);
													}
													else
													{

														$post_ap['expense_id'] = $exp_id->expenses_id;
														//$post_ap['designation_level_no'] = $designation_res->designation_level_no;
														$post_ap['designation_level_no'] = $da_val->approval_designation_id;
														
														$post_ap['department_id'] = $da_val->approval_department_id;
														$post_ap['designation_id'] = $da_val->approval_designation_id;
														$post_ap['approval_by_id'] = $da_val->approval_emp_id;
														$post_ap['approval_status'] = '2';
														$post_ap['approval_sequence_no'] = '2';
														$post_ap['approval_note'] = '';
														
														$post_ap['expense_approval_created_date'] = date('Y-m-d');
														$post_ap['expense_approval_updated_date'] = date('Y-m-d');
														
														$this->webservice_comman_model->addData('tbl_expense_approval', $post_ap);
													}
												}
											}
										}													
									}
								}
								
				
							/* add expense Approval data */
						}
						
					}
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

	public function getExpenseCategory()
	{
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			if($sync_date_time == 0)
			{
				$res = $this->webservice_comman_model->getData('tbl_expense_category',NULL , 'multi' , NULL);	
			}
			else
			{
				$res = $this->webservice_comman_model->getData('tbl_expense_category', array('sync_date_time >' => $sync_date_time) , 'multi', NULL);
			} 
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}
	/*Expense End*/
	/*Item Start*/
	public function getItemsList()
	{
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			if($sync_date_time == 0)
			{
				$res = $this->webservice_comman_model->getData('tbl_items', NULL , 'multi' , NULL);	
			}
			else
			{
				$res =$this->webservice_comman_model->getData('tbl_items',array('sync_date_time >' => $sync_date_time) , 'multi', NULL);
			}
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res));
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}

	public function addUpdateItem()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		// print_r($st_data->data);die();
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['item_id']        	= $val->item_id;
					$post['quantity']          	= $val->quantity;
					$post['item_name']          = $val->item_name;
					$post['item_desc']          = $val->item_desc;
					$post['unit_cost']          = $val->unit_cost;
					$post['model_number']       = $val->model_number;
					$post['category_id']        = $val->category_id;
					$post['sub_category_id']    = $val->sub_category_id;
					$post['brand_id']          	= $val->brand_id;
					$post['unit_id']          	= $val->unit_id;
					$post['hsn_id']          	= $val->hsn_id;
					$post['tax_id']          	= $val->tax_id;
					$post['sync_date_time']     = $val->sync_date_time;
					$post['create_by']          = $val->create_by;
					$post['update_by']          = $val->update_by;
					$post['create_date']        = $val->create_date;
					$post['update_date']        = $val->update_date;
					$post['item_tax_type']      = $val->item_tax_type;
					$post['total_price']        = $val->total_price;

					$check_res = $this->webservice_comman_model->check_by(array('item_id' => $post['item_id']),'tbl_items');
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_items', array('item_id' => $item_id),$post);
						$u_id_arr[] = array('item_id'=>$post['item_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_items',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('item_id'=>$post['item_id']);
						}
					}
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

    public function addUpdateItemAttachment()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['item_doc_id']      = $val->item_doc_id;
					$post['item_id']          = $val->item_id;
					$post['item_description'] = $val->item_description;
					$post['item_attachment']  = $val->item_attachment;
					$post['status']           = $val->status;
					$post['create_date']     = $val->created_date;
					$post['update_date']      = $val->update_date;
					$post['create_by']         = $val->create_by;
					$post['update_by']        = $val->update_by;
					$post['sync_date_time']   = $val->sync_date_time;

					$check_res = $this->webservice_comman_model->check_by('tbl_item_attachment', array('id' => $id));
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_item_attachment', array('id' => $id));

						$u_id_arr[] = array('id'=>$post['id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_item_attachment',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('id'=>$post['id']);
						}
					}
					
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }
	/*Item end*/

	/*Leads start*/
	public function getLeadList()
	{
		// sync_date_time
		if(isset($_POST['sync_date_time']))
		{
			$user_id = $_POST['user_id'];
			$sync_date_time = $_POST['sync_date_time'];
			if($sync_date_time == 0)
			{
				$res=$this->api_model->getLeadByUserId($user_id,$sync_date_time);
			}
			else
			{
				$res=$this->api_model->getLeadByUserId($user_id ,$sync_date_time);
			}
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}
	public function getLeadClient()
	{
		//add user_id
		//Join with tbl_assign_user, tbl_lead, tbl_lead_client 
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			$user_id = $_POST['user_id'];
			if($sync_date_time == 0)
			{
				
				$res=$this->api_model->getClientLeadByUserId($user_id ,$sync_date_time);
				// $res = $this->webservice_comman_model->getData('tbl_lead_client',NULL , 'multi' , NULL);
			}
			else
			{
				$res=$this->api_model->getClientLeadByUserId($user_id ,$sync_date_time);
				// $res =$this->webservice_comman_model->getData('tbl_lead_client',array('sync_date_time  >' => $sync_date_time) , 'multi', NULL);
			}
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res));
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}
	public function getLeadMeeting()
	{
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			$user_id = $_POST['user_id'];
			if($sync_date_time == 0)
			{
				$res=$this->api_model->getLeadMeetingByUserId($user_id ,$sync_date_time);
				// $res = $this->webservice_comman_model->getData('tbl_lead_meeting',array('create_by' => $user_id) , 'multi' , NULL);
			}
			else
			{
				$res=$this->api_model->getLeadMeetingByUserId($user_id ,$sync_date_time);
				// $res =$this->webservice_comman_model->getData('tbl_lead_meeting',array('sync_date_time >' => $sync_date_time, 'create_by' => $user_id) , 'multi', NULL);
			}
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res));
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}

	public function getShowProfileList()
	{
		$user_id = $_POST['user_id']; 
		$user_detail = $this->api_model->showProfile($user_id);
		if(!empty($user_detail))
		{
			echo json_encode(array("status"=>1, "data"=>$user_detail)); 
		}
		else
		{
			echo json_encode(array("status"=>0 , 'data'=> array())); 
		}
	}

	public function getLeadProduct()
	{
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			$user_id = $_POST['user_id'];
			if($sync_date_time == 0)
			{
				$res=$this->api_model->getLeadProductByUserId($user_id ,$sync_date_time);
			}
			else
			{
				$res=$this->api_model->getLeadProductByUserId($user_id ,$sync_date_time);
			}
			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res));
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}

	public function getLeadSource()
	{
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			if($sync_date_time == 0)
			{
				$res = $this->webservice_comman_model->getData('tbl_lead_source',NULL , 'multi' , NULL);	
			}
			else
			{
				$res = $this->webservice_comman_model->getData('tbl_lead_source', array('sync_date_time  >' => $sync_date_time) , 'multi', NULL);
			}

			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}

	public function getLeadstatus()
	{
		if(isset($_POST['sync_date_time']))
		{
			$sync_date_time = $_POST['sync_date_time'];
			if($sync_date_time == 0)
			{
				$res = $this->webservice_comman_model->getData('tbl_lead_status',NULL , 'multi' , NULL);	
			}
			else
			{
				$res = $this->webservice_comman_model->getData('tbl_lead_status', array('sync_date_time >' => $sync_date_time) , 'multi', NULL);
			}

			if(!empty($res))
			{
				echo json_encode(array("status"=>1, "data"=>$res)); 
			}
			else
			{
				echo json_encode(array("status"=>0, "data"=>array())); 
			}
		}
	}


	public function uploadFile()
	{//*************
		if(!empty($_FILES['attachment_online']['name']))
		{
			//echo $_FILES['attachment_online']['name']; die();
			$name 		= 'attachment_online';
	      	$imagePath 	= 'webroot/upload/expenses/';
	       	$temp 		= explode(".",$_FILES['attachment_online']['name']);
			$extension 	= end($temp);
			$filenew 	=  str_replace($_FILES['attachment_online']['name'],$name,$_FILES['attachment_online']['name']).'_'.time().''. "." .$extension;  		
			$config['file_name'] 	= $filenew;
			$config['upload_path'] 	= $imagePath;
			$this->upload->initialize($config);
			$this->upload->set_allowed_types('*');
			$this->upload->set_filename($config['upload_path'],$filenew);
			
			if(!$this->upload->do_upload('attachment_online'))
			{
				$data = array('msg' => $this->upload->display_errors());
			}
			else 
			{ 
				$data = $this->upload->data();	
			
				if(!empty($data['file_name']))
				{
					$data['file_name']='webroot/upload/expenses/'.$data['file_name'];
				 	echo json_encode(array("status"=>"1", "data"=>array($data))); 
				}
				else
				{
					echo json_encode(array("status"=>"0", "data"=>array())); 
				}
			}
		}	
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
	}

	public function addUpdateLeadData()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		// print_r($st_data);die();
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['lead_id']        	= $val->lead_id;
					$post['lead_name']        	= $val->lead_name;
					$post['organization']       = $val->organization;
					$post['lead_status_id']     = $val->lead_status_id;
					$post['lead_source_id']     = $val->lead_source_id;
					$post['industry']         	= $val->industry;
					// $post['salutation']        	= $val->salutation; **
					// $post['name']        		= $val->name; **
					$post['address']        	= $val->address;
					$post['country']        	= $val->country;
					$post['state']        		= $val->state;
					$post['zip_code']        	= $val->zip_code;
					$post['city']        		= $val->city;
					$post['title']        		= $val->title;
					// $post['email']        		= $val->email; **
					$post['sec_email']        	= $val->sec_email;
					// $post['designation']        = $val->designation; **
					$post['email_opt_out']     	= $val->email_opt_out;
					// $post['mobile_number']     	= $val->mobile_number; **
					$post['phone_number']      	= $val->phone_number;
					$post['facebook']        	= $val->facebook;
					$post['notes']        		= $val->notes;
					$post['skype']        		= $val->skype;
					$post['twitter']      	  	= $val->twitter;
					$post['annual_revenue']     = $val->annual_revenue;
					$post['secondary_email']    = $val->secondary_email;
					$post['stages']      		= $val->stages;
					$post['website']        	= $val->website;
					$post['current_status']     = $val->current_status;
					$post['lead_assign_status'] = $val->lead_assign_status;
					$post['lead_status']        = $val->lead_status;
					$post['create_by']        	= $val->create_by;
					$post['update_by']        	= $val->update_by;
					$post['create_date']        = $val->create_date;
					$post['update_date']        = $val->update_date;
					$post['sync_date_time']     = $val->sync_date_time;
					$post['opportunity_status'] = $val->opportunity_status;
					$post['opportunities_state_reason_id'] 	= $val->opportunities_state_reason_id;
					$post['close_date']        	= $val->close_date;
					$post['expected_revenue']   = $val->expected_revenue;
					$post['new_link']        	= $val->new_link;
					$post['contact_name']       = $val->contact_name;
					$post['permission']        	= $val->permission;
					$post['next_action']        = $val->next_action;
					$post['next_action_date']   = $val->next_action_date;
					$post['attachment']        	= $val->attachment;
					$post['lead_lat']        	= $val->lead_lat;
					$post['lead_long']        	= $val->lead_long;
					$post['location_taken_by']  = $val->location_taken_by;
					$post['location_approved']  =  $val->location_approved;
					$post['location_taken_date_time']  = $val->location_taken_date_time;
					$post['probability']        = $val->probability;
					$post['lead_category_id']   = $val->lead_category_id;
					$post['user_all_level']     = $val->user_all_level;

					$check_res = $this->webservice_comman_model->check_by(array('lead_id' => $post['lead_id'] ) ,'tbl_leads',$post['create_by']);
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_leads', array('lead_id' => $post['lead_id']) , $post);
						$u_id_arr[] = array('lead_id'=>$post['lead_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_leads', $post);
						$as_post['assign_id'] 	= $this->generate_id();
						$as_post['lead_id']		= $val->lead_id;
						$as_post['user_id']		= $val->user_id;
						$as_post['assign_date_time'] =  date('Y-m-d H:i:s');
						$as_post['assign_by'] 		=	$val->user_id;
						$this->webservice_comman_model->addData('tbl_assign_user', $as_post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('lead_id'=>$post['lead_id']);
						}
					}
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

    public function addUpdateLeadClient()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		// print_r($st_data->data);die();
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['lead_client_id']     = $val->lead_client_id;
					$post['lead_id']        	= $val->lead_id;
					$post['salutation']        	= $val->salutation;
					$post['name']        		= $val->name;
					$post['email']        		= $val->email;
					$post['designation']        = $val->designation;
					$post['mobile_number']      = $val->mobile_number;
					$post['email_status']       = $val->email_status;
					$post['create_date']        = $val->create_date;
					$post['update_date']        = $val->update_date;
					$post['sync_date_time']     = $val->sync_date_time;
					$post['create_by']          = $val->create_by;
					$post['update_by']          = $val->update_by;
					$post['user_all_level']     = $val->user_all_level;
					

					// $check_res = $this->lead_model->checkData($post['lead_status_id']);
					$check_res = $this->webservice_comman_model->check_by(array('lead_client_id' => $post['lead_client_id']),'tbl_lead_client',$post['create_by']);
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_lead_client', array('lead_client_id' => $post['lead_client_id']),$post);
						$u_id_arr[] = array('lead_client_id'=>$post['lead_client_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_lead_client',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('lead_client_id'=>$post['lead_client_id']);
						}
					}
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

    public function addUpdateLeadMeetingData()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['lm_id']               = $val->lm_id;
					$post['lead_id']             = $val->lead_id;
					$post['client_id']           = $val->client_id;
					$post['lm_date']             = $val->lm_date;
					$post['lm_time']             = $val->lm_time;
					$post['lm_start_time']       = $val->lm_start_time;
					$post['lm_end_time']         = $val->lm_end_time;
					$post['lm_start_lat']        = $val->lm_start_lat;
					$post['lm_start_lng']        = $val->lm_start_lng;
					$post['lm_end_lat']          = $val->lm_end_lat;
					$post['lm_end_lng']          = $val->lm_end_lng;
					$post['lm_notes']            = $val->lm_notes;
					$post['lm_cancel_reason']    = $val->lm_cancel_reason;
					$post['lm_cancel_status']    = $val->lm_cancel_status;
					$post['lm_response_lavel']   = $val->lm_response_lavel;
					$post['lm_shedule_status']   = $val->lm_shedule_status;
					$post['lm_to_do_list']       = $val->lm_to_do_list;
					$post['lm_next_meeting_date']= $val->lm_next_meeting_date;
					$post['lm_next_meeting_time']= $val->lm_next_meeting_time;
					$post['lm_lead_state']       = $val->lm_lead_state;
					$post['status']              = $val->status;
					$post['create_date']         = $val->create_date;
					$post['update_date']         = $val->update_date;
					$post['create_by']           = $val->create_by;
					$post['update_by']           = $val->update_by;
					$post['sync_date_time`']     = $val->sync_date_time;
					$post['user_all_level`']     = $val->user_all_level;
                    /* new column end */
					// $check_res = $this->lead_model->checkData($post['id']);
					$check_res = $this->webservice_comman_model->check_by(array('lm_id' => $post['lm_id']),'tbl_lead_meeting',$post['create_by']);
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_lead_meeting', array('lm_id' => $post['lm_id']),$post);
						$u_id_arr[] = array('lm_id'=>$post['lm_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_lead_meeting',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('lm_id'=>$post['lm_id']);
						}
					}
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

    public function addUpdateLeadProduct()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		// print_r($st_data->data);die();
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{ 
					$post['lead_id']                    = $val->lead_id;
					$post['lead_process_id']            = $val->lead_process_id;
					$post['opportunity_process_id']     = $val->opportunity_process_id;
					$post['product_type']             	= $val->product_type;
					$post['product_id']            		= $val->product_id;
					$post['product_name']            	= $val->product_name;
					$post['product_price']            	= $val->product_price;
					$post['lead_product_status']        = $val->lead_product_status;
					$post['create_by']                  = $val->create_by;
					$post['update_by']                  = $val->update_by;
					$post['create_date']                = $val->create_date;
					$post['update_date']                = $val->update_date;
					$post['product_desc']              	= $val->product_desc;
					$post['lead_product_id']            = $val->lead_product_id;
					$post['sync_date_time`']            = $val->sync_date_time;
					$post['user_all_level']     		= $val->user_all_level;

					$check_res = $this->webservice_comman_model->check_by(array('lead_product_id' => $post['lead_product_id']) ,'tbl_lead_product',$post['create_by']);
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_lead_product', array('lead_product_id' => $post['lead_product_id']),$post);
						$u_id_arr[] = array('lead_product_id'=>$post['lead_product_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_lead_product',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('lead_product_id'=>$post['lead_product_id']);
						} 
					}
					
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

	public function addUpdateLiveLocation()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		// print_r($st_data->data);die();
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['location_id']    		= $val->location_id;
					$post['user_id']      			= $val->user_id;
					$post['location_lat']   		= $val->location_lat;
					$post['location_long']  		= $val->location_long;
					$post['location_date_time'] 	= $val->location_date_time;
					$post['location_status']    	= $val->location_status;
					$post['location_flag_date_time']= $val->location_flag_date_time;
					$check_res = $this->webservice_comman_model->check_by(array('location_id' => $post['location_id']) ,'tbl_live_location',null);
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_live_location', array('location_id' => $post['location_id']),$post);
						$u_id_arr[] = array('location_id'=>$post['location_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_live_location',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('location_id'=>$post['location_id']);
						} 
					}
					
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }


     public function addUpdateLeadSource()
    {
    	$rawData = file_get_contents("php://input");
		$st_data = json_decode($rawData);
		// print_r($st_data->data);die();
		$u_id_arr = array();
		if(!empty($st_data->data))
		{
			foreach ($st_data->data as $val)
			{
				if(!empty($val))
				{
					$post['lead_source']       	      	= $val->lead_source;
					$post['lead_source_status']       	= $val->lead_source_status;
					$post['create_date'] 				= $val->create_date;
					$post['update_date']  				= $val->update_date;
					$post['sync_date_time']       	  	= $val->sync_date_time;

					$check_res = $this->webservice_comman_model->check_by('tbl_lead_source', array('lead_source_id' => $lead_source_id));
					if(!empty($check_res))
					{
						$this->webservice_comman_model->updateData('tbl_lead_source', array('lead_source_id' => $lead_source_id));

						$u_id_arr[] = array('lead_source_id'=>$post['lead_source_id']);
					}
					else
					{
						$lead_id_x = $this->webservice_comman_model->addData('tbl_lead_source',$post);
						if($lead_id_x)
						{
							$u_id_arr[] = array('lead_source_id'=>$post['lead_source_id']);
						}
					}
					
				}
			}
		}
		if(!empty($u_id_arr))
		{
			echo json_encode(array("status"=>1, "data"=>$u_id_arr)); 
		}
		else
		{
			echo json_encode(array("status"=>0, "data"=>array())); 
		}
    }

	/*Leads end*/

 	public function checkLicence()
    {
		// $licence_key 	= 	'362cvbnhgf4kk1b';
		// $device_id 		=  	'00vdffghbc921840';
		// $system_Ip 		=  	'192.168.1.2';
    
		$licence_key 	=  $_POST['licence_key'];
		$device_id 		=  $_POST['device_id'];
		$system_Ip 		=  $_POST['system_Ip'];

		$check_licence  = $this->webservice_comman_model->getData('tbl_app_activation' , array('licence_key'=>$licence_key));


       //print_r($check_licence); 
		if(!empty($check_licence))
		{
			$check_device_id = $this->webservice_comman_model->check_by(array('device_id'=> '','licence_key'=>$licence_key),'tbl_app_activation');
			if(empty($check_device_id))
			{
				$checkdouble_device_id = $this->webservice_comman_model->check_by(array('device_id'=> $device_id,'licence_key'=>$licence_key),'tbl_app_activation');
				if(!empty($checkdouble_device_id))
				{
					$prefix_url = $checkdouble_device_id->prefix_url;
					$current_date = date('Y-m-d');
					if($current_date <= $checkdouble_device_id->deactivation_date && $current_date >= $checkdouble_device_id->activation_date)
					{
						echo json_encode(array("status"=>1, "prefix_url"=> $prefix_url));
					}
					else
					{
						echo json_encode(array("status"=>0, "Licence Expaired"));
					}
				}
				else
				{
					echo json_encode(array("status"=>0, "Device on Licence is not there"));
				}
			}
			else
			{
				$valid_day = $check_licence[0]->valid_day;
				$post['device_id'] = $device_id;
				$post['activation_date'] = date('Y-m-d');
				$post['deactivation_date'] = date('Y-m-d', strtotime("+$valid_day day", strtotime($post['activation_date'])));
				$add_activation = $this->webservice_comman_model->updateData('tbl_app_activation', array('id'=>$check_licence[0]->id), $post);

				$checkdouble_device_id = $this->webservice_comman_model->check_by(array('device_id'=> $device_id,'licence_key'=>$licence_key),'tbl_app_activation');
				$prefix_url = $checkdouble_device_id->prefix_url;
				echo json_encode(array("status"=>1, "prefix_url"=> $prefix_url));
			}
		}
		else
		{
			echo json_encode(array("status"=>0, "Licence Key is not valid"));
		}
    }

    public function getIndustry()
    {
        
            $res =  $this->webservice_comman_model->getData('tbl_industry' , array('industry_status'=>1), 'multi');

            if(!empty($res))
            {
                echo json_encode(array("status"=>1, "data"=>$res));
            }
            else
            {
                echo json_encode(array("status"=>0, "data"=>array()));
            }
        
    }
	/* End of file */
}
?>