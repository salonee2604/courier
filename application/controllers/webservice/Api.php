<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller 
{
    
   
    function __construct()
    {
        

        parent::__construct();
        $this->load->model('webservice/Api_model'); 
        $this->load->model('webservice/Comman_model');
        $this->load->model('admin/Driver_model');

    }   

    public function login()
    { 

        
        $email            = $_POST['email'];
        $driver_details   = $this->Api_model->getDriverByEmailId($email);
        if(empty($driver_details->email))
        {
            echo json_encode(array("status"=>0 , 'data'=> array(),'message'=>"Email Not Exist"));
        }
        else
        {
          
          $post['email']    = $_POST['email'];
          $post['password'] = md5($_POST['password']);
          $driver_details   = $this->Comman_model->getData('driver',$post);
          if(!empty($driver_details[0]->email))
          {   
              $this->session->set_userdata("driver_id",$driver_details[0]->driver_id);
              echo json_encode(array("status"=>1,'data'=>$driver_details,'message'=>"Login SuccessFully"));
          }
          else
          {
              echo json_encode(array("status"=>0 , 'data'=> array(),'message'=>"Email And Password Not Matched"));
          }

        }
       
    }
    
    public function driverRegistration()
    {
        
        
        $post['first_name']    = $_POST['first_name'];
        $post['surname']       = $_POST['surname'];
        $post['mobile_number'] = $_POST['mobile_number'];
        $post['email']         = $_POST['email'];
        $poste['email']        = $_POST['email'];
        $post['password']      = md5($_POST['password']);
        $post['address']       = $_POST['address'];
        $post['city']          = $_POST['city'];
        $post['post_code']     = $_POST['post_code'];
        $post['create_date']   = date('Y-m-d H:i:s');
        $post['update_date']   = date('Y-m-d H:i:s');
        $driver_details   = $this->Comman_model->getData('driver',$poste);
        if(!empty($driver_details))
        {
          echo json_encode(array("status"=>0 , 'data'=> 'Email Already Exist'));

        }
        else
        {
           
            $driver_id = $this->Comman_model->AddData($post);
            if(!empty($driver_id))
            {  
                
                $post2['driver_id']        = $driver_id;
                $driver_arr                = $this->Comman_model->getData('driver',$post2);
                echo json_encode(array("status"=>1, "data"=>$driver_arr,"message"=>"Driver Added Successflly")); 
            }
            else
            {   
                
              echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Driver Not Added")); 
                
            } 
        }
        
    }
    
    public function getTripList_backup()
    {
        
        $driver_id          = $_POST['driver_id'];
        $restaurant_id      = $_POST['restaurant_id'];
        $res                =  $this->Comman_model->getAllTripData($driver_id,$restaurant_id);
        if(!empty($res))
        {  
          echo json_encode(array("status"=>1, "data"=>$res,"message"=>"Get Trip List")); 

        }
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found")); 
        }  
        
    }

    public function getTripList()
    {
      
       
        $driver_id          = $_POST['driver_id'];
        $restaurant_id      = $_POST['restaurant_id'];
        $res                =  $this->Comman_model->getAllTripData($driver_id,$restaurant_id);
        $trip_details       = array();
        foreach ($res as $value) 
        {
          
            $trip_id              = $value->trip_id; 
            $parcel_id            = $value->parcel_id;
            $parcel_id_array      = explode(",", $parcel_id);
            if(!empty($parcel_id))
            {

                foreach ($parcel_id_array  as $p_id) 
                {
                   
                     $parcel_details   =  $this->Comman_model->getParcelByTripId($p_id);
                     if(!empty($parcel_details))
                     {
                        $trip_details[]   =  $this->Comman_model->getTripIdByParcelId($parcel_details->parcel_id);
                     }
                     
                }   
            }
            else
            {
               $trip_details[]   =  $this->Comman_model->tripByTripId($trip_id);  
            }
            


            
        }
       
        $trip_details_array =    $trip_details;
        $final  = array();
        foreach ($trip_details_array as $current) {
        if ( ! in_array($current, $final)) 
        {
         $final[] = $current;
        }
      }
        if(!empty($final))
        {  
          echo json_encode(array("status"=>1, "data"=>$final,"message"=>"Get Trip List")); 

        }
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found")); 
        }  
        

           
 


       
   }
    
    public function getParceList()
    {
        
        $trip_id                    =  $_POST['trip_id'];
        $alltrip                    =  $this->Comman_model->getTripById($trip_id);
        $parcel_id                  =  $alltrip->parcel_id;
        $parcel_id_array            =  explode(',',$parcel_id);
        $res = array();
        foreach($parcel_id_array as $parcel_id)
        {

          //echo $parcel_id;
           $result =  $this->Comman_model->getAllParcelDataByParcelid($parcel_id);
           if(!empty($result))
           {
             $res[]  = $result;
           }

          
 
                  
            //print_r($res);
        }

                  
        if(!empty($res))
        {
            echo json_encode(array("status"=>1, "data"=>$res,"message"=>"Parcel Avaliable")); 
        }
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Parcel Not Avaliable")); 
        }  
        
    }
    
    public function profileUpdate()
    {
        
        $driver_id             = $_POST['driver_id'];
        $post['first_name']    = $_POST['first_name'];
        $post['surname']       = $_POST['surname'];
        $post['mobile_number'] = $_POST['mobile_number'];
        $post['email']         = $_POST['email'];
        $email                 = $_POST['email'];
        // $post['password']      = md5($_POST['password']);
        $post['address']       = $_POST['address'];
        $post['city']          = $_POST['city'];
        $post['post_code']     = $_POST['post_code'];
        $post['create_date']   = date('Y-m-d H:i:s');
        $post['update_date']   = date('Y-m-d H:i:s');
        $driver_details   = $this->Comman_model->getAlldriverEmail($email,$driver_id);
        if(!empty($driver_details))
        {
          echo json_encode(array("status"=>0 , 'data'=> 'Email Already Exist','message'=>"Email Already Exist"));

        }
        else
        {
           
            $driver_id_update = $this->Comman_model->updateDriver($post,$driver_id);
            if(!empty($driver_id_update))
            {  
                
                $post2['driver_id']        = $driver_id;
                $driver_arr                = $this->Comman_model->getData('driver',$post2);
                echo json_encode(array("status"=>1, "data"=>$driver_arr,"message"=>"Profile Updated Successfully")); 
            }
            else
            {   
                
              echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Profile Not Updated")); 
                
            } 
        }
        
        
    }
    
    public function assignDriver()
    {
       $trip_id                     =    $_POST['trip_id'];
       $post['driver_id']           =    $_POST['driver_id'];
       $post['trip_assign_status']  =    2;
       $trip_id_details             =  $this->Comman_model->updateTrip($post,$trip_id);
       if(!empty($trip_id_details))
            {  
                echo json_encode(array("status"=>1, "data"=>array(),'message'=>"driver assign successfully")); 
            }
            else
            {     
              echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Data Not Found")); 
                
            } 
       
    }

    public function parcelDelivered()
    {
        
        
        
        if(!empty($_FILES['image']['name']))
        {
            
           
            
            $name       = 'image';
            $imagePath  = 'image/parcel/';
            $temp       = explode(".",$_FILES['image']['name']);
            $extension  = end($temp);
            $filenew    =  str_replace($_FILES['image']['name'],$name,$_FILES['image']['name']).'_'.time().''. "." .$extension;         
            $config['file_name']    = $filenew;
            $config['upload_path']  = $imagePath;
            $this->upload->initialize($config);
            $this->upload->set_allowed_types('*');
            $this->upload->set_filename($config['upload_path'],$filenew);
            if(!$this->upload->do_upload('image'))
            {
                $data = array('messageg' => $this->upload->display_errors());
                
                echo $data;
            }
            else 
            { 
                $data = $this->upload->data();  
            
                if(!empty($data['file_name']))
                {
                    $data['file_name']='image/parcel/'.$data['file_name'];
                 
                }
            
            }
        }
        else
        {
           echo json_encode(array("status"=>0, "data"=>"Image Not avalible"));   
        }
       $parcel_id                     =    $_POST['parcel_id'];
       $post['note']                  =    $_POST['note'];
       $signature_image               =    $data;
       $post['signature_image']       =  base_url().$data['file_name'];
       $post['status']  =    2;
       $trip_id_details = $this->Comman_model->updateParcel($post,$parcel_id);
       if(!empty($trip_id_details))
        {  
                echo json_encode(array("status"=>1, "data"=>array(),'message'=>"parcelDelivered Successfully")); 
        }
        else
        {     
              echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Parcel Not Delivered")); 
                
        } 
    }
    
    public function getDriverById()
    {
       
        $driver_id        =  $_POST['driver_id'];
        $driver_details   = $this->Comman_model->getAlldriverByDriverId($driver_id);
        if(!empty($driver_details))
        {  
        echo json_encode(array("status"=>1, "data"=>$driver_details,'message'=>"Driver List Successfully")); 
        }
        else
        {     
        echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found")); 
        
        }          
                    
    }
    
    public function addUpdateDataLiveLocation()
    {
       

       $latitude                   = $_POST['location_lat'];
       $longitude                  = $_POST['location_long'];
       $apiKey = 'AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw';
       $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=true_or_false&key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw');
       $output = json_decode($geocodeFromLatLong);
       $status = $output->status;
       //Get address from json data
       $address = ($status=="OK")?$output->results[1]->formatted_address:'';
       $city    = ($status=="OK")?$output->results[0]->address_components[4]->long_name:'';
       $zip_code    = ($status=="OK")?$output->results[0]->address_components[6]->long_name:'';
      
       $post['driver_id']          = $_POST['driver_id'];
       $post['location_lat']       = $_POST['location_lat'];
       $post['location_long']      = $_POST['location_long'];
       $post['parcel_status']      = $_POST['parcel_status'];
       $post['trip_id']            = $_POST['trip_id'];
       $post['live_address']       = $address;
       $post['live_city']          = $city;
       $post['live_zip_code']      = $zip_code;
       $post['location_date_time'] = date('y-m-d h:i:s');
       $check_res = $this->Api_model->checkData($post['driver_id']);
        if(!empty($check_res))
        {
        
           $id=$this->Api_model->updateData($post);
            $u_id_arr[] = array('location_id'=>$check_res['0']->location_id);
        }
        else
        {
          
            $location_id_x = $this->Api_model->addData($post);
            if($location_id_x)
            {
                $u_id_arr[] = array('location_id'=>$location_id_x);
            }
        }
        if(!empty($u_id_arr))
        {
            echo json_encode(array("status"=>1, "data"=>$u_id_arr,"message"=>"Live Location Updated Successfully")); 
        }
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Live Location Not Updated")); 
        }

    }
     // lat long to address
    public function getAddress($latitude,$longitude)
    {
        
        $apiKey = 'AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw'; 
        if(!empty($latitude) && !empty($longitude))
        {

            //Send request and receive json data by address
          $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=true_or_false&key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw');
            // $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?&key='.$apiKey.trim($latitude).','.trim($longitude).'&sensor=false');
           
            $output = json_decode($geocodeFromLatLong);
           
            

            $status = $output->status;

            //Get address from json data
            $address = ($status=="OK")?$output->results[1]->formatted_address:'';
            $city    = ($status=="OK")?$output->results[0]->long_name:'';
            //Return address of the given latitude and longitude
            if(!empty($address)){
                echo $address;
                
            }else{
                echo false;
            }
        }else{
            echo false;   
        }
    }
    
    public function getParcelDropLocationByDriverId()
    {
       $driver_id          =  $_POST['driver_id'];
       $trip_id            =  $_POST['trip_id'];
       $trip_details       =  $this->Api_model->getParcelDropLocationByDriverId($driver_id,$trip_id);
       $restaurant_id      = $trip_details['0']->restaurant_id;
       $restaurant_details = $this->Api_model->getRestaurantsById($restaurant_id);
       $latitude           = $restaurant_details->latitude;
       $longitude          = $restaurant_details->longitude;
       $parcel_id_array =array();
       foreach($trip_details as $value)
        {
           $parcel_id                  = $value->parcel_id;
           $parcel_id_array[]           =  explode(',',$parcel_id);
           
            
        }
        $res = array();
         foreach($parcel_id_array as $valueparcelid)
         {
            $i = 0;
            foreach($valueparcelid as $parcel_id )
            {
                   
                   
                   $res[]                =  $this->Comman_model->getAllParcelDataByParcelid($parcel_id);
                   $drop_latitude        = $res[$i]->drop_latitude;
                   $drop_longitude        = $res[$i]->drop_longitude;
                   


              $i++;  
            }
                         
      
     
        }
        $res1        = $res;
        $last_array  = array_pop($res1);
        $pickup_latitude             = $last_array->drop_latitude;
        $pickup_longitude            = $last_array->drop_longitude;
        if(!empty($parcel_id_array))
        {  
           echo json_encode(array("status"=>1, "data"=>$res,'pickup_latitude'=>$pickup_latitude,'pickup_longitude'=>$pickup_longitude,'drop_latitude'=>$latitude,'drop_longitude'=>$longitude,"message"=>"Get Parcel successfully")); 
        }
        else
        {     
          echo json_encode(array("status"=>0, "data"=>array(),'message'=>"No Data Found")); 
        
        }   
        
    }
    
    
  public function addTrip()
  {
        
      
   
       $post['name']               = $_POST['name'];
       $post['restaurant_id']      = $_POST['restaurant_id'];
       $post['brand']              = '';
       $post['driver_id']          = $_POST['driver_id'];
       $post['parcel_id']          = '';
       $post['image']              = '';
       $post['trip_assign_status'] = 1;
       $post['created_date']       = date('Y-m-d H:i:s');
       $post['updated_date']       = date('Y-m-d H:i:s'); 
       $trip_id                    = $this->Comman_model->AddTrip($post);
       if(!empty($trip_id))
        {
            echo json_encode(array("status"=>1, "data"=>$trip_id,"message"=>"Trip Added Successfully")); 
        }
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Trip Not Added")); 
        }
  }
    
  public function getAllRestaurantList()
  {
        
        $resturent_name     = $_POST['search_key'];
        $offeset            = $_POST['start_value'];
        if(!empty($resturent_name))
        {
            
            $restaurantsList    = $this->Api_model->getAllRestaurantListByresurentName($resturent_name,$offeset);
            if(!empty($restaurantsList))
            {  
               echo json_encode(array("status"=>1, "data"=>$restaurantsList,"message"=>"All Resturent List")); 
            }
            else
            {     
               echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Data Not Found")); 
            
            }   
        }
        else
        {
           
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Search Key Not avaliable")); 
        }
       
       
        
  }
    
  public function parcelListByRestaurantId()
  {
        $restaurant_id      = $_POST['restaurant_id'];
        $parcelList         = $this->Api_model->parcelListByRestaurantId($restaurant_id);
        if(!empty($parcelList))
        {  
           echo json_encode(array("status"=>1, "data"=>$parcelList,"message"=>"Parcel List")); 
        }
        else
        {     
           echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Data Not Found")); 
        
        }   
        
  }
    
  public function addParcel()
  {
        


       
        $post['parcel_information'] = $_POST['parcel_information'];
        $post['parcel_added_by']    = 'app';
        $post['restaurant_id']      = $_POST['restaurant_id'];
        $restaurant_id              = $_POST['restaurant_id'];
        $pic_up_location_input            = $_POST['pic_up_location'];
        if(!empty($pic_up_location_input))
        {
           $post['pic_up_location']        = $pic_up_location_input;
           $pic_up_location                = $pic_up_location_input;
           $post['pickup_location_status'] = 1;


        }
        else
        {
            $restaurant_details         = $this->Api_model->getRestaurantById($restaurant_id);
            $post['pic_up_location']    = $restaurant_details->address;
            $pic_up_location            = $restaurant_details->address;
            $post['pickup_location_status'] = 0;

        }
        $post['drop_location']      = $_POST['drop_location'];
        $drop_location              = $_POST['drop_location'];
        // api map //
                        $address1 = $pic_up_location;
                        $address2 = $drop_location;

                        $apiKey = 'AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw'; // Google maps now requires an API key.
                        // pickup location //

                        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address1).'&sensor=false&key='.$apiKey);

                        $geo = json_decode($geo, true); // Convert the JSON to an array

                        $pickup_location = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address1).'&sensor=false&key='.$apiKey);

                        $pickup_location = json_decode($pickup_location, true); // Convert the JSON to an array

                        if (isset($geo['status']) && ($geo['status'] == 'OK')) 
                        {
                            $pickup_latitude =  $pickup_location['results'][0]['geometry']['location']['lat']; 

                            $pickup_longitude = $pickup_location['results'][0]['geometry']['location']['lng']; // Longitude
                            
                             $post['pickup_latitude']    = $pickup_latitude;
                             $post['pickup_longitude']   = $pickup_longitude;

                        }
                        // Get JSON results from this request
                        // get lat long drop location 

                        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address2).'&sensor=false&key='.$apiKey);

                        $geo = json_decode($geo, true); // Convert the JSON to an array

                        $drop_location = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address2).'&sensor=false&key='.$apiKey);

                        $drop_location = json_decode($drop_location, true); // Convert the JSON to an array

                        if (isset($geo['status']) && ($geo['status'] == 'OK')) 
                        {
                           $drop_latitude = $drop_location['results'][0]['geometry']['location']['lat']; 

                            $drop_longitude = $drop_location['results'][0]['geometry']['location']['lng']; // Longitude
                            
                            $post['drop_latitude']     = $drop_latitude;

                            $post['drop_longitude']    = $drop_longitude;

                        }
                        

                        
        // api close//

        
        $post['created_date']                = date('Y-m-d H:i:s');
        $post['updated_date']                = date('Y-m-d H:i:s'); 
        $parcel_id                           =  $this->Api_model->addParcel($post);
        $post_customer['first_name']         =  $_POST['first_name'];
        $post_customer['last_name']          =  $_POST['last_name'];
        $post_customer['customer_number']    =  $_POST['customer_number'];
        $post_customer['customer_address']   =  $_POST['drop_location'];
        $post_customer['customer_country']   =  $_POST['customer_country'];
        $post_customer['customer_state']     =  $_POST['customer_state'];
        $post_customer['customer_zip_code']  =  $_POST['customer_zip_code'];
        $post_customer['customer_city']      =  $_POST['customer_city'];
        $post_customer['created_date']      = date('y-m-d H:i:s');
        $post_customer['updated_date']      = date('y-m-d H:i:s');
        $post_customer['parcel_id']         = $parcel_id;
        $customer_id                        =  $this->Api_model->addCustomer($post_customer);
        $trip_id                           = $_POST['trip_id'];
        $trip_deatils                       = $this->Api_model->getTripById  ($trip_id);
        if(!empty($trip_deatils->parcel_id))
        {
           $parcel_id_all  = $trip_deatils->parcel_id.','.$parcel_id;   
        }
        else
        {
            $parcel_id_all = $parcel_id;
        }
                                           
         $post_trip['parcel_id']            = $parcel_id_all;
         $trip_id                           = $this->Api_model->updateParcelIdByTrip($post_trip,$trip_id);
        if(!empty($trip_id))
        {  
           echo json_encode(array("status"=>1, "data"=>array(),'message'=>"Parcel Added Successfully")); 
        }
        else
        {     
            echo json_encode(array("status"=>0, "data"=>array(),'message'=>"Parcel Not Added")); 
            
        } 
    
    


  }
  public function forgetPassword()
  {
       
           $email           =   $_POST['email'];
           $driverDetails   =   $this->Api_model->getDriverByEmailId($email);
           $uid   =   $driverDetails->driver_id;    
           $to    =   $email; 
           $sub   =   'verify Our Account';
           $key   =   rand(1000,9999);
           $udata =
           array(                          
            'emailverify'=>$key 
            );
            $data = array( 
                        'vrfn_code'  => $key                       
                        );
            $this->db->where('email',$email);
            $this->db->update('driver', $data);
            $message = "<h3>Hello Dear,</h3>          
            <p>Please use this code <h4><b>".$key."</b></h4></p>
            <p>Click on the button below the reset your password.</p>
            <a href='".base_url()."Home/Driverresetpass/$uid'>Click Here</a>
            Use This Otp Code $key";    
            $this->email($to,$sub,$message); 
            if(!empty($driverDetails->driver_id))
            {  
               echo json_encode(array("status"=>1, "data"=>array(),'message'=>"Please Check Email")); 
            }
            else
            {     
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Parcer Not Added")); 
            
            }   
                    
  }
    
  public function changePassword()
  {
        
        $driver_id                    =    $_POST['driver_id'];
        $old_password                 =    md5($_POST['old_password']);
        $new_password                 =    md5($_POST['new_password']);
        $driver_details               =    $this->Api_model->getDriverListByDriverId($driver_id,$old_password);
        if(!empty($driver_details))
        {
            $driver_id     =      $driver_details->driver_id;
            $data = array( 
                        'password'  => $new_password                       
                        );
            $this->db->where('driver_id',$driver_id);
            $this->db->update('driver', $data);
            echo json_encode(array("status"=>1, "data"=>array(),"message"=>"Password Updated Successfully"));  
            
        }
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Password Does not Matched"));  
            
        }
        

       
        
  }
    
  public function getCountryList()
  {
        
        
        $countryList               =    $this->Api_model->getCountryList();
        if(!empty($countryList))
        {  
           echo json_encode(array("status"=>1, "data"=>$countryList,"message"=>"Get Country List")); 
        }
        else
        {     
            echo json_encode(array("status"=>0, "data"=>"No country")); 
            
        }   
        
          
  }
  public function getStateList()
  {
        
        $country_id                 = $_POST['country_id'];
        $state_list               =    $this->Api_model->getStateList($country_id);
        if(!empty($state_list))
        {  
           echo json_encode(array("status"=>1, "data"=>$state_list,'message'=>"Get state List Successfully")); 
        }
        else
        {     
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found")); 
            
        }   
        
          
  }
    
  public function getCityList()
  {
        
        $region_id                 = $_POST['region_id'];
        $cityList                  = $this->Api_model->getCityList($region_id);
        if(!empty($cityList))
        {  
           echo json_encode(array("status"=>1, "data"=>$cityList,'message'=>"City List successfully")); 
        }
        else
        {     
            echo json_encode(array("status"=>0, "data"=>array(),'message'=>"No Data Found")); 
            
        }   
        
          
  }
    
  public function uploadFile()
  {
       
        if(!empty($_FILES['image']['name']))
        {
            
           
            
            $name       = 'image';
            $imagePath  = 'image/parcel/';
            $temp       = explode(".",$_FILES['image']['name']);
            $extension  = end($temp);
            $filenew    =  str_replace($_FILES['image']['name'],$name,$_FILES['image']['name']).'_'.time().''. "." .$extension;         
            $config['file_name']    = $filenew;
            $config['upload_path']  = $imagePath;
            $this->upload->initialize($config);
        
            $this->upload->set_allowed_types('*');
            $this->upload->set_filename($config['upload_path'],$filenew);
            if(!$this->upload->do_upload('image'))
            {
                $data = array('messageg' => $this->upload->display_errors());
            }
            else 
            { 
                $data = $this->upload->data();  
            
                if(!empty($data['file_name']))
                {
                    $data['file_name']='image/parcel/'.$data['file_name'];
                    echo json_encode(array("status"=>"1", "data"=>array($data),"message"=>"Image Uploaded Successfully")); 
                }
                else
                {
                    echo json_encode(array("status"=>"0", "data"=>array(),'message'=>"Data Not Found")); 
                }
            }
        }   
        else
        {
            echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Data NOT Found")); 
        }
  }

   // api connecting driver to resturent

  public function connect_resturent_old()
  {
        
        $driver_id           =  $_POST['driver_id'];
        $restaurant_id       =  $_POST['restaurant_id'];
        $connectivity_number =  $_POST['connectivity_number'];
        $status              =  1;
        if(!empty($driver_id && $restaurant_id && $connectivity_number)) 
        {
             
            $connecting_details = $this->Api_model->getAllConnectingData($restaurant_id,$connectivity_number);
           
            if($connecting_details->connectivity_status == 1)
            {
                
                echo json_encode(array("status"=>2, "data"=>array(),"message"=>"Already Used This Connecting Number "));    
            }
           

            else if(!empty($connecting_details->restaurant_id))
            {
                 
                
                 $post['connecting_driver_id']      =  $_POST['driver_id'];
                 $post['restaurant_id']             =  $_POST['restaurant_id'];
                 $post['connectivity_number']       =  $_POST['connectivity_number'];
                 $post['connectivity_status']       =  1;

                 $post['connectivity_date']         = date("Y-m-d");
                 
                 $updatedata                        =  $this->Api_model->updateConectivity($post);
                 $insertdata                        =  $this->Api_model->insertConectivity($post);
                                                   
                echo json_encode(array("status"=>1, "data"=>array(),"message"=>"driver connected Successfully"));


            }
            else
            {
              
               echo json_encode(array("status"=>3, "data"=>array(),"message"=>"Code Does Not Exist"));
            }
            
        }
       else
       {
           echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found"));
       }
  }

   public function connect_resturent()
  {
      
      $restaurant_id       =  $_POST['restaurant_id'];
      $connectivity_number =  $_POST['connectivity_number'];
      $resturent_details   =  $this->Api_model->getRestaurantsById($restaurant_id);
      $connectivity_number_fetch =  $resturent_details->connectivity_number;
     
    if($connectivity_number == $connectivity_number_fetch)
    {

      
      $connecting_details = $this->Api_model->getAllConnectingData($restaurant_id,$connectivity_number);
      if(!empty($connecting_details[0]->restaurant_id))
      {
        
                 $post['connecting_driver_id']     =  $_POST['driver_id'];
                 $post['restaurant_id']             =  $_POST['restaurant_id'];
                 $post['connectivity_number']       =  $_POST['connectivity_number'];
                 $post['connectivity_status']       =  1;

                 $post['connectivity_date']         = date("Y-m-d");
                 
                 $updatedata                        =  $this->Api_model->updateConectivity($post);
                 $insertdata                        =  $this->Api_model->insertConectivity($post);
                 echo json_encode(array("status"=>1, "data"=>$connecting_details,"message"=>"Driver Connected Successfully"));
      }
      else
      {
         echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found"));
      }
    }
    else
    {
      echo json_encode(array("status"=>0, "data"=>array(),"message"=>"Your Resturent Passcode wrong")); 
    }

      

  }

  public function parcelHistory()
  {
       
        // $driver_id              =        $_POST['driver_id'];
        $trip_id                =        $_POST['trip_id'];
        $trip_details           =        $this->Api_model->getAllTripByDriverIdAndtripId($trip_id);
        $parcel_details     = array();
        foreach ($trip_details as $value) 
        {
                $trip_id            = $value->trip_id;
                $parcel_id          = $value->parcel_id;
                $parcel_id_array    = explode(",", $parcel_id);
                // $parcel_details     = array();
                foreach ($parcel_id_array as $p_id) 
                {
                    $parcel_details[] = $this->Api_model->getAllParcelByParcelId($p_id);  
                }
                


        }
        if(!empty($parcel_details))
        {
            echo json_encode(array("status"=>1, "data"=>$parcel_details,"message"=>"parcel history"));
        }
        else

        {

           echo json_encode(array("status"=>0, "data"=>$parcel_details,"message"=>"No Data Found"));  
        }

  }

  public function getTripHistory()
  {
        $driver_id              =        $_POST['driver_id'];
        $trip_details           =        $this->Api_model->getAllTripByDriverId($driver_id);
         if(!empty($trip_details))
        {
            echo json_encode(array("status"=>1, "data"=>$trip_details,"message"=>"Trip History"));
        }
        else

        {

           echo json_encode(array("status"=>0, "data"=>$trip_details,"message"=>"No Data Found"));  
        }

  }

// trip start//

public function tripStart()
{
    $post['parcel_id']         =  $_POST['parcel_id'];
    $post['trip_start_status'] =  $_POST['trip_start_status'];
    $update_parcel            = $this->Api_model->updateTripStart($post);
    if($update_parcel == 1)
    {

      echo json_encode(array("status"=>1, "data"=>array(),"message"=>"Trip Start Or Stop Successfully"));
    }
    else
    {

      echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found")); 
    }
}

// getTripStartEnd//

public function getTripParcelStatus()
{
    $parcel_id                 =  $_POST['parcel_id'];
    $gettripStartEnd            = $this->Api_model->getTripStartEnd($parcel_id);

    if(!empty($gettripStartEnd))
    {
       echo json_encode(array("status"=>1, "data"=>$gettripStartEnd,"message"=>"Successfully"));
    }
    else
    {

      echo json_encode(array("status"=>0, "data"=>array(),"message"=>"No Data Found")); 
    }
}




  








    
    
    
    
    
    
    
}
?>