<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MY_Controller  
{
    
    
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Login_model');	
	}
	
	/*	Validation Rules */
	 protected $validation_rules = array
        (
        'login' => array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ),
			 array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            )
        ),

		'adminEdit' => array(
            array(
                'field' => 'admin_name',
                'label' => ' admin name',
                'rules' => 'trim|required'
            ),      
      		array(
                'field' => 'admin_phone',
                'label' => 'admin phone',
                'rules' => 'trim|required'
            ),
        )

       
        

		
    );
		
	/* Login */
	public function index()
	{
		
		    
			if(isset($_POST['loginsubmit']))
			{	
				$this->form_validation->set_rules($this->validation_rules['login']);
				if ($this->form_validation->run()) 
				{
				    $email    = $_POST['email'];
					$password = md5($_POST['password']);
					$user_details = $this->Login_model->checkUserLogin($email,$password);
					if(!empty($user_details))
					{

						$this->session->set_userdata('web_admin' , $user_details);
						redirect(base_url().'admin/dashboard');
					}
					else
					{
						$msg = 'Ungültige E-Mail und Passwort';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'admin/login');
					}
				}
				else
				{					
					$this->load->view('admin/login', $this->data);
				}
			}
			else
			{
				// To Load second database and perform query operation.
				$this->load->view('admin/login');
			}
		
    }	  public function smscountmonth()    {        $data = ['smscount' =>0,];	  $this->db->where('smscount!=', 0);	  $this->db->update('restaurant', $data);            }
    public function dashboard()
    {
      
      
      $this->data['driver_count']    =  $this->Login_model->getDriverCount();
      $this->data['parcel_count']    =  $this->Login_model->getParcelCount();
      $this->data['trip_count']      =  $this->Login_model->getTripCount();
      $this->data['get_top_driver']  =  $this->Login_model->getTopDriver();
      $this->show_view_admin('admin/dashbord',$this->data);
        
    }
    
    public function logout() 
	{        
        $this->session->sess_destroy();		
        redirect(base_url().'admin');
    }
    
    
    

}
/* End of file */