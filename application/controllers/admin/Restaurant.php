<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Restaurant extends MY_Controller  
{
    
    
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Restaturant_model');
        $this->load->library('form_validation');
        $this->load->library('Ciqrcode');
        $this->load->library('Paypal_lib');
        $this->load->helper('string');
 



	}
	
	/*	Validation Rules */
	 protected $validation_rules = array
        (
        'restaturantAdd' => array(
           array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            
             array(
                'field' => 'restaurant_name',
                'label' => 'Restaurant Name',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|is_unique[restaurant.email]'
            ),
			array(
                'field' => 'mobile_number',
                'label' => 'Mobile Number',
                'rules' => 'trim|required'
            ),              
            array( 
 				'field' => 'password', 
 				'label' => 'Password',   
 				'rules' => 'trim|required'  
 			),
 			array(  
 				'field' => 'c_password',
 				'label' => 'Confirm Password', 
 				'rules' => 'trim|required|matches[password]'
             ),			
            array(
                'field' => 'surname',
                'label' => 'surname',
                'rules' => 'trim|required'
            ),

            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'city',
                'label' => 'City',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'post_code',
                'label' => 'post code',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'name_holder',
                'label' => 'Name auf der Karte',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'card_no',
                'label' => 'Kartennummer',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'expire_date',
                'label' => 'g���ltig bis',
                'rules' => 'trim|required'
            ),
            	
            array(
                'field' => 'expire_date',
                'label' => 'Expire Date',
                'rules' => 'trim|required'
            )
          
        

                     
                  
        ),
		'restaturantEdit' => array
		(
           array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
             array(
                'field' => 'surname',
                'label' => 'Surname',
                'rules' => 'trim|required'
            ),
             array(
                'field' => 'restaurant_name',
                'label' => 'Restaurant Name',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'mobile_number',
                'label' => 'Mobile Number',
                'rules' => 'trim|required'
            ),
//              array( 
// 				'field' => 'password', 
// 				'label' => 'Password',   
// 				'rules' => 'trim|required'  
// 			),
// 			array(  
// 				'field' => 'c_password',
// 				'label' => 'Confirm Password', 
// 				'rules' => 'trim|required|matches[password]'
//             ),			
            array(
                'field' => 'surname',
                'label' => 'surname',
                'rules' => 'trim|required'
            ),

            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'city',
                'label' => 'City',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'post_code',
                'label' => 'post code',
                'rules' => 'trim|required'
            )    
        )
    );
		
	/* Login */
	public function index()
	{

         
	     $this->data['restaturant_details'] = $this->Restaturant_model->getRestaurantList();
	     $this->show_view_admin('admin/restaurant_list',$this->data);
  
	}
	
	public function smsmonthlist($restaurant_id)
    {	
	$this->data['smscoundata'] = $this->db->get_where('smsmonthdata',array('restr_id'=> $restaurant_id))->result_array();
    //print_r($this->data['smscoundata']); die;
	$this->show_view_admin('admin/smsmonthrList',$this->data);
        
    }
	
	public function addRestaurant($restaurant_id='')
	{
	    if($restaurant_id)
		{
			
			        
				if (isset($_POST['editResturentsubmit'])) 
				{
				    $this->form_validation->set_rules($this->validation_rules['restaturantEdit']);
                    if($this->form_validation->run())
					{
					    
                        $post['first_name'] = $this->input->post('first_name');
					    $post['restaurant_name'] = $this->input->post('restaurant_name');
					    $post['restaurant_name'] = $this->input->post('restaurant_name');
                        $post['bank_details']    = $this->input->post('bank_details');
                        
						$post['surname'] = $this->input->post('surname');
				        $post['address']    = $this->input->post('address');
				        $latitude                   = $this->input->post('latitude');
				        if(!empty($latitude))
				        {
				           $post['latitude']    = $latitude;
   
				        }                   
				        $longitude                      = $this->input->post('longitude');
				        if(!empty($longitude))
				        {
				          
				          $post['longitude']    = $longitude;
  
				        }
                        $post['user_type'] = 'restaurant';
                        $post['admin_approved_status'] = $this->input->post('admin_approved_status');
						$post['mobile_number'] = $this->input->post('mobile_number');
						$password    = $this->input->post('password');
						if(!empty($password))
						{
						   
						    $post['password'] = md5($password);
   
						}
						$post['email'] = $this->input->post('email');
						$post['city'] = $this->input->post('city');
						$post['zipcode'] = $this->input->post('post_code');
						$post['status'] = 1;
						$post['name_holder'] = $this->input->post('name_holder');
						 $post['security_code']    = $this->input->post('security_code');
						$post['card_no'] = $this->input->post('card_no');
						$post['expire_date'] = $this->input->post('expire_date');
						$post['created_date'] = date('Y-m-d H:i:s');
						$post['updated_date'] = date('Y-m-d H:i:s');
						if (!empty($_FILES["image"]["name"]))

                        {

                                $image = 'image';

                                $fieldName = "image";

                                $Path = 'image/restorent/';

                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);
                                 $post['restaurant_image'] = base_url().$Path.''.$image;

                                

                        }
                        $driver_id =  $this->Restaturant_model->updateRestaurant($post,$restaurant_id);
                        if($driver_id)
						{					
							$msg = 'Restaturant Updated successfully!!';					
							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
							redirect(base_url().'admin/restaurant');
						}
                        
					}
					else
					{	
					    
                            
						$this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);
						$this->show_view_admin('restaurant/restaturant_edit', $this->data);
					}		
				}
				else
				{
				    
                   
				    $this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);
					$this->show_view_admin('admin/restaurant_edit',$this->data);
				}
			
			
		}
		else
		{		
		    if (isset($_POST['addresturentsubmit'])) 
		    {
		        
		        
		          
				    $this->form_validation->set_rules($this->validation_rules['restaturantAdd']);
				    if (empty($_FILES['image']['name']))
                     {
                       $this->form_validation->set_rules('image', 'Image', 'required');
                    } 
					if($this->form_validation->run())
					{
						$post['first_name'] = $this->input->post('first_name');
					    $post['restaurant_name'] = $this->input->post('restaurant_name');
					    $post['restaurant_name'] = $this->input->post('restaurant_name');
                        $post['bank_details']    = $this->input->post('bank_details');
                        $post['security_code']    = $this->input->post('security_code');
						$post['surname'] = $this->input->post('surname');
				        $post['address']    = $this->input->post('address');
				        $post['latitude']    = $this->input->post('latitude');
                        $post['longitude']    = $this->input->post('longitude');
                        $post['user_type'] = 'restaurant';
						$post['mobile_number'] = $this->input->post('mobile_number');
						$connecnumber = random_int(100000, 999999);
                        $post['connectivity_number'] = $connecnumber;
						$post['password'] = md5($this->input->post('password'));
						$post['email'] = $this->input->post('email');
						$post['city'] = $this->input->post('city');
						$post['zipcode'] = $this->input->post('post_code');
						$post['status'] = $this->input->post('status');
						$post['name_holder'] = $this->input->post('name_holder');
						$post['card_no'] = $this->input->post('card_no');
						$post['expire_date'] = $this->input->post('expire_date');
						$post['created_date'] = date('Y-m-d H:i:s');
						$post['updated_date'] = date('Y-m-d H:i:s');
						if ($_FILES["image"]["name"])

                        {

                                $image = 'image';

                                $fieldName = "image";

                                $Path = 'image/restorent/';

                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);
                                 $post['restaurant_image'] = base_url().$Path.''.$image;

                                

                        }
                        $driver_id =  $this->Restaturant_model->addRestaurant($post);	
						if($driver_id)
						{					
							$msg = 'Restaurant added successfully!!';					
							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
							redirect(base_url().'admin/restaurant');
						}
					}
					else
					{				

						$this->show_view_admin('admin/addrestaurant');
					}		
				}
		    else
		    {
              $this->show_view_admin('admin/addrestaurant');
	       }
	   }
			
		
		
    }
    public function dashboard()
    {
        
      
      $this->show_view_admin('admin/dashbord');
        
    }
    
    public function logout() 
	{        
        $this->session->sess_destroy();		
        redirect(base_url());
    }
    
    public function delete_restaurant()
	{
			
			
			$driver_id = $this->uri->segment(4);	
			$this->Restaturant_model->delete_restaurant($driver_id);
			$msg = 'Restaurant remove successfully...!';					
		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
		    redirect(base_url().'admin/restaurant');
			
		
				
	}
	
	public function status_institute(){ 
	$id =$this->input->post('id'); 
	$status = $this->input->post('status');
	if($status == 0){  
	$institution_status= 1;  
	}        else{  
	$institution_status= 0; 
	}	
	$data = ['admin_approved_status' => $institution_status,]; 
	$this->db->where('restaurant_id',$id);  
	$this->db->update('restaurant', $data); 
	echo json_encode(array('status'=>1, 'msg'=>'Institution Status Updated Successfully')); 
	}		
	
	
	public function fullViewRestaurant()
	{
	    $restaurant_id        = $this->uri->segment(4);
        $this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);
        $this->show_view_admin('admin/restaurant_view',$this->data);
	    
    }
    
    function buy($id){ 
        // Set variables for paypal form 
        $returnURL = base_url().'admin/paypal/success'; //payment success url 
        $cancelURL = base_url().'admin/paypal/cancel'; //payment cancel url 
        $notifyURL = base_url().'admin/paypal/ipn'; //ipn url 
         
        // Get product data from the database 
        $product = $this->Restaturant_model->getRestaturantbyid($id);
      
         
       
         
        // Add fields to paypal form 
        $this->paypal_lib->add_field('return', $returnURL); 
        $this->paypal_lib->add_field('cancel_return', $cancelURL); 
        $this->paypal_lib->add_field('notify_url', $notifyURL); 
        $this->paypal_lib->add_field('item_name', $product->restaurant_name); 
        $this->paypal_lib->add_field('custom', $product->restaurant_id); 
        $this->paypal_lib->add_field('item_number',  1); 
        $this->paypal_lib->add_field('amount',  2); 
         
        // Render paypal form 
        $this->paypal_lib->paypal_auto_form(); 
    } 
    
    
    public function membership()
    {
       
        $this->data['membership_details'] = $this->Restaturant_model->getPaymentDetails();
        $this->show_view_admin('admin/member_list',$this->data);
 
    }
    
    public function transactionDetails()
    {
        $resturent_id                     = $this->uri->segment(4);
        $this->data['membership_details'] = $this->Restaturant_model->getTransectionDetails($resturent_id);
        $this->show_view_admin('admin/transection_details',$this->data);
        
    }
    
     public function profile($resturent_id='')
    {      
             
           
            if (isset($_POST['submit']) && $_POST['submit'] == "Edit") 
			{
				       $post['first_name'] = $this->input->post('first_name');
					    $post['restaurant_name'] = $this->input->post('restaurant_name');
					    $post['restaurant_name'] = $this->input->post('restaurant_name');
                        $post['bank_details']    = $this->input->post('bank_details');
						$post['surname'] = $this->input->post('surname');
				        $post['address']    = $this->input->post('address');
				        $post['latitude']    = $this->input->post('latitude');
                        $post['longitude']    = $this->input->post('longitude');
                        $post['user_type'] = 'admin';
						$post['mobile_number'] = $this->input->post('mobile_number');
						$password = $this->input->post('password');
						if(!empty($password))
						{
						   $post['password'] = md5($password);

						}
						$post['email'] = $this->input->post('email');
						$post['city'] = $this->input->post('city');
						$post['zipcode'] = $this->input->post('post_code');
						$post['status'] = 1;
						$post['name_holder'] = $this->input->post('name_holder');
						$post['card_no'] = $this->input->post('card_no');
						$post['expire_date'] = $this->input->post('expire_date');
						$post['created_date'] = date('Y-m-d H:i:s');
						$post['updated_date'] = date('Y-m-d H:i:s');
						if ($_FILES["image"]["name"])

                        {

                                $image = 'image';

                                $fieldName = "image";

                                $Path = 'image/restorent/';

                                $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);
                                 $post['restaurant_image'] = base_url().$Path.''.$image;

                                

                        }
                        $driver_id =  $this->Restaturant_model->updateRestaurant($post,$resturent_id);
                        if($driver_id)
						{					
							$msg = 'Profile Updated successfully!!';					
							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
							redirect(base_url().'admin/restaurant/profile');
						}
                        
		    }
						
				
            else
            {
            
                $restaurant_id  = 4;
                $this->data['restaurent_details'] = $this->Restaturant_model->getRestaturantbyid($restaurant_id);
               
                $this->show_view_admin('admin/profile',$this->data);
            
            
            }

        
    }
    
    public function edit_membership()
    {
        
               $resturent_id                               = $this->uri->segment(4); 
               
                 if (isset($_POST['editmembershipsubmit']))
                 {
                     
                     
                     $resturent_id_post              = $_POST['resturent_id'];
                     $post['note']  =  $_POST['note'];
                     $resturent_id_main =  $this->Restaturant_model->updateRestaurant($post,$resturent_id_post);
                    
                     if(!empty($resturent_id_main))
                     {
                         
                          $msg = 'Membership  Updated successfully!!';					

                         $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
							redirect(base_url().'admin/restaurant/membership'); 
                     }
                     
                 }
                 else
                 {
                     
                      $this->data['resturent_details']             = $this->db->get_Where('restaurant', array('restaurant_id'=>$resturent_id))->result();
                      $this->show_view_admin('admin/edit_membership',$this->data);
                 }

              

        
    }
}
    
    
    


/* End of file */