<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagedata extends MY_Controller  

{
   function __construct()
	{        
		parent::__construct();
		$this->load->model('admin/Customer_model');	    
	}
	/*	Validation Rules */

	/* Login */

	public function referenzenList()
    {
     
	   //$this->data['customer_details'] =  $this->Customer_model->getCustomerList();	   
	   $this->data['refelist'] =  $this->Customer_model->getreferenzenList();
	  // echo 'neha'; die;
	   $this->show_view_admin('admin/refer_list',$this->data);
	   //$this->show_view_admin('admin/referenzen_list',$this->data);

	}
	
	 public function addreferenze(){ 
	  $this->show_view_admin('admin/addreferenze');
	 }
	
	 public function repostrefer(){

  	 if($_POST){
	   	if ($_FILES["image"]["name"])
                        {
                        $image = 'image';
                        $fieldName = "image";
                        $Path = 'image/referenzen/';
                        $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);
                        $listimage = base_url().$Path.''.$image;                         
                  
                        }
		
			   
	            $name = $this->input->post('rp_title');	         
	            $desc = $this->input->post('rp_description');
			
                $insert_ary = array("rp_title"=>$name, "rp_description"=>$desc,"rp_image"=>$listimage);	
				$insert_cat = $this->Customer_model->insertCommon('referenzenpage',$insert_ary);
               //echo $this->db->last_query(); die;	           
			   if($insert_cat){
			    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>added successfully</div></div></section>');
					           
	                redirect('admin/Pagedata/referenzenList');
	            }else{
	                $this->session->set_flashdata('error','Something went wrong.');
	            }
	        }
			
    }  
	
     public function delete_referenzen()
	{
			$driver_id = $this->uri->segment(4);	
			$delete = $this->Customer_model->deleteByNoImageId("referenzenpage",'rp_id ', $driver_id);			
			$msg = 'Referenzen erfolgreich entfernen...!';					
		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
		    redirect(base_url().'admin/Pagedata/referenzenList');
			
		
				
	}
	 
	 
   public function restaurantlogoList()
    {
       
	   $this->data['refelist'] =  $this->Customer_model->getrestaurantlogo();
	   $this->show_view_admin('admin/restaurantlogo_list',$this->data);
	
	}
	
	 public function addrestaurantlogo(){ 
	  $this->show_view_admin('admin/addrestaurantlogo');
	 }
	 
	 public function restlogopost(){     	
	 if($_POST){
	   	if ($_FILES["image"]["name"])
                        {
                        $image = 'image';
                        $fieldName = "image";
                        $Path = 'image/referenzen/';
                        $image = $this->ImageUpload($_FILES["image"]["name"], $image, $Path, $fieldName);
                        $listimage = base_url().$Path.''.$image;                         
                  
                        }
		
			      //echo 'hello'; die;
	             $insert_ary = array("rs_logo"=>$listimage);	
				$insert_cat = $this->Customer_model->insertCommon('restaurantlogo',$insert_ary);
                 //echo $this->db->last_query(); die;	           
			   if($insert_cat){
			    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>added successfully</div></div></section>');
					           
	                redirect('admin/Pagedata/restaurantlogoList');
	            }else{
	                $this->session->set_flashdata('error','Something went wrong.');
	            }
	        }
			
    } 
	
	public function delete_restlogo()
	{
			$driver_id = $this->uri->segment(4);	
			$delete = $this->Customer_model->deleteByNoImageId("restaurantlogo",'rs_id ', $driver_id);			
			$msg = 'Restaurant Logo erfolgreich entfernen...!';					
		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
		    redirect(base_url().'admin/Pagedata/restaurantlogoList');
			
		
				
	}
    

    

    



}

    

    

    





/* End of file */