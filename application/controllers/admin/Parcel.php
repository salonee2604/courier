<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parcel extends MY_Controller  

{

    

    

	function __construct()

	{

		parent::__construct();

		$this->load->model('admin/Parcel_model');

		$this->load->library('form_validation');

       $this->load->library('Twilio');

	}

	

	/*	Validation Rules */

	 protected $validation_rules = array

        (

        'parcelAdd' => array(

           array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),
			array(

                'field' => 'last_name',

                'label' => 'Last Name',

                'rules' => 'trim|required'

            ),
            array(

                'field' => 'customer_number',

                'label' => 'Customer Number',

                'rules' => 'trim|required'

            ),
            array(

                'field' => 'drop_location',

                'label' => 'Drop Location',

                'rules' => 'trim|required'

            ),

            array(

                'field' => 'restaurant_id',

                'label' => 'Resturent Name',

                'rules' => 'trim|required'

            )



            

					

        ),

		'parcelEdit' => array

		(
		    


             array(

                'field' => 'first_name',

                'label' => 'First Name',

                'rules' => 'trim|required'

            ),
			array(

                'field' => 'last_name',

                'label' => 'Last Name',

                'rules' => 'trim|required'

            ),
		    array(

                'field' => 'customer_number',

                'label' => 'Customer Number',

                'rules' => 'trim|required'

            ),
            array(

                'field' => 'drop_location',

                'label' => 'Drop Location',

                'rules' => 'trim|required'

            ),
             array(

                'field' => 'restaurant_id',

                'label' => 'Resturent Name',

                'rules' => 'trim|required'

            )

             

			

        )

    );

		

	/* Login */

	public function index()

	{

	   $this->data['parcel_details'] =  $this->Parcel_model->getAllParcelData();

	   $this->show_view_admin('admin/parcel_list',$this->data);

	    

	}
	
	public function getstate()
    {
        $id =  $this->input->post('id'); 
        $this->data['statelist']= $this->Parcel_model->getstate($id);
        $data1                        ='<option value="">Select One</option>';
        foreach ($this->data['statelist'] as $plist){							  
    	 $data .=   '<option value="'.$plist->region_id .'">'.$plist->name.'</option>';
    } 
    
    echo $data1.$data;
    }
	
	public function getcity()
    {
    $id =  $this->input->post('id'); 
    $this->data['citylist']= $this->Parcel_model->getcity($id);
    foreach ($this->data['citylist'] as $plist){							  
	echo $data =   '<option value="'.$plist->city_id .'">'.$plist->name.'</option>';
    }   
    }

	public function addparcel($parcel_id='')

	{ 
       
	    if($parcel_id)
        {  

				if (isset($_POST['editparcelsubmit'])) 

				{
				     
                     $this->form_validation->set_rules($this->validation_rules['parcelEdit']);

					if($this->form_validation->run())

					{ 

                        $pickup_location_input     = $this->input->post('pickup_location');
                        if(!empty($pickup_location_input))
                        {

                           $post['pic_up_location']   = $this->input->post('pickup_location');


                        }
						$post['drop_location']     = $this->input->post('drop_location');
                        $post['parcel_information'] = $this->input->post('parcel_information');
                        $post['restaurant_id']   = $this->input->post('restaurant_id');
                        $post['created_date']       = date('Y-m-d H:i:s');
                        $post['updated_date']       = date('Y-m-d H:i:s');
						$pickup_latitude          = $this->input->post('pickup_latitude');
						if(!empty($pickup_latitude))
						{
						   $post['pickup_latitude']     = $this->input->post('pickup_latitude'); 
						}
						 $pickup_longitude = $this->input->post('pickup_longitude');
						 if(!empty($pickup_longitude))
						 {
						    $post['pickup_longitude']     = $this->input->post('pickup_longitude');
                         }
						  $drop_latitude          = $this->input->post('drop_latitude');
						  if(!empty($drop_latitude))
						  {
						    
						      $post['drop_latitude']     = $this->input->post('drop_latitude');

						      
						  }
						  $drop_longitude             =  $this->input->post('drop_longitude');
                        if(!empty($drop_longitude))
                        {
                           
                           $post['drop_longitude']     = $this->input->post('drop_longitude');
                            
                        }
                    
                        $parcel_id_update =  $this->Parcel_model->updateParcel($post,$parcel_id);
                        $post_customer['first_name'] = $this->input->post('first_name');
                        $post_customer['last_name'] = $this->input->post('last_name');
                        $post_customer['customer_number']    = $this->input->post('customer_number');
                        $post_customer['customer_address']  = $this->input->post('drop_location');
                        $post_customer['customer_zip_code']   = $this->input->post('customer_zip_code');
                        $post_customer['customer_country']   = 210;
						$post_customer['customer_state']   = '';
                        $post_customer['customer_city']   = $this->input->post('city');
                        $post_customer['restaurant_id']   = $this->input->post('restaurant_id');

                        $post_customer['created_date']      = date('y-m-d H:i:s');
                        $post_customer['updated_date']      = date('y-m-d H:i:s');
                        $post_customer['parcel_id']         = $parcel_id;
                        $customer_id                          =  $this->Parcel_model->updateCustomer($post_customer,$parcel_id);

                        if($parcel_id_update)

						{					

							$msg = 'Paket erfolgreich aktualisiert!!';		
							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
                            redirect(base_url().'admin/parcel');

						}

                        

					}

					else

					{				
                        

                        $this->data['resturent_list']  = $this->Parcel_model->getRestaurantList();
						$this->data['edit_parcel'] = $this->Parcel_model->getParcelid($parcel_id);
                        $this->data['countrylist'] = $this->Parcel_model->getcountry();
						$this->data['statelist'] = $this->Parcel_model->getstates();
						$this->data['citylist'] = $this->Parcel_model->getcitys();
						$this->show_view_admin('admin/edit_parcel', $this->data);

					}		

				}

				else

				{

					$this->data['resturent_list']  = $this->Parcel_model->getRestaurantList();
                    $this->data['countrylist'] = $this->Parcel_model->getcountry();
					$this->data['statelist'] = $this->Parcel_model->getstates();
					$this->data['citylist'] = $this->Parcel_model->getcitys();
				    $this->data['edit_parcel'] = $this->Parcel_model->getParcelid($parcel_id);
                    $this->show_view_admin('admin/edit_parcel',$this->data);

				}

			

			

		}

		else

		{		

		    if (isset($_POST['addparcelsubmit'])) 

		    {
		        

					$this->form_validation->set_rules($this->validation_rules['parcelAdd']);

					if($this->form_validation->run())

					{

                        $restaurant_id             = $this->input->post('restaurant_id');
                        $resturent_details         = $this->Parcel_model->getResturentById($restaurant_id);
                        $pickup_location_input     = $this->input->post('pickup_location');
                        if(!empty($pickup_location_input))
                        {
                           
                            $pickup_location             = $pickup_location_input;
                            $post['pic_up_location']     = $pickup_location; 
                            $post['pickup_latitude']     = $this->input->post('pickup_latitude');
                            $post['pickup_longitude']    = $this->input->post('pickup_longitude');
                            $post['pickup_location_status'] = 1;

                        }
                        else
                        {

                        	$pickup_location                = $resturent_details->address;
                        	$post['pic_up_location']        = $pickup_location; 
                        	$post['pickup_latitude']        = $resturent_details->latitude;
                            $post['pickup_longitude']       = $resturent_details->longitude;
                            $post['pickup_location_status'] = 0;

                        }

						$drop_location           = $this->input->post('drop_location');

						$post['drop_location']     = $this->input->post('drop_location');

						$post['parcel_information'] = $this->input->post('parcel_information');
						$post['parcel_added_by']    = 'web';

						$post['restaurant_id']   = $this->input->post('restaurant_id');


					    $post['pickup_latitude']     = $this->input->post('pickup_latitude');

                        $post['pickup_longitude']     = $this->input->post('pickup_longitude');

                        $post['drop_latitude']     = $this->input->post('drop_latitude');

                        $post['drop_longitude']     = $this->input->post('drop_longitude');

                        
						$post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 
					

						$address = $pickup_location; // Address

						$address2 = $drop_location;

                        $apiKey = 'AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw'; // Google maps now requires an API key.

                        // Get JSON results from this request

                        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);

                        $geo = json_decode($geo, true); // Convert the JSON to an array

                        $drop_location = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address2).'&sensor=false&key='.$apiKey);

                        $drop_location = json_decode($drop_location, true); // Convert the JSON to an array

                        if (isset($geo['status']) && ($geo['status'] == 'OK')) {

	                        $latitude = $geo['results'][0]['geometry']['location']['lat']; 

	                        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude

	                        $drop_latitude = $drop_location['results'][0]['geometry']['location']['lat']; 

	                        $drop_longitude = $drop_location['results'][0]['geometry']['location']['lng']; // Longitude



	                        $post['pickup_latitude']   = $latitude;

	                        $post['pickup_longitude']  = $longitude;

	                        $post['drop_latitude']     = $drop_latitude;

	                        $post['drop_longitude']    = $drop_longitude;

                        }


                        //echo "<pre>"; print_r($post);die;

					   $parcel_id                           =  $this->Parcel_model->addParcel($post);
                        
                        $post_customer['first_name'] = $this->input->post('first_name');
                        $post_customer['last_name'] = $this->input->post('last_name');
                        $post_customer['customer_number']    = $this->input->post('customer_number');
                        $post_customer['restaurant_id']   = $this->input->post('restaurant_id');

                        $post_customer['customer_address']  = $this->input->post('drop_location');
                        $post_customer['customer_zip_code']   = $this->input->post('customer_zip_code');
                        $post_customer['customer_country']   = 210;
						$post_customer['customer_state']   = '';
                        $post_customer['customer_city']   = $this->input->post('city');
                        $post_customer['created_date']      = date('y-m-d H:i:s');
                        $post_customer['updated_date']      = date('y-m-d H:i:s');
                        $post_customer['parcel_id']         = $parcel_id;
                        $customer_id                          =  $this->Parcel_model->addCustomer($post_customer);
                        if($parcel_id)
                        {					
                         $sms_sender ="19737989690"; 
						 //$sms_reciever = "9202243498";
                         $sms_reciever = $this->input->post('customer_number');
                         $sms_message = "Dear customer your order will arrive in 7 minutes.";
                         $from = '+'.$sms_sender; //trial account twilio number
                         $to = '+91'.$sms_reciever; //sms recipient number
                        //echo $to; die;
						
                          $response = $this->twilio->sms($from, $to,$sms_message);
					/* 	  if($response->IsError){
 
echo 'Sms Has been Not sent'; die;
}
else{
 
echo 'Sms Has been sent'; die;
}  */
						  

							$msg = 'Paket erfolgreich hinzugefügt!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'admin/parcel');

						}

					}

					else

					{

					    $this->data['resturent_list']  = $this->Parcel_model->getRestaurantList();		
                        $this->data['countrylist'] = $this->Parcel_model->getcountry();
						$this->show_view_admin('admin/addparcel',$this->data);

					}		

				}

		    else

		    {

		      $this->data['resturent_list']  = $this->Parcel_model->getRestaurantList();
              $this->data['countrylist'] = $this->Parcel_model->getcountry();
              $this->show_view_admin('admin/addparcel',$this->data);

	       }

	   }

			

		

		

    }

    public function dashboard()

    {

      $this->show_view_admin('admin/dashbord');

        

    }

    

    public function logout() 

	{        

        $this->session->sess_destroy();		
        redirect(base_url());

    }
    
    public function parcelFullView()
    {
        $parcel_id = $this->uri->segment(4);
        $this->data['resturent_list']  = $this->Parcel_model->getRestaurantList();
        $this->data['edit_parcel'] = $this->Parcel_model->getParcelid($parcel_id);
        $this->show_view_admin('admin/parcel_view',$this->data);                  
        
    }

    

    public function delete_Parcel()

	{

			$parcel_id = $this->uri->segment(4);	

			$this->Parcel_model->deleteParcel($parcel_id);

			$msg = 'Parcel remove successfully...!';					

		    $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

		    redirect(base_url().'admin/parcel');
    }



}

    

    

    





/* End of file */