<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trip extends MY_Controller  

{

    

    

	public function __construct()

	{

		parent::__construct();

		$this->load->model('admin/Trip_model');
        $this->load->library('form_validation'); 
        $this->load->library('Ciqrcode');


	}

	

	/*	Validation Rules */

	 protected $validation_rules = array

        (

        'tripAdd' => array(

           array(

                'field' => 'name',

                'label' => 'Name',

                'rules' => 'trim|required'

            ),

			
            

            array(

                'field' => 'parcel_id[]',

                'label' => 'Parcel',

                'rules' => 'trim|required'

            )

			

			

             			

        ),

		'tripEdit' => array

		(

           array(

                'field' => 'name',

                'label' => 'Name',

                'rules' => 'trim|required'

            ),

			
            

            array(

                'field' => 'parcel_id[]',

                'label' => 'Parcel',

                'rules' => 'trim|required'

            )

		

        )

    );

		

	/* Login */

	public function index()

	{

	   $this->data['trip_details'] =  $this->Trip_model->getAllTripData();

	   $this->show_view_admin('admin/trip_list',$this->data);

	    

	}
	
	/* trackingtrip */
	public function trackingtrip($trip_id='')
	{
		$tripid = $this->uri->segment(4); 
        $this->data['trackingtrip'] =  $this->Trip_model->gettrackingtrip($tripid);
      
		//print_r($this->data['trackingtrip']); die;
		$this->data['trip_details'] =  $this->Trip_model->getAllTripData();
		//print_r($this->data['trackingtrip']); die;
		$this->show_view_admin('admin/trackingtrip',$this->data);
	}
	/* trackingtrip */

	public function addTrip($trip_id='')

	{

	    

	    $trip_id_qr = $this->uri->segment(4);
	    
	    if($trip_id)

		{

			

			        

				if (isset($_POST['edittripsubmit'])) 

				{
				    
				    
				    $this->form_validation->set_rules($this->validation_rules['tripEdit']);

					if($this->form_validation->run())

					{

				        $post['name']               = $this->input->post('name');

						$post['brand']              = $this->input->post('brand');

						$post['driver_id']          = $this->input->post('driver_id');

						$parcel_id_array            = $this->input->post('parcel_id');

						 $post['parcel_id']         = implode(",",$parcel_id_array);

                         $post['trip_assign_status'] = 1;

						$post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 

                      
						$trip_id_main                 =  $this->Trip_model->updateTrip($post,$trip_id);

                        if($trip_id_main)

						{					

							$msg = 'Trip Updated successfully!!';		

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'admin/trip');

						}

                        

					}

					else

					{				
                        
                        
                        $qr_image=rand().'.png';
                        $params['data'] = $trip_id_qr;
                        $params['level'] = 'H';
                        $params['size'] = 8;
                        $params['savename'] =FCPATH."image/".$qr_image;
                        if($this->ciqrcode->generate($params))
                        {
                        	$this->data['img_url']=$qr_image;	
                        }
                        
						$this->data['edit_trip'] = $this->Trip_model->getTripById($trip_id);
                        	$this->data['resturent_list']  = $this->Trip_model->getRestaurantList();
						$this->data['driver_list']  = $this->Trip_model->getAllDriverData();

            		      $this->data['parcel_details']  = $this->Trip_model->getAllParcelData();

						$this->show_view_admin('admin/edit_trip', $this->data);

					}		

				}

				else

				{

				    
                        $qr_image=rand().'.png';
                        $params['data'] = $trip_id_qr;
                        $params['level'] = 'H';
                        $params['size'] = 8;
                        $params['savename'] =FCPATH."image/".$qr_image;
                        if($this->ciqrcode->generate($params))
                        {
                        	$this->data['img_url']=$qr_image;	
                        }
                        
                        
                        
                    
				    $this->data['driver_list']  = $this->Trip_model->getAllDriverData();

            		      $this->data['parcel_details']  = $this->Trip_model->getAllParcelData();
                   	$this->data['resturent_list']  = $this->Trip_model->getRestaurantList();
				    $this->data['edit_trip'] = $this->Trip_model->getTripById($trip_id);

					$this->show_view_admin('admin/edit_trip',$this->data);

				}

			

			

		}

		else

		{		

		    if (isset($_POST['addtripsubmit'])) 

		    {

		        
		    	$this->form_validation->set_rules($this->validation_rules['tripAdd']);

					if($this->form_validation->run())

					{

					    

					    

						$post['name']               = $this->input->post('name');

						$post['brand']              = $this->input->post('brand');

						$post['driver_id']          = $this->input->post('driver_id');

						$parcel_id_array            = $this->input->post('parcel_id');

						 $post['parcel_id']         = implode(",",$parcel_id_array);
						 
						 $post['restaurant_id']     = $this->input->post('restaurant_id');


                         $post['trip_assign_status'] = 1;

						$post['created_date']       = date('Y-m-d H:i:s');

						$post['updated_date']       = date('Y-m-d H:i:s'); 

                       

						$parcel_id =  $this->Trip_model->addTrip($post);

					

						if($parcel_id)

						{					

							$msg = 'Trip added successfully!!';					

							$this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');

							redirect(base_url().'admin/trip');

						}

					}

					else

					{				



                          $this->data['driver_list']  = $this->Trip_model->getAllDriverData();

            		      $this->data['parcel_details']  = $this->Trip_model->getAllParcelData();
            		      
            		      	$this->data['resturent_list']  = $this->Trip_model->getRestaurantList();

                          $this->show_view_admin('admin/addtrip',$this->data);					}		

				}

		    else

		    {

		        

		       $this->data['resturent_list']  = $this->Trip_model->getRestaurantList();
		       $this->data['driver_list']  = $this->Trip_model->getAllDriverData();
               $this->data['parcel_details']  = $this->Trip_model->getAllParcelData();
              $this->show_view_admin('admin/addtrip',$this->data);

	       }

	   }

			

		

		

    }

    public function dashboard()

    {

      $this->show_view_admin('admin/dashbord');

        

    }

    

    public function logout() 

	{        

        $this->session->sess_destroy();		

        redirect(base_url());

    }

    

    public function delete_trip()

	{

			$trip_id = $this->uri->segment(4);	
            $this->Trip_model->deleteTrip($trip_id);
            $msg = 'Trip remove successfully...!';					
            $this->session->set_flashdata('message', '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
            redirect(base_url().'admin/trip');

   }
   
   public function tripfullView()
   {
              
            $trip_id_qr = $this->uri->segment(4);  
              
            $qr_image=rand().'.png';
            $params['data'] = $trip_id_qr;
            $params['level'] = 'H';
            $params['size'] = 8;
            $params['savename'] =FCPATH."image/".$qr_image;
            if($this->ciqrcode->generate($params))
            {
            $this->data['img_url']=$qr_image;	
            }
            
            $this->data['edit_trip'] = $this->Trip_model->getTripById($trip_id_qr);
            $this->data['resturent_list']  = $this->Trip_model->getRestaurantList();
            $this->data['driver_list']  = $this->Trip_model->getAllDriverData();
            
            $this->data['parcel_details']  = $this->Trip_model->getAllParcelData();
            
            $this->data['driver_list']  = $this->Trip_model->getAllDriverData();
            $this->data['parcel_details']  = $this->Trip_model->getAllParcelData();
            $this->show_view_admin('admin/trip_view',$this->data);

       
   }



}

    

    

    





/* End of file */