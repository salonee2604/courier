<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MY_Controller  
{
    
    
	function __construct()
	{
		parent::__construct();
        $this->load->model('front/Login_model');
        $this->load->model('front/Restaturant_model');
        $this->load->library('form_validation');
    }
	
	protected $validation_rules = array
        (
         'contact' => array
        (
            array(
                'field' => 'name',
                'label' => 'Name, Vorname',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'E-Mail',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'subject',
                'label' => 'Betrifft',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'message',
                'label' => 'Mitteilung',
                'rules' => 'trim|required'
            )
        ),
		
		 'resetpassv' => array
        (
            array(
                'field' => 'token_id',
                'label' => 'token_id',
                'rules' => 'trim|required'
            ),
            array( 
				'field' => 'password', 
				'label' => 'Password',   
				'rules' => 'trim|required'  
			),
			array(  
				'field' => 'c_password',
				'label' => 'Confirm Password', 
				'rules' => 'trim|required|matches[password]'
            )
        )

    );
	
		
	public function index()
	{
	  $this->data['refelist'] =  $this->Restaturant_model->getrestaurantlogo();
	   $this->load->view('front/header');	  
	   $this->load->view('front/home',$this->data);	  
	   $this->load->view('front/footer');
	    //echo 'hello'; die;
    }
	
    public function about()
	{
	   $this->load->view('front/header');
	   $this->load->view('front/about');
	   $this->load->view('front/footer');
	   
    }
    	
	public function disclaimer()
	{
	   $this->load->view('front/header');
	   $this->load->view('front/disclaimer');
	   $this->load->view('front/footer');
	}
	
	public function imressum()
	{
	  $this->load->view('front/header');
	   $this->load->view('front/imressum');
	   $this->load->view('front/footer');
	}
	
	
    public function team()
	{	   
	   $this->load->view('front/header');
	   $this->load->view('front/team');
	   $this->load->view('front/footer');
		
    }
    
    public function contact()
	{
	    
	  if(isset($_POST['addContactsubmit']))
			{	
				$this->form_validation->set_rules($this->validation_rules['contact']);
				if ($this->form_validation->run()) 
				{
                        $c_name=$this->input->post('name'); 
                        $c_email=$this->input->post('email'); 
                        $c_subject=$this->input->post('subject');
                        $c_message=$this->input->post('message'); 
                        $data = array(
                        'c_name'=>$c_name,
                        'c_email'=>$c_email,
                        'c_subject'=>$c_subject,
                        'c_message'=>$c_message
                        );
                        
                        $save =  $this->db->insert('contactdata',$data);
					if(!empty($save))
					{

					    $msg = 'Contact Added Successfully';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'contact#contact');
					}
					else
					{
						$msg = 'Invalid Data';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'home/contact');
					}
				}
				else
				{					
                     $this->load->view('front/header');
                     $this->load->view('front/contact');
					 $this->load->view('front/footer');
				}
			}
			else
			{
				// To Load second database and perform query operation.
                     $this->load->view('front/header');
                     $this->load->view('front/contact');
					 $this->load->view('front/footer');
			}
		
    }
    
    public function privacy()
	{
	   $this->load->view('front/header');
	   $this->load->view('front/privacy');
	   $this->load->view('front/footer');
		
    }
    
    public function terms()
    {
       $this->load->view('front/header');
	   $this->load->view('front/terms');
	   $this->load->view('front/footer');
  
    }
	
	public function passwordreset()
    {
       $this->load->view('front/header');
	   $this->load->view('front/passwordreset');
	   $this->load->view('front/footer');
  
    }
	
	public function referenzen()
    {
       $this->data['refelist'] =  $this->Restaturant_model->getreferenzenList();
	   $this->load->view('front/header');
	   $this->load->view('front/referenzen',$this->data);
	   $this->load->view('front/footer');
  
    }

  public function postconatct()
	{
	// echo 'hello'; die;	
	  $c_name=$this->input->post('name'); 
      $c_email=$this->input->post('email'); 
	  $c_subject=$this->input->post('subject');
	 $c_message=$this->input->post('message'); 
     $data = array(
        'c_name'=>$c_name,
        'c_email'=>$c_email,
		'c_subject'=>$c_subject,
		'c_message'=>$c_message
     );

    $save =  $this->db->insert('contactdata',$data);
    //echo $this->db->last_query(); die;
     if($save)
     {
	   //redirect('contact');
     redirect(base_url().'home/contact');
	}
	
	}
	
	public function resetpass($uid)
    {
       $this->load->view('front/header');
	   $this->load->view('front/resetpass');
	   $this->load->view('front/footer');
  
    }
	
	public function mailsend()
	{
        $email =  $this->input->post('email');
		$useremail = $this->Restaturant_model->email_exists($email);
        if ($useremail) 
                {
			          $uid =  $useremail[0]->restaurant_id; 	
					  $to  = $email; 
	                  $sub = 'verify Our Account';
		              $key=rand(1000,9999);
	                  $udata=array(                          
							   'emailverify'=>$key													
                                );

                   $data = array( 
                        'vrfn_code'  => $key                       
                        );
                      $this->db->where('email',$email);
                      $this->db->update('restaurant', $data);
								
		              $message = "<h3>Hello Dear,</h3>          
                        <p>Please use this code <h4><b>".$key."</b></h4></p>
                        <p>Click on the button below the reset your password.</p>
                       <a href='".base_url()."Home/resetpass/$uid'>Click Here</a>

					  Use This Otp Code $key"; 	
	                  $this->email($to,$sub,$message);  	
					
                        $msg = 'Please check Mail';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'passwordreset');
					}
					else
					{
						$msg = 'this email address does not exist';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'passwordreset');
					} 
		 
	   
	}
	
	public function resetpasspa()
	{
	    
	    $uid =  $this->input->post('user_id');
	    $token =$this->input->post('token_id');
	    $useremail = $this->Restaturant_model->token_exists($token);
		if($useremail){
				$this->form_validation->set_rules($this->validation_rules['resetpassv']);
				if ($this->form_validation->run()) 
				{
					
				 $uid =  $this->input->post('user_id');
	             $password = md5($this->input->post('password'));
				 $token =$this->input->post('token_id');
				
				   $data = array( 
                        'vrfn_code'  => 0, 
						 'password'  => $password 
                        );
                      $this->db->where('restaurant_id',$uid);
                      $this->db->update('restaurant', $data);
			           $msg = 'Password Change successfully';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'Home/resetpass/'.$uid.'');  
					
				} else {
				$msg = 'Invalid Token ID and password same confirm password';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'Home/resetpass/'.$uid.'');	
				}	} else {
				$msg = 'Invalid Token ID and password same confirm password';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'Home/resetpass/'.$uid.'');	
				}					
		
	}
	
    public function Driverresetpass($uid)
    {
       $this->load->view('front/header');
	   $this->load->view('front/driverresetpass');
	   $this->load->view('front/footer');
  
    }
    
    public function driverresetpasspa()
	{
	    
	    $uid =  $this->input->post('user_id');
	    $token =$this->input->post('token_id');
	    $useremail = $this->Restaturant_model->driver_token_exists($token);
		if($useremail){
				$this->form_validation->set_rules($this->validation_rules['resetpassv']);
				if ($this->form_validation->run()) 
				{
					
				 $uid =  $this->input->post('user_id');
	             $password = md5($this->input->post('password'));
				 $token =$this->input->post('token_id');
				
				   $data = array( 
                        'vrfn_code'  => 0, 
						 'password'  => $password 
                        );
                      $this->db->where('driver_id',$uid);
                      $this->db->update('driver', $data);
			           $msg = 'Password Change successfully';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'Home/Driverresetpass/'.$uid.'');  
					
				} else {
				$msg = 'Invalid Token ID and password same confirm password';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'Home/Driverresetpass/'.$uid.'');	
				}	} else {
				$msg = 'Invalid Token ID and password same confirm password';
						$this->session->set_flashdata('message', '<div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div>');
						redirect(base_url().'Home/Driverresetpass/'.$uid.'');	
				}					
		
	}
		
	

    
    

}
/* End of file */