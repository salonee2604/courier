<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';

$route['admin'] = 'admin/Login';
$route['restaurantLogin']  = 'Login/restaurantLogin';
$route['restaurantSignup'] = 'Login/restaurantSignup';


/* front end link */
$route['about']           =  'Home/about';
$route['team']            =  'Home/team';
$route['contact']         =  'Home/contact';
$route['privacy']         =  'Home/privacy';
$route['terms']           =  'Home/terms';
$route['disclaimer']      =  'Home/disclaimer';
$route['referenzen']      =  'Home/referenzen';
$route['imressum']      =  'Home/imressum';
$route['passwordreset']      =  'Home/passwordreset';
/* front end link */

/* admin work */
$route['admin/contactlist']              =  'admin/Customer/conatctlist';
$route['admin/referenzenlist']           = 'admin/Pagedata/referenzenList';
$route['admin/profile']                  = 'admin/restaurant/profile';

/* admin work */

/* $route['about']           = 'Login/about';
$route['team']            =  'Login/team';
$route['contact']         =  'Login/contact';
$route['privacy']         =  'Login/privacy';
$route['terms']           =    'Login/terms';
$route['disclaimer']      =    'Login/disclaimer'; */

$route['admin/dashboard'] = 'admin/Login/dashboard';
$route['restaurant/dashboard'] = 'restaurant/Login/dashboard';
$route['restaurant/dashbordnoplan'] = 'restaurant/Login/dashboardno'; 
$route['restaurant/dashboardtermination'] = 'restaurant/Login/dashboardtermination'; 

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
